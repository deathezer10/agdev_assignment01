#ifndef KEYBOARD_CONTROLLER_H
#define KEYBOARD_CONTROLLER_H

#include "SingletonTemplate.h"
#include <bitset>
#include <windows.h>

class KeyboardController : public Singleton<KeyboardController>
{
	friend Singleton<KeyboardController>;
public:
	const static int MAX_KEYS = 348;

	// System Interface
	void UpdateKeyboardStatus(int _slot, bool _isPressed);
	void EndFrameUpdate();
	
	// User Interface
	bool IsKeyDown(int _slot);
	bool IsKeyUp(int _slot);
	bool IsKeyPressed(int _slot);
	bool IsKeyReleased(int _slot);
	
private:
	KeyboardController();
	~KeyboardController();

	std::bitset<MAX_KEYS> currStatus, prevStatus;
	
};

#endif // KEYBOARD_CONTROLLER_H