#include "KeyboardController.h"

KeyboardController::KeyboardController()
{
}

KeyboardController::~KeyboardController()
{
}

void KeyboardController::UpdateKeyboardStatus(int _slot, bool _isPressed)
{
	currStatus.set(_slot, _isPressed);
}

void KeyboardController::EndFrameUpdate()
{
	prevStatus = currStatus;
}

bool KeyboardController::IsKeyDown(int _slot)
{
	return currStatus.test(_slot);
}

bool KeyboardController::IsKeyUp(int _slot)
{
	return !currStatus.test(_slot);
}

bool KeyboardController::IsKeyPressed(int _slot)
{
	return IsKeyDown(_slot) && !prevStatus.test(_slot);
}

bool KeyboardController::IsKeyReleased(int _slot)
{
	return IsKeyUp(_slot) && prevStatus.test(_slot);
}
