[Objective]

1. Survive as long as possible!
2. Kill enemies and collect the scraps they dropped to use different skills!
3. Enemy spawn rate gets faster and faster, see what high score you can achieve!



[Controls]

[Mouse Move] - Camera look around

[W/A/S/D] - Basic movement

[Q] Refill Weapon Ammo

[R] Reload Weapon Ammo

[1] Sniper Rifle - Fires a line projectile, dealing heavy damage

[2] Automatic RPG - Fires a box projectile, dealing damage

[3] Deploy Sentry - Deploys a Sentry that fires bullet at nearby enemies, Cost 25 Scraps to use

[E] Use Air Strike - Select the spatial partition using W/A/S/D and left-click to launch, , Cost 100 Scraps to use

[T] Toggle Map Analyser - Use mouse movement to look around the map, revealing visible enemies in the spatial partition

[G] Enter Tank - W/S to accelerate/decelerate, A/D to rotate left/right, Mouse Look to rotate turret (SceneNode Relationship), LMB to fire cannonball

[V] Toggle Box-Collider - Lightblue boxes are outer hitbox while green boxes are inner hitbox, upon collision with the outer hitbox,
it checks for all its children hitboxes and verifies the collision, this is used for the Enemy's hit detection to allow Head/Body/Arm Shots

[L] Toggle LOD - Red: Low Detail, Yellow: Medium Detail, Green: High Detail



[Features]

[Advanced Collision Detection]
1. Line vs AABB projectile using Sniper Rifle - Hotkey: [1]
2. Hierachical Hitboxes using SceneNodes - Shooting a enemy in the Head/Arm/Body
3. Spatial Partition Optimised Collision - Entities only checks its grid and its neighbour grids for collision

[Level-Of-Details]
1. Objects further are rendered at a lower resolution - Hotkey: [L]
2. Objects that are not visible are not rendered at all - Hotkey: [T]

[Scene Graph]
1. Able to add/detach/destroy/search for SceneNodes
2. All Spatial Partition Grids are nested in the Scene Graph
3. Able to apply Translate/Rotate/Scale, its children will also transform with it

[Spatial Partitioning]
1. Able to migrate objects that has moved out of grid to another grid
2. Able to retrieve SceneNodes in a position
3. Helper functions in CSpatialPartition::GetObjectsWithNeighbour() to get neighbour objects of a grid
4. Able to remove all nested objects via SceneGraph::FindAndDestroy() - Hotkey: [E]

[Extra]
1. Able to control a Tank, both the Body and Turret are rotatable and movable - Hotkey [G]
2. Upon Tank duration over, it's Body and Turret gets detached and collapses properly



[Task Assigned]


[Koh Yoong Soon]

1. LOD Logic & Rendering
2. Spatial Partitioning
2. Tank
3. Enemy
4. Tree
5. Rock


[Ong Hui Sheng]

1. SceneGraph
2. Hierarchy Hitbox
3. Implement get neighbour objects in grid
4. Sentry
5. Air Strike
6. Map Analyser
