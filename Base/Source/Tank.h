#pragma once


#include "EmptyObject.h"
#include "Scene.h"
#include "FPSCamera.h"

class Tank : public EmptyObject {

public:
	enum TANK_PART
	{
		TANK_TURRET,
		TANK_BODY,
	};

	TANK_PART whichTankPart;

	Tank(Scene* scene, Vector3 pos, TANK_PART _tankPart) : EmptyObject(scene){
		active = true;
		whichTankPart = _tankPart;
		// this->pos = pos; // causes camera rotation bug
		Translate(pos.x, pos.y, pos.z);

		mass = 50.f;

		switch (whichTankPart)
		{
		case  TANK_TURRET:
			type = Scene::GEO_HIGH_LOD_TANK_TURRET;
			break;
		case TANK_BODY:
			type = Scene::GEO_HIGH_LOD_TANK_BODY;
			collider.setBoundingBoxSize(Vector3(8.5f, 10, 10.f));
			break;
		}
		collider.type = Collider::C_AABB;
	

	}
	~Tank() {};

	virtual void OnCollisionHit(GameObject* other)
	{
		// only destroy tree , robot and lamp when the player is inside the tank
		if (b_isPlayerInTank == true) {
			if (other->type == Scene::GEO_HIGH_LOD_TREE || other->type == Scene::GEO_HIGH_LOD_ROBOT_BODY || other->type == Scene::GEO_HIGH_LOD_ROCK) {
				m_scene->sceneGraph.FindAndDestroy(other);
			}
		}
	}

	//Movement speed of the tank
	const float TANKSPEED = 0.5f;
	bool b_isPlayerInTank = false;
};