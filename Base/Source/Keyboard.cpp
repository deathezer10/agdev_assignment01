#include "Keyboard.h"
#include <iostream>
using namespace std;

#include "KeyboardController.h"
#include "PlayerInfo.h"
#include "LuaInterface.h"
#include <map>

CKeyboard::CKeyboard()
{
}


CKeyboard::~CKeyboard()
{
}

bool CKeyboard::LoadControls(string filePath) {

	int lineCounter = 0;

	std::ifstream file(filePath, std::ios::in);
	std::string line;

	// Failed to open file
	if (file.is_open() == false)
		return false;

	// Read line by line
	while (std::getline(file, line)) {

		// Ignore header
		if (lineCounter == 0) {
			lineCounter++;
			continue;
		}

		std::stringstream linestream(line);
		std::string data;

		string functionName;
		unsigned char key;

		unsigned index = 0;

		// Delimittedly read from line and output the data
		while (getline(linestream, data, '=')) {
			switch (index) {
			case 0:
				functionName = data;
				break;
			case 1:
				key = data[0];

				if (data.length() > 1) {
					if (data == "M0") // M0 - Left Click
						key = 0;
					if (data == "M1") // M1 - Right Click
						key = 1;
					if (data == "M2") // M2 - Middle Click
						key = 2;
					if (data == "Lshift")
						key = VK_LSHIFT;
					if (data == "Spacebar")
						key = VK_SPACE;
					if (data == "Ctrl")
						key = VK_CONTROL;
				}
				break;
			}
			index++;
		}

		// Add a new key for every line read
		keyMap.insert(std::make_pair(functionName, key));
		
		lineCounter++;
	}

	file.close();

	CLuaInterface::GetInstance()->SetLuaState("PlayerControls");

	(*keyMap.find("MoveFront")).second = CLuaInterface::GetInstance()->getIntValue("MoveFront");
	(*keyMap.find("MoveBack")).second = CLuaInterface::GetInstance()->getIntValue("MoveBack");
	(*keyMap.find("MoveLeft")).second = CLuaInterface::GetInstance()->getIntValue("MoveLeft");
	(*keyMap.find("MoveRight")).second = CLuaInterface::GetInstance()->getIntValue("MoveRight");

	(*keyMap.find("SecondaryWeapon")).second = CLuaInterface::GetInstance()->getIntValue("Primary");
	(*keyMap.find("PrimaryWeapon")).second = CLuaInterface::GetInstance()->getIntValue("Secondary");
	
	(*keyMap.find("Sprint")).second = CLuaInterface::GetInstance()->getIntValue("Sprint");

	CLuaInterface::GetInstance()->SetLuaState("");

	return true;
}

// Create this controller
bool CKeyboard::Create(CPlayerInfo* thePlayerInfo)
{
	CController::Create(thePlayerInfo);
	cout << "CKeyboard::Create()" << endl;
	return false;
}


// Read from the controller
int CKeyboard::Read(const float deltaTime)
{
	CController::Read(deltaTime);

	// Process the keys for customisation
	if (KeyboardController::GetInstance()->IsKeyDown(keyMap.at("MoveFront")))
		Move_FrontBack(deltaTime, true);

	// Process the keys for customisation
	if (KeyboardController::GetInstance()->IsKeyDown(keyMap.at("MoveBack")))
		Move_FrontBack(deltaTime, false);

	// Process the keys for customisation
	if (KeyboardController::GetInstance()->IsKeyDown(keyMap.at("MoveLeft")))
		Move_LeftRight(deltaTime, true);

	// Process the keys for customisation
	if (KeyboardController::GetInstance()->IsKeyDown(keyMap.at("MoveRight")))
		Move_LeftRight(deltaTime, false);


	return 0;
}
