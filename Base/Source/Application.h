#ifndef APPLICATION_H
#define APPLICATION_H

#include "timer.h"

struct GLFWwindow;

class Application
{
public:
	static Application& GetInstance()
	{
		static Application app;
		return app;
	}
	void Init();
	void Run();
	void Exit();

	void UpdateInput();
	void PostInputUpdate();

	static void MouseButtonCallbacks(GLFWwindow* window, int button, int action, int mods);
	static void MouseScrollCallbacks(GLFWwindow* window, double xoffset, double yoffset);

	int GetWindowHeight();
	int GetWindowWidth();

	static int windowHeight() {
		return m_window_height;
	}
	static int windowWidth() {
		return m_window_width;
	}

	static int fullscreenWindowHeight() {
		return m_full_window_height;
	}
	static int fullscreenWindowWidth() {
		return m_full_window_width;
	}

	static void SetIsPaused(bool toggle) { _isPaused = toggle; }

	static float WindowToFullscreenWidthRatio();
	static float WindowToFullscreenHeightRatio();

	// Switch between Cursor Disabled & Enabled mode
	static void ToggleCursor();

	// Switch between Fullscreen and Window mode
	static void ToggleFullscreen();

	// Is the application currently in Fullscreen?
	static bool IsFullScreenMode() { return _isFullScreen; };

	// Was window resized recently?
	static bool IsWindowValid() { return _isValid; };

	// Is the application currently Paused?
	static bool IsPaused() { return _isPaused; }

	// Flag window as clean
	static void ValidateWindow() { _isValid = true; };

	const static unsigned char FPS = 60; // FPS of this game
	const static unsigned int frameTime = 1000 / FPS; // time for each frame

private:
	Application();
	~Application();

	static bool IsKeyPressed(unsigned short key);

	// Should make these not hard-coded :P
	static int m_window_width;
	static int m_window_height;
	static int m_full_window_height;
	static int m_full_window_width;

	int m_default_window_width = 800;
	int m_default_window_height = 600;

	//Declare a window object
	StopWatch m_timer;

	// Fullscreen helper
	static bool _isFullScreen;
	static bool _isValid;
	static bool _isPaused;

};

#endif