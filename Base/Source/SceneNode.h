#pragma once

#include <vector>
#include "Mtx44.h"

class GameObject;


class SceneNode {

	friend class SceneGraph;

public:
	SceneNode(const std::string& name, GameObject* go);
	~SceneNode() {};

	void AddChild(SceneNode* child);

	// Destroys this node and all underlying childrens
	void Destroy();

	// Search the Child for the given SceneNode and destroy it and its underlying SceneNodes
	// Returns TRUE if child is destroyed, FALSE if cannot be found
	bool DestroyChild(SceneNode* child);

	// Search the Child for the given SceneNode and destroy it and its underlying SceneNodes
	// Returns TRUE if child is destroyed, FALSE if cannot be found
	// bool DestroyChild(GameObject * m_Go);

	// Destroys its Children and its underlying SceneNodes
	void DestroyAllChild();

	// Break of a single connection between this node and the child, 
	// Deteched node's memory is now not managed by the SceneGraph
	// Returns TRUE if child is removed
	bool DetachChild(SceneNode* child);
	void DetachParent();

	// Returns the first occurence of the SceneNode in this children's collection
	// Returns nullptr if nothing found
	SceneNode* FindNode(const std::string& name);

	// Returns the first occurence of the SceneNode in this children's collection
	// Returns nullptr if nothing found
	SceneNode* FindNode(SceneNode* node);

	// Returns a collection of all objects beneath this SceneNode
	std::vector<SceneNode*> GetAllChild();

	SceneNode* GetParent() { return m_Parent; }
	Mtx44 GetTransform() { return m_Transform; }
	GameObject* GetOBJ() { return m_Go; };
	void SetName(std::string _name) { name = _name; }
	std::string GetName() { return name; }

	void Translate(float x, float y, float z);
	void Rotate(float degrees, float x, float y, float z) throw(DivideByZero);
	void Scale(float x, float y, float z);

	void Render();

	std::vector<SceneNode*> m_Child;

protected:
	std::string name;
	GameObject* m_Go;
	SceneNode* m_Parent = nullptr;

	Mtx44 m_Transform;

	// Recursion Helper
	void RecurseDestroy(SceneNode* currentNode);
	SceneNode* RecurseFindNode(const std::string& name, SceneNode* currentNode);
	SceneNode* RecurseFindNode(SceneNode* nodeToFind, SceneNode* currentNode);
	void RecurseFindChild(std::vector<SceneNode*> &container, SceneNode* currentNode);

};