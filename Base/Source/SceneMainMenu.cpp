#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "Application.h"
#include "SceneMainMenu.h"
#include "SceneManager.h"
#include "SceneHighScore.h"
#include "SceneOptions.h"
#include "SceneText.h"
#include "GraphicsManager.h"
#include "MouseController.h"
#include "KeyboardController.h"
#include "RenderHelper.h"
#include "MeshBuilder.h"
#include "LoadTGA.h"
#include "LuaInterface.h"


void SceneMainMenu::Init()
{
	SceneBase::Init();


	// Create the playerinfo instance, which manages all information about the player
	CLuaInterface::GetInstance()->SetLuaState("PlayerSettings");
	CLuaInterface::GetInstance()->LoadPlayer();

	// Create and attach the camera to the scene
	camera.Init(playerInfo->GetPos(), playerInfo->GetTarget(), playerInfo->GetUp());
	playerInfo->AttachCamera(&camera);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	meshList[GEO_MAINMENU] = MeshBuilder::GenerateRadarQuad("GEO_MAINMENU", Color(1, 1, 1));
	meshList[GEO_MAINMENU]->textureID = LoadTGA("Image/mainmenubg.tga", false);

	glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	MouseController::GetInstance()->SetKeepMouseCentered(false);

	btnStartGame = new GUIButton(0, Vector2(0.5f, 0.5f), Vector2(20, 7), "Start Game");
	m_GUI.AddButton(btnStartGame);

	btnHighScore = new GUIButton(1, Vector2(0.5f, 0.4f), Vector2(20, 7), "High Score");
	m_GUI.AddButton(btnHighScore);

	btnOptions = new GUIButton(2, Vector2(0.5f, 0.3f), Vector2(20, 7), "Controls");
	m_GUI.AddButton(btnOptions);

	btnQuit = new GUIButton(3, Vector2(0.5f, 0.2f), Vector2(20, 7), "Quit");
	m_GUI.AddButton(btnQuit);
}


void SceneMainMenu::Update(double dt) {

	SceneBase::Update(dt);

	if (btnStartGame->OnClick()) {
		SceneManager::getInstance()->changeScene(new SceneText());
	}

	if (btnHighScore->OnClick()) {
		SceneManager::getInstance()->changeScene(new SceneHighScore());
	}

	if (btnOptions->OnClick()) {
		SceneManager::getInstance()->changeScene(new SceneOptions());
	}

	if (btnQuit->OnClick()) {
		glfwSetWindowShouldClose(glfwGetCurrentContext(), GL_TRUE);
	}

	GraphicsManager::GetInstance()->UpdateLights(dt);
}

void SceneMainMenu::Render() {

	SceneBase::Render();

	GraphicsManager::GetInstance()->SetPerspectiveProjection(45, Application::windowWidth() / Application::windowHeight(), 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Setup 2D pipeline then render 2D
	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	textManager.dequeueMesh();

	float winHeight = (float)Application::windowHeight() / 10;
	float winWidth = (float)Application::windowWidth() / 10;

	textManager.RenderMeshOnScreen(meshList[GEO_MAINMENU], winWidth * 0.5f, winHeight * 0.5f, Vector3(90, 0, 0), Vector3(winWidth / 2, 1, winHeight / 2));

	textManager.renderTextOnScreen(Vector3(0.45f, 0.7f, 0), Vector3(2, 2, 1), "Robot Forest", Color(1, 1, 1));

	m_GUI.RenderButtons();

	textManager.dequeueText();
	textManager.RenderTimedMeshOnScreen();
	textManager.reset(); // Must be called at the end of Render()
}

void SceneMainMenu::Exit()
{
	m_GUI.Exit();

	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();
	playerInfo->DetachCamera();

	playerInfo->DropInstance();

	goManager.Exit();

	GraphicsManager::GetInstance()->RemoveLight("lights[0]");

	SceneBase::Exit();
}
