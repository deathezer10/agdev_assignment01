#pragma once

#include "SceneBase.h"
#include "Mtx44.h"
#include "Mesh.h"
#include "MatrixStack.h"

class TextEntity;
class Light;
class Tank;

class SceneText : public SceneBase
{
public:
	SceneText();
	~SceneText();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

	float m_FOV = 45.f;
	float m_ScopedFOV = 0;

	Tank * theTank;
	Tank * theCannon;

private:
	TextEntity* textObj[3];

	float m_NextTextDisappearTime = 0;
	bool b_IsGameComplete = false;

	float m_lifeTimeTank = 20.f;
	bool b_countDowntank = false;
	bool b_isTankBlowApart = false;
	bool b_isTranslated = false;
	float m_translationTime = 0.f;
	const float TRANSLATION_END = 2.0f;
	bool b_isGone = false;

	int m_RobotCount = 0; // amount of robot alive

	float m_nextEnemySpawnTime = 5.f;
	float ENEMY_SPAWN_COOLDOWN = 5.f;
	int m_spawnCounter = 0;
	float m_decreaseSpawnTime = 0.5f;
	bool b_spawnTimeDecrease = false;

	GUIButton* btnResume;
	GUIButton* btnMainMenu;
	GUIButton* btnQuitGame;

	void SaveHighScore(int score);
	
};