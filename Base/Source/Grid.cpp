#include "Grid.h"
#include "stdio.h"
#include "MeshBuilder.h"
#include "RenderHelper.h"
#include "MatrixStack.h"
#include "SceneManager.h"
#include "SceneNode.h"
#include "EmptyObject.h"
#include "SpatialPartition.h"


/********************************************************************************
Constructor
********************************************************************************/
CGrid::CGrid(void)
	: index(Vector3(-1, -1, -1))
	, size(Vector3(-1, -1, -1))
	, offset(Vector3(-1, -1, -1))
	, m_min(Vector3(-1, -1, -1))
	, m_max(Vector3(-1, -1, -1))
	, theMesh(NULL)
	// , ListOfObjects(NULL)
	, theDetailLevel(CLevelOfDetails::NO_DETAILS)
	, SceneNode("", new EmptyObject(SceneManager::getInstance()->getCurrentScene()))
{
}

/********************************************************************************
Destructor
********************************************************************************/
CGrid::~CGrid(void)
{
	if (theMesh)
	{
		delete theMesh;
		theMesh = NULL;
	}
	Remove();
}

/********************************************************************************
Initialise this grid
********************************************************************************/
void CGrid::Init(const int xIndex, const int zIndex,
	const int xGridSize, const int zGridSize,
	const float xOffset, const float zOffset)
{
	name = std::to_string(xIndex) + ':' + std::to_string(zIndex);
	index.Set((float)xIndex, 0, (float)zIndex);
	size.Set((float)xGridSize, 0, (float)zGridSize);
	offset.Set(xOffset, 0, zOffset);
	m_min.Set(index.x * size.x - offset.x, 0.0f, index.z * size.z - offset.z);
	m_max.Set(index.x * size.x - offset.x + xGridSize, 0.0f, index.z * size.z - offset.z + zGridSize);
}

/********************************************************************************
 Set a particular grid's Mesh
********************************************************************************/
void CGrid::SetMesh(Mesh * aMesh)
{
	theMesh = aMesh;
}

/********************************************************************************
Update the grid
********************************************************************************/
void CGrid::Update(vector<GameObject*>* migrationList)
{

	int xIndex = (int)index.x;
	int zIndex = (int)index.z;

	// Handle Collision
	// Using neighbour grid in case the collider is too big
	auto container = CSpatialPartition::GetInstance()->GetObjectsWithNeighbour(xIndex, zIndex);

	for (auto it = container.begin(); it != container.end(); ++it) {
		GameObject* obj = (*it)->GetOBJ();

		for (auto it2 = it + 1; it2 != container.end(); ++it2) {
			GameObject* obj2 = (*it2)->GetOBJ();

			obj->getCollider().HandleCollision(obj2->getCollider());
		}
	}

	// Check each object to see if they are no longer in this grid
	std::vector<SceneNode*>::iterator it;
	it = m_Child.begin();
	while (it != m_Child.end())
	{

		GameObject* obj = (*it)->GetOBJ();

		//// Update physics only when enabled
		//if (obj->b_EnablePhysics) {
		//	// Apply velocity, angular acceleration, momentum, etc
		//	obj->UpdatePhysics();
		//}

		Vector3 position = (*it)->GetOBJ()->getCollider().getPosition();
		if (((m_min.x > position.x) || (position.x >= m_max.x)) ||
			((m_min.z > position.z) || (position.z >= m_max.z)))
		{
			migrationList->push_back((*it)->GetOBJ());

			// Remove from this Grid
			it = m_Child.erase(it);
		}
		else
		{
			// Move on otherwise
			++it;
		}

	}
}

/********************************************************************************
RenderScene
********************************************************************************/
void CGrid::Render(void)
{

	// Parent modelstack has been set to identity
	MS* modelStack = SceneManager::getInstance()->getCurrentScene()->modelStack;

	for (auto &i : m_Child) {
		if (i->GetOBJ()->active) {

			modelStack->PushMatrix();
			i->Render();
			modelStack->PopMatrix();

		}
	}
}


/********************************************************************************
Add a new object to this grid
********************************************************************************/
void CGrid::Add(GameObject* theObject)
{
	for (size_t i = 0; i < m_Child.size(); ++i)
	{
		if (m_Child[i]->GetOBJ() == theObject)
			return;
	}
	theObject->m_grid = this;
	AddChild(theObject);
}

/********************************************************************************
 Remove but not delete object from this grid
********************************************************************************/
void CGrid::Remove(void)
{
	for (size_t i = 0; i < m_Child.size(); i++)
	{
		// Do not delete the objects as they are stored in EntityManager and will be deleted there.
		//delete ListOfObjects[i];
		m_Child[i] = nullptr;
	}
	m_Child.clear();
}

/********************************************************************************
 Remove but not delete an object from this grid
********************************************************************************/
bool CGrid::Remove(GameObject* theObject)
{
	// Clean up entities that are done
	//std::vector<SceneNode*>::iterator it, end;
	//it = m_Child.begin();
	//end = m_Child.end();
	//while (it != end)
	//{
	//	if ((*it) == theObject)
	//	{
	//		it = m_Child.erase(it);
	//		return true;
	//	}
	//	else
	//	{
	//		// Move on otherwise
	//		++it;
	//	}
	//}
	return false;
}

/********************************************************************************
 Check if an object is in this grid
********************************************************************************/
bool CGrid::IsHere(GameObject* theObject) const
{
	for (size_t i = 0; i < m_Child.size(); ++i)
	{
		if (m_Child[i]->GetOBJ() == theObject)
			return true;
	}
	return false;
}

/********************************************************************************
PrintSelf
********************************************************************************/
void CGrid::PrintSelf()
{
	cout << "CGrid::PrintSelf()" << endl;
	cout << "\tIndex\t:\t" << index << "\t\tOffset\t:\t" << offset << endl;
	cout << "\tMin\t:\t" << m_min << "\tMax\t:\t" << m_max << endl;
	if (m_Child.size() > 0)
	{
		cout << "\tList of objects in this grid: (LOD:" << this->theDetailLevel << ")" << endl;
		cout << "\t------------------------------------------------------------------------" << endl;
	}
	for (size_t i = 0; i < m_Child.size(); ++i)
	{
		cout << "\t" << i << "\t:\t" << m_Child[i]->GetOBJ()->pos << endl;
	}
	if (m_Child.size() > 0)
		cout << "\t------------------------------------------------------------------------" << endl;
	cout << "********************************************************************************" << endl;
}

void CGrid::SetDetailLevel(const CLevelOfDetails::DETAIL_LEVEL theDetailLevel)
{
	this->theDetailLevel = theDetailLevel;

	if ((m_Child.size() > 0) && (theDetailLevel == 0))
	{
		// Put a break-point here to trace and see that the entities in this CGrid are set to NO_DETAILS
		int a = 0;
	}
	// Check each object to see if they are no longer in this grid
	auto container = GetAllChild();
	for (auto &it : container) {
		GameObject* obj = static_cast<GameObject*>(it);
		if (obj->GetLODStatus() == true || theDetailLevel == CLevelOfDetails::DETAIL_LEVEL::NO_DETAILS) {
			obj->SetDetailLevel(theDetailLevel);
		}
	}
}

