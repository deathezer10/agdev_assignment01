#include "Collider.h"
#include "GameObject.h"
#include "Scene.h"
#include "MeshBuilder.h"


Collider::Collider(Vector3* pos, float width, float height, float depth) {
	position = pos;
	innerBoxSize.Set(width, height, depth);
}

Collider::Collider(GameObject* obj, float width, float height, float depth, COLLIDER_TYPE type) {
	_obj = obj;
	position = &obj->pos;
	innerBoxSize.Set(width, height, depth);

	this->type = type;
}

Vector3 Collider::getInnerBoxMin() const {
	return Vector3(
		(-innerBoxSize.x / 2) + position->x + offset.x,
		(-innerBoxSize.y / 2) + position->y + offset.y,
		(-innerBoxSize.z / 2) + position->z + offset.z
	);
}

Vector3 Collider::getInnerBoxMax() const {
	return Vector3(
		(innerBoxSize.x / 2) + position->x + offset.x,
		(innerBoxSize.y / 2) + position->y + offset.y,
		(innerBoxSize.z / 2) + position->z + offset.z
	);
}

Vector3 Collider::getOuterBoxMin() const {
	return Vector3(
		(-outerBoxSize.x / 2) + position->x + offset.x,
		(-outerBoxSize.y / 2) + position->y + offset.y,
		(-outerBoxSize.z / 2) + position->z + offset.z
	);
}

Vector3 Collider::getOuterBoxMax() const {
	return Vector3(
		(outerBoxSize.x / 2) + position->x + offset.x,
		(outerBoxSize.y / 2) + position->y + offset.y,
		(outerBoxSize.z / 2) + position->z + offset.z
	);
}

bool Collider::GetIntersection(const float fDst1, const float fDst2, Vector3 P1, Vector3 P2, Vector3* Hit) {
	if ((fDst1 * fDst2) >= 0.0f)
		return false;
	if (fDst1 == fDst2)
		return false;
	*Hit = P1 + (P2 - P1) * (-fDst1 / (fDst2 - fDst1));
	return true;
}

bool Collider::InBox(Vector3 Hit, Vector3 B1, Vector3 B2, const int Axis) {
	if (Axis == 1 && Hit.z > B1.z && Hit.z < B2.z && Hit.y > B1.y && Hit.y < B2.y) return true;
	if (Axis == 2 && Hit.z > B1.z && Hit.z < B2.z && Hit.x > B1.x && Hit.x < B2.x) return true;
	if (Axis == 3 && Hit.x > B1.x && Hit.x < B2.x && Hit.y > B1.y && Hit.y < B2.y) return true;
	return false;
}

void Collider::HandleCollision(Collider &other) {

	// AABB vs AABB
	if (type == C_AABB && other.type == C_AABB) {

		// Check OuterBox for collision
		if (!outerBoxSize.IsZero() && !other.outerBoxSize.IsZero()) { // Hierarchy vs Hierarchy

			if (AABBvsAABB(getOuterBoxMin(), getOuterBoxMax(), other.getOuterBoxMin(), other.getOuterBoxMax())) {

				// Dont forget to check my own inner box
				if (AABBvsAABB(getInnerBoxMin(), getInnerBoxMax(), other.getInnerBoxMin(), other.getInnerBoxMax())) {
					_obj->OnCollisionHit(other._obj);
					other._obj->OnCollisionHit(_obj);
				}

				// Compare children vs children collision
				auto container1 = _obj->GetAllChild();
				auto container2 = other._obj->GetAllChild();

				for (auto &child : container1) {
					for (auto &otherChild : container2) {
						child->GetOBJ()->getCollider().HandleCollision(otherChild->GetOBJ()->getCollider());
					}
				}

			}

		}
		else if (outerBoxSize.IsZero() && other.outerBoxSize.IsZero()) { // Linear vs Linear
			if (AABBvsAABB(getInnerBoxMin(), getInnerBoxMax(), other.getInnerBoxMin(), other.getInnerBoxMax())) {
				_obj->OnCollisionHit(other._obj);
				other._obj->OnCollisionHit(_obj);
			}
		}
		else { // Linear vs Hierarchy

			Collider go1 = *this;
			Collider go2 = other;

			// Let go1 always be Linear, go2 = Hierarchy
			if (go2.outerBoxSize.IsZero())
				std::swap(go1, go2);

			// Only start checking if outerbox touched innerbox
			if (AABBvsAABB(go1.getInnerBoxMin(), go1.getInnerBoxMax(), go2.getOuterBoxMin(), go2.getOuterBoxMax())) {

				// cout << "Inside box" << endl;

				// Dont forget to check go2 own inner box
				if (AABBvsAABB(go1.getInnerBoxMin(), go1.getInnerBoxMax(), go2.getInnerBoxMin(), go2.getInnerBoxMax())) {
					go1._obj->OnCollisionHit(go2._obj);
					go2._obj->OnCollisionHit(go1._obj);
				}

				auto container = go2._obj->GetAllChild();

				for (auto &child : container) {
					child->GetOBJ()->getCollider().HandleCollision(go1);
				}
			}
		}
	}


	Collider go1 = *this;
	Collider go2 = other;

	// Let go1 always be C_LINE, go2 = C_AABB
	if (go2.type != C_AABB && go1.type != C_LINE)
		std::swap(go1, go2);

	if (go1.type == C_LINE && go2.type == C_AABB) {

		// Line vs Box
		if (go2.outerBoxSize.IsZero()) {

			Vector3* hitDirection = &Vector3(0, 0, 0);

			if ((GetIntersection(go1.lineStart.x - go2.getInnerBoxMin().x, go1.lineEnd.x - go2.getInnerBoxMin().x, go1.lineStart, go1.lineEnd, hitDirection) &&
				InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 1))
				|| (GetIntersection(go1.lineStart.y - go2.getInnerBoxMin().y, go1.lineEnd.y - go2.getInnerBoxMin().y, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 2))
				|| (GetIntersection(go1.lineStart.z - go2.getInnerBoxMin().z, go1.lineEnd.z - go2.getInnerBoxMin().z, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 3))
				|| (GetIntersection(go1.lineStart.x - go2.getInnerBoxMax().x, go1.lineEnd.x - go2.getInnerBoxMax().x, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 1))
				|| (GetIntersection(go1.lineStart.y - go2.getInnerBoxMax().y, go1.lineEnd.y - go2.getInnerBoxMax().y, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 2))
				|| (GetIntersection(go1.lineStart.z - go2.getInnerBoxMax().z, go1.lineEnd.z - go2.getInnerBoxMax().z, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 3))) {

				// Line Hit
				go1._obj->OnCollisionHit(go2._obj);
				go2._obj->OnCollisionHit(go1._obj);

			}
		}
		else { // Line vs Hierarchy

			 // Only start checking if line touched outerbox
			Vector3* hitDirection = &Vector3(0, 0, 0);

			if ((GetIntersection(go1.lineStart.x - go2.getOuterBoxMin().x, go1.lineEnd.x - go2.getOuterBoxMin().x, go1.lineStart, go1.lineEnd, hitDirection) &&
				InBox(*hitDirection, go2.getOuterBoxMin(), go2.getOuterBoxMax(), 1))
				|| (GetIntersection(go1.lineStart.y - go2.getOuterBoxMin().y, go1.lineEnd.y - go2.getOuterBoxMin().y, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getOuterBoxMin(), go2.getOuterBoxMax(), 2))
				|| (GetIntersection(go1.lineStart.z - go2.getOuterBoxMin().z, go1.lineEnd.z - go2.getOuterBoxMin().z, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getOuterBoxMin(), go2.getOuterBoxMax(), 3))
				|| (GetIntersection(go1.lineStart.x - go2.getOuterBoxMax().x, go1.lineEnd.x - go2.getOuterBoxMax().x, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getOuterBoxMin(), go2.getOuterBoxMax(), 1))
				|| (GetIntersection(go1.lineStart.y - go2.getOuterBoxMax().y, go1.lineEnd.y - go2.getOuterBoxMax().y, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getOuterBoxMin(), go2.getOuterBoxMax(), 2))
				|| (GetIntersection(go1.lineStart.z - go2.getOuterBoxMax().z, go1.lineEnd.z - go2.getOuterBoxMax().z, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getOuterBoxMin(), go2.getOuterBoxMax(), 3))) {
				
				hitDirection = &Vector3(0, 0, 0);

				// Dont forget to check go2 own inner box
				if (((GetIntersection(go1.lineStart.x - go2.getInnerBoxMin().x, go1.lineEnd.x - go2.getInnerBoxMin().x, go1.lineStart, go1.lineEnd, hitDirection) &&
					InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 1))
					|| (GetIntersection(go1.lineStart.y - go2.getInnerBoxMin().y, go1.lineEnd.y - go2.getInnerBoxMin().y, go1.lineStart, go1.lineEnd, hitDirection) &&
						InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 2))
					|| (GetIntersection(go1.lineStart.z - go2.getInnerBoxMin().z, go1.lineEnd.z - go2.getInnerBoxMin().z, go1.lineStart, go1.lineEnd, hitDirection) &&
						InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 3))
					|| (GetIntersection(go1.lineStart.x - go2.getInnerBoxMax().x, go1.lineEnd.x - go2.getInnerBoxMax().x, go1.lineStart, go1.lineEnd, hitDirection) &&
						InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 1))
					|| (GetIntersection(go1.lineStart.y - go2.getInnerBoxMax().y, go1.lineEnd.y - go2.getInnerBoxMax().y, go1.lineStart, go1.lineEnd, hitDirection) &&
						InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 2))
					|| (GetIntersection(go1.lineStart.z - go2.getInnerBoxMax().z, go1.lineEnd.z - go2.getInnerBoxMax().z, go1.lineStart, go1.lineEnd, hitDirection) &&
						InBox(*hitDirection, go2.getInnerBoxMin(), go2.getInnerBoxMax(), 3)))) {
					go1._obj->OnCollisionHit(go2._obj);
					go2._obj->OnCollisionHit(go1._obj);
				}

				auto container = go2._obj->GetAllChild();

				for (auto &child : container) {
					child->GetOBJ()->getCollider().HandleCollision(go1);
				}
			}

		}

	}


}


bool Collider::AABBvsAABB(const Collider &other) {
	return AABBvsAABB(getInnerBoxMin(), getInnerBoxMax(), other.getInnerBoxMin(), other.getInnerBoxMax());
}

bool Collider::AABBvsAABB(const Vector3 &firstMin, const Vector3 &firstMax, const Vector3 &secondMin, const Vector3 &secondMax) {

	Vector3 myMin = firstMin;
	Vector3 myMax = firstMax;

	Vector3 otherMin = secondMin;
	Vector3 otherMax = secondMax;

	// Check if the box intercepts
	if (myMax.x > otherMin.x && myMin.x < otherMax.x &&
		myMax.y > otherMin.y && myMin.y < otherMax.y &&
		myMax.z > otherMin.z && myMin.z < otherMax.z) {
		return true;
	}
	else {
		return false;
	}
}