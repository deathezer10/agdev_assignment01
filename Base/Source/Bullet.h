#ifndef BULLET_H
#define BULLET_H


#include "GameObject.h"

class Bullet : public GameObject {
	
public:
	enum WHOSEBULLET
	{
		SNIPER_BULLET,
		ROCKET_BULLET,
		TANK_BULLET,
	};


	Bullet(Scene* scene, Vector3 pos, Vector3 dir, bool isEnemyProjectile);
	~Bullet() {};

	virtual bool Update();

	//return bullet damage based on which bullet is it
	int RetrieveBulletDamage(WHOSEBULLET _bulletOwner);
private:
	int m_BulletDamage;
	float m_BulletSpeed;
	bool m_EnemyProjectile;

	float m_BulletMaxDistance;
	float m_CurrentLifeTime = 0;

	Vector3 m_Direction;

	virtual void OnCollisionHit(GameObject* other);

};
#endif