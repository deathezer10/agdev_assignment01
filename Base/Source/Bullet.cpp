#include <map>
#include <iterator>
#include "Scene.h"
#include "Bullet.h"
#include "SpatialPartition.h"
#include "Turret.h"
#include "LuaInterface.h"

using std::multimap;

// Player Bullet
Bullet::Bullet(Scene* scene, Vector3 pos, Vector3 dir, bool isEnemyProjectile) : GameObject(scene, Scene::GEO_BULLET) {
	active = true;
	scale.Set(0.25f, 0.25f, 0.25f);
	// b_EnablePhysics = true;
	m_EnemyProjectile = isEnemyProjectile;

	this->pos = pos;
	CLuaInterface::GetInstance()->SetLuaState("PlayerSettings");
	m_BulletSpeed = CLuaInterface::GetInstance()->getFloatValue("rifleBulletSpeed");
	m_BulletMaxDistance = 250;

	m_Direction = dir.Normalized();
	collider.setBoundingBoxSize(Vector3(2, 2, 2));

	collider.type = Collider::C_AABB;
}


bool Bullet::Update() {

	pos += m_Direction * m_BulletSpeed * m_scene->_dt;
	// collider.setLineSize(pos, (pos + (m_Direction.Normalized() * 5)));

	m_CurrentLifeTime += m_scene->_dt;

	// Destroy object after a set time
	if (m_CurrentLifeTime >= 3) {
		active = false;
		m_scene->sceneGraph.FindAndDestroy(this);
		return false;
	}

	return true;
}

int Bullet::RetrieveBulletDamage(WHOSEBULLET _bulletOwner)
{
	CLuaInterface::GetInstance()->SetLuaState("PlayerSettings");

	switch (_bulletOwner)
	{
	case SNIPER_BULLET:
		m_BulletDamage = CLuaInterface::GetInstance()->getIntValue("sniperBulletDamage");
		break;

	case ROCKET_BULLET:
		m_BulletDamage = CLuaInterface::GetInstance()->getIntValue("rifleBulletDamage");
		break;
	}

	return m_BulletDamage;
}

void Bullet::OnCollisionHit(GameObject * other) {

	if (active) {
		active = false;

		Turret* m_turret = dynamic_cast<Turret*>(other);

		//Destroy both bullet and other object (trees, lamp post and etc Except turret)
		if (this->type == Scene::GEO_CANNON_BALL && m_turret == nullptr) {
			m_scene->sceneGraph.FindAndDestroy(other);
			m_scene->sceneGraph.FindAndDestroy(this);
		}
		else { // Destroy bullet only
			m_scene->sceneGraph.FindAndDestroy(this);
		}
	}

}
