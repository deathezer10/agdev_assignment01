#ifndef OBJ_COLLIDER_H
#define OBJ_COLLIDER_H

#include "Vector3.h"

class GameObject;

class Collider {

public:
	enum COLLIDER_TYPE {
		C_LINE = 0,
		C_AABB
	};

	Collider(Vector3* pos, float width, float height, float depth);
	Collider(GameObject* obj, float width, float height, float depth, COLLIDER_TYPE type = C_AABB);
	~Collider() {};

	COLLIDER_TYPE type;

	Vector3 innerBoxSize;
	Vector3 outerBoxSize;

	const Vector3 getPosition() { return (*position + offset); };

	// Does this Collider push the player back?
	bool isTrigger() { return _isTrigger; };

	// Toggles whether this Collider pushes the player's Camera back upon collision
	void setTrigger(bool toggle) { _isTrigger = toggle; };

	void setOffset(const Vector3 &offset) { this->offset = offset; };

	void setBoundingBoxSize(Vector3 size) { innerBoxSize = size; };

	void setOuterBoxSize(Vector3 size) { outerBoxSize = size; };

	void setLineSize(Vector3 lineStart, Vector3 lineEnd) { this->lineStart = lineStart; this->lineEnd = lineEnd; };

	void HandleCollision(Collider &other);

	bool AABBvsAABB(const Collider & other);

	bool AABBvsAABB(const Vector3 &firstMin, const Vector3 &firstMax, const Vector3 &secondMin, const Vector3 &secondMax);

	Vector3 getInnerBoxMin() const;
	Vector3 getInnerBoxMax() const;
	Vector3 getOuterBoxMin() const;
	Vector3 getOuterBoxMax() const;
	
private:
	Vector3* position;
	Vector3 offset;
	GameObject* _obj = nullptr;

	bool _isTrigger = false;
	
	Vector3 lineStart;
	Vector3 lineEnd;

	// Check where a line segment between two positions intersects a plane
	bool GetIntersection(const float fDst1, const float fDst2, Vector3 P1, Vector3 P2, Vector3* Hit);

	// Check two positions are within a box region
	bool InBox(Vector3 Hit, Vector3 B1, Vector3 B2, const int Axis);

};
#endif