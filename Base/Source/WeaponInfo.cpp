#include "WeaponInfo.h"
#include "KeyboardController.h"
#include "SceneManager.h"
#include "Bullet.h"
#include "Bullet2.h"
#include "SceneNode.h"

#include <iostream>
using namespace std;

CWeaponInfo::CWeaponInfo(string weaponName)
	: magRounds(1)
	, maxMagRounds(1)
	, totalRounds(8)
	, maxTotalRounds(8)
	, timeBetweenShots(0.5)
	, elapsedTime(0.0)
	, bFire(true)
	, s_WeaponName(weaponName)
{
}


CWeaponInfo::~CWeaponInfo()
{
}

// Set the number of ammunition in the magazine for this player
void CWeaponInfo::SetMagRound(const int magRounds)
{
	this->magRounds = magRounds;
}

// Set the maximum number of ammunition in the magazine for this weapon
void CWeaponInfo::SetMaxMagRound(const int magRounds)
{
	this->magRounds = magRounds;
}

// The current total number of rounds currently carried by this player
void CWeaponInfo::SetTotalRound(const int totalRounds)
{
	this->totalRounds = totalRounds;
}

// The max total number of rounds currently carried by this player
void CWeaponInfo::SetMaxTotalRound(const int maxTotalRounds)
{
	this->maxTotalRounds = maxTotalRounds;
}


// Get the number of ammunition in the magazine for this player
int CWeaponInfo::GetMagRound(void) const
{
	return magRounds;
}

// Get the maximum number of ammunition in the magazine for this weapon
int CWeaponInfo::GetMaxMagRound(void) const
{
	return maxMagRounds;
}

// Get the current total number of rounds currently carried by this player
int CWeaponInfo::GetTotalRound(void) const
{
	return totalRounds;
}

// Get the max total number of rounds currently carried by this player
int CWeaponInfo::GetMaxTotalRound(void) const
{
	return maxTotalRounds;
}

// Set the time between shots
void CWeaponInfo::SetTimeBetweenShots(const double timeBetweenShots)
{
	this->timeBetweenShots = timeBetweenShots;
}

// Set the firing rate in rounds per min
void CWeaponInfo::SetFiringRate(const int firingRate)
{
	timeBetweenShots = 60.0 / (double)firingRate;	// 60 seconds divided by firing rate
}

// Set the firing flag
void CWeaponInfo::SetCanFire(const bool bFire)
{
	this->bFire = bFire;
}

// Get the time between shots
double CWeaponInfo::GetTimeBetweenShots(void) const
{
	return timeBetweenShots;
}

// Get the firing rate
int CWeaponInfo::GetFiringRate(void) const
{
	return (int)(60.0 / timeBetweenShots);	// 60 seconds divided by timeBetweenShots
}

// Get the firing flag
bool CWeaponInfo::GetCanFire(void) const
{
	return bFire;
}

// Initialise this instance to default values
void CWeaponInfo::Init(void)
{
	// The number of ammunition in a magazine for this weapon
	magRounds = 1;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 1;
	// The current total number of rounds currently carried by this player
	totalRounds = 8;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 8;

	// The time between shots
	timeBetweenShots = 0.5;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;
}

// Update the elapsed time
void CWeaponInfo::Update(const double dt)
{
	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	elapsedTime += dt;
	if (elapsedTime > timeBetweenShots)
	{
		bFire = true;
		elapsedTime = 0.0;
	}
	if (isReloading) {
		_scene->textManager.queueRenderText(UIManager::Text{ "", Color(1,1,1), UIManager::ANCHOR_CENTER_CENTER });
		_scene->textManager.queueRenderText(UIManager::Text{ "", Color(1,1,1), UIManager::ANCHOR_CENTER_CENTER });
		_scene->textManager.queueRenderText(UIManager::Text{ "Reloading..", Color(1,1,1), UIManager::ANCHOR_CENTER_CENTER });

		if (_scene->m_elapsedTime >= nextReloadTime) {

			if (magRounds < maxMagRounds)
			{
				if (maxMagRounds - magRounds <= totalRounds)
				{
					totalRounds -= maxMagRounds - magRounds;
					magRounds = maxMagRounds;
				}
				else
				{
					magRounds += totalRounds;
					totalRounds = 0;
				}
			}
			if (currentClip > 0) {
				//magRounds = maxMagRounds;
				currentClip--;
			}

			isReloading = false;
		}
	}

	if (KeyboardController::GetInstance()->IsKeyDown('Q') == false) {
		b_IsReloading = false;
	}

}

// Discharge this weapon
void CWeaponInfo::Discharge(Vector3 position, Vector3 target, CPlayerInfo* _source)
{
	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	float eTime = SceneManager::getInstance()->getCurrentScene()->m_elapsedTime;

	if (bFire &&  b_IsReloading == false && isReloading == false) {
		// If there is still ammo in the magazine, then fire
		if (magRounds > 0 && eTime > nextShootTime) {

			if (s_WeaponName == "Pistol") {
				BulletTwo* bullet = new BulletTwo(_scene, position, target - position, false);
				bullet->CalculateAngles();
				_scene->goManager.CreateObject(bullet);
			}
			else {
				Bullet* bullet = new Bullet(_scene, position, target - position, false);
				_scene->goManager.CreateObject(bullet);
			}

			_source->mouse_diff_y = -50; // recoil effect lol
			bFire = false;

			if (b_IsInfiniteAmmo == false)
				magRounds--;

			nextShootTime = (float)timeBetweenShots + eTime;
		}
	}
}

// Reload this weapon
void CWeaponInfo::Reload(void) {
	if (isReloading || magRounds >= maxMagRounds || totalRounds == 0)
		return;

	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	bFire = false;
	isReloading = true;
	nextReloadTime = _scene->m_elapsedTime + reloadTime;
}

// Add rounds
void CWeaponInfo::AddRounds(const int newRounds)
{
	if (totalRounds + newRounds > maxTotalRounds)
		totalRounds = maxTotalRounds;
	else
		totalRounds += newRounds;
}

void CWeaponInfo::ReloadClip(float dt) {

	if (totalRounds >= maxTotalRounds || isReloading)
		return;

	b_IsReloading = true;

	currentClipReloadTime += dt;

	SceneManager::getInstance()->getCurrentScene()->textManager.queueRenderText(UIManager::Text{ "", Color(0,1,0), UIManager::ANCHOR_CENTER_CENTER });
	SceneManager::getInstance()->getCurrentScene()->textManager.queueRenderText(UIManager::Text{ "", Color(0,1,0), UIManager::ANCHOR_CENTER_CENTER });

	stringstream text;
	text << "Refilling in " << (int)(5 - currentClipReloadTime);
	SceneManager::getInstance()->getCurrentScene()->textManager.queueRenderText(UIManager::Text{ text.str(), Color(0,1,0), UIManager::ANCHOR_CENTER_CENTER });

	if (currentClipReloadTime >= 4) {
		totalRounds += maxMagRounds;
		totalRounds = Math::Clamp(totalRounds, 0, maxTotalRounds);

		currentClipReloadTime = 0;
	}
}

// Print Self
void CWeaponInfo::PrintSelf(void)
{
	cout << "CWeaponInfo::PrintSelf()" << endl;
	cout << "========================" << endl;
	cout << "magRounds\t\t:\t" << magRounds << endl;
	cout << "maxMagRounds\t\t:\t" << maxMagRounds << endl;
	cout << "totalRounds\t\t:\t" << totalRounds << endl;
	cout << "maxTotalRounds\t\t:\t" << maxTotalRounds << endl;
	cout << "timeBetweenShots\t:\t" << timeBetweenShots << endl;
	cout << "currentClip\t\t:\t" << currentClip << endl;
	cout << "elapsedTime\t\t:\t" << elapsedTime << endl;
	cout << "bFire\t\t\t:\t" << bFire << endl;
}
