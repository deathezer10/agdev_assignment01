#include "SceneBase.h"
#include "GraphicsManager.h"
#include "ShaderProgram.h"
#include "MeshBuilder.h"
#include "LoadTGA.h"
#include "GL\glew.h"
#include "Light.h"
#include "LuaInterface.h"
#include "Application.h"
#include "SceneText.h"

void SceneBase::Init()
{

	GraphicsManager::GetInstance()->Init();

	modelStack = &GraphicsManager::GetInstance()->GetModelStack();

	currProg = GraphicsManager::GetInstance()->LoadShader("default", "Shader//shadow.vertexshader", "Shader//shadow.fragmentshader");

	// Tell the shader program to store these uniform locations
	currProg->AddUniform("MVP");
	currProg->AddUniform("MV");
	currProg->AddUniform("MV_inverse_transpose");
	currProg->AddUniform("material.kAmbient");
	currProg->AddUniform("material.kDiffuse");
	currProg->AddUniform("material.kSpecular");
	currProg->AddUniform("material.kShininess");
	currProg->AddUniform("lightEnabled");
	currProg->AddUniform("numLights");
	currProg->AddUniform("lights[0].type");
	currProg->AddUniform("lights[0].position_cameraspace");
	currProg->AddUniform("lights[0].color");
	currProg->AddUniform("lights[0].power");
	currProg->AddUniform("lights[0].kC");
	currProg->AddUniform("lights[0].kL");
	currProg->AddUniform("lights[0].kQ");
	currProg->AddUniform("lights[0].spotDirection");
	currProg->AddUniform("lights[0].cosCutoff");
	currProg->AddUniform("lights[0].cosInner");
	currProg->AddUniform("lights[0].exponent");
	currProg->AddUniform("lights[1].type");
	currProg->AddUniform("lights[1].position_cameraspace");
	currProg->AddUniform("lights[1].color");
	currProg->AddUniform("lights[1].power");
	currProg->AddUniform("lights[1].kC");
	currProg->AddUniform("lights[1].kL");
	currProg->AddUniform("lights[1].kQ");
	currProg->AddUniform("lights[1].spotDirection");
	currProg->AddUniform("lights[1].cosCutoff");
	currProg->AddUniform("lights[1].cosInner");
	currProg->AddUniform("lights[1].exponent");
	currProg->AddUniform("colorTextureEnabled");
	currProg->AddUniform("colorTexture");
	currProg->AddUniform("textEnabled");
	currProg->AddUniform("textColor");
	currProg->AddUniform("currentAlpha");
	currProg->AddUniform("currentColor");
	// Tell the graphics manager to use the shader we just loaded
	GraphicsManager::GetInstance()->SetActiveShader("default");

	glEnable(GL_DEPTH_TEST);
	// Enable blend mode
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	lights[0] = new Light();
	GraphicsManager::GetInstance()->AddLight("lights[0]", lights[0]);
	lights[0]->type = Light::LIGHT_DIRECTIONAL;
	lights[0]->position.Set(0, 20, 0);
	lights[0]->color.Set(1, 1, 1);
	lights[0]->power = 0.7f;
	lights[0]->name = "lights[0]";

	currProg->UpdateInt("numLights", 1);
	currProg->UpdateInt("colorTexture", 1);
	currProg->UpdateInt("textEnabled", 1);

	meshList[GEO_TEXT] = MeshBuilder::GetInstance()->GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureID = LoadTGA("Image//arial.tga");
	textManager.LoadFontWidth("Image//arial.csv");

	meshList[GEO_BUTTON1_HOVER] = MeshBuilder::GenerateSphere("GEO_BUTTON1_HOVER", Color(.5f, .5f, .5f), 16, 16, 0.5f);
	meshList[GEO_BUTTON1_NORMAL] = MeshBuilder::GenerateSphere("GEO_BUTTON1_NORMAL", Color(0.2f, 0.2f, 0.2f), 16, 16, 0.5f);
	m_GUI.SetButtonMesh(meshList[GEO_BUTTON1_NORMAL], meshList[GEO_BUTTON1_HOVER], &textManager);

	Application::SetIsPaused(false);
}

void SceneBase::Update(double dt) {
	_dt = (float)dt;
	m_elapsedTime += _dt;

	auto gameplayScene = dynamic_cast<SceneText*>(this);

	if (gameplayScene) {

		if (Application::IsPaused())
			m_GUI.UpdateButtons();

	}
	else
		m_GUI.UpdateButtons();
}

void SceneBase::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GraphicsManager::GetInstance()->UpdateLightUniforms();
}

void SceneBase::Exit()
{

	glDeleteProgram(currProg->GetProgramID());
	glDeleteVertexArrays(1, &GraphicsManager::GetInstance()->vertexArrayID);
}
