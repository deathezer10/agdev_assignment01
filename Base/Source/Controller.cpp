#include "Controller.h"
#include <iostream>
#include <utility>
#include <string>

using namespace std;

CController::CController()
{
}

CController::~CController()
{
	// We just set thePlayerInfo to NULL without deleting. SceneText will delete this.
	if (thePlayerInfo)
		thePlayerInfo = NULL;
}



// Create this controller
bool CController::Create(CPlayerInfo* thePlayerInfo)
{
	cout << "CController::Create()" << endl;
	this->thePlayerInfo = thePlayerInfo;
	return false;
}

// Read from the controller
int CController::Read(const float deltaTime)
{
	// cout << "CController::Read()" << endl;
	return 0;
}

// Detect and process front / back movement on the controller
bool CController::Move_FrontBack(const float deltaTime, const bool direction, const float speedMultiplier)
{
	if (thePlayerInfo)
	{
		thePlayerInfo->Move_FrontBack(deltaTime, direction, speedMultiplier);
	}
	return false;
}

// Detect and process left / right movement on the controller
bool CController::Move_LeftRight(const float deltaTime, const bool direction)
{
	if (thePlayerInfo)
	{
		thePlayerInfo->Move_LeftRight(deltaTime, direction, 1);
	}
	return false;
}

// Detect and process look up / down on the controller
bool CController::Look_UpDown(const float deltaTime, const bool direction)
{
	return false;
}

// Detect and process look left / right on the controller
bool CController::Look_LeftRight(const float deltaTime, const bool direction)
{
	return false;
}


// Reload current weapon
bool CController::Reload(const float deltaTime)
{
	cout << "CController::Reload()" << endl;
	return false;
}

// Change current weapon (for primary only)
bool CController::Change(const float deltaTime)
{
	cout << "CController::Change()" << endl;
	return false;
}

// Fire primary weapon
bool CController::FirePrimary(const float deltaTime)
{
	cout << "CController::FirePrimary()" << endl;
	return false;
}

// Fire secondary weapon
bool CController::FireSecondary(const float deltaTime)
{
	cout << "CController::FireSecondary()" << endl;
	return false;
}