#include "Application.h"

#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "SceneText.h"

#include "shader.hpp"
#include "MeshBuilder.h"
#include "Utility.h"
#include "LoadTGA.h"
#include <sstream>
#include "KeyboardController.h"
#include "MouseController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "Keyboard.h"
#include "EmptyObject.h"
#include "SpatialPartition.h"

#include "Light.h"
#include "RenderHelper.h"
#include "SceneNode.h"
#include "SceneMainMenu.h"
#include "Tank.h"
#include "Robot.h"
#include "RobotBase.h"
#include "WayPointManager.h"
#include "LuaInterface.h"

#include <iostream>
#include <functional>

using namespace std;


SceneText::SceneText()
{
}

SceneText::~SceneText()
{
}

void SceneText::Init()
{
	SceneBase::Init();

	// Create the playerinfo instance, which manages all information about the player
	CLuaInterface::GetInstance()->SetLuaState("PlayerSettings");
	CLuaInterface::GetInstance()->LoadPlayer();


	// Create and attach the camera to the scene
	camera.Init(playerInfo->GetPos(), playerInfo->GetTarget(), playerInfo->GetUp());
	playerInfo->AttachCamera(&camera);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Load all the meshes
	meshList[GEO_BOXCOLLIDER] = MeshBuilder::GenerateCube("GEO_BOXCOLLIDER", Color(0, 1, 0), .5f, .5f, .5f);
	meshList[GEO_OUTERBOXCOLLIDER] = MeshBuilder::GenerateCube("GEO_OUTERBOXCOLLIDER", Color(0, 1, 1), .5f, .5f, .5f);

	meshList[GEO_GRID_SELECTED] = MeshBuilder::GenerateRadarQuad("GEO_GRID_SELECTED", Color(0.8f, 0.8f, 0), 0.49f, 0.49f);
	meshList[GEO_GRID_UNSELECTED] = MeshBuilder::GenerateRadarQuad("GEO_GRID_UNSELECTED", Color(0.2f, 0.2f, 0.2f), 0.49f, 0.49f);

	meshList[GEO_LOD_HIGH] = MeshBuilder::GenerateCube("GEO_LOD_HIGH", Color(0, 1, 0));
	meshList[GEO_LOD_MED] = MeshBuilder::GenerateCube("GEO_LOD_MED", Color(1, 1, 0));
	meshList[GEO_LOD_LOW] = MeshBuilder::GenerateCube("GEO_LOD_LOW", Color(1, 0, 0));

	// Load skybox depending on settings
	CLuaInterface::GetInstance()->SetLuaState("PlayerControls");

	meshList[GEO_SKYPLANE] = MeshBuilder::GenerateSkyPlane("GEO_SKYPLANE", Color(1, 1, 1), 32, 100.f, 1000.f, 1.f, 1.f);

	if (CLuaInterface::GetInstance()->getIntValue("Skybox") == 0) {
		meshList[GEO_SKYPLANE]->textureID = LoadTGA("Image//SkyBox//top.tga");
	}
	else {
		meshList[GEO_SKYPLANE]->textureID = LoadTGA("Image//SkyBox//topV2.tga");
	}

	CLuaInterface::GetInstance()->SetLuaState("");

	meshList[GEO_CROSSHAIR] = MeshBuilder::GenerateRadarQuad("crosshair", Color(1, 1, 1));
	meshList[GEO_CROSSHAIR]->textureID = LoadTGA("Image/crosshair.tga", false);

	meshList[GEO_RADAR_BACKGROUND] = MeshBuilder::GenerateRadarQuad("radar bg", Color(0, 0.5f, 0));
	meshList[GEO_RADAR_BACKGROUND]->textureID = LoadTGA("Image/radar.tga", true);

	meshList[GEO_SELECTOR] = MeshBuilder::GenerateRadarQuad("selector", Color(1, 1, 1));
	meshList[GEO_SELECTOR]->textureID = LoadTGA("Image/selector.tga", false);

	meshList[GEO_DAMAGED] = MeshBuilder::GenerateRadarQuad("damaged", Color(1, 1, 0));
	meshList[GEO_DAMAGED]->textureID = LoadTGA("Image/damaged.tga", false);

	meshList[GEO_HEALED] = MeshBuilder::GenerateRadarQuad("healed", Color(1, 1, 0));
	meshList[GEO_HEALED]->textureID = LoadTGA("Image/healed.tga", false);

	meshList[GEO_PANEL] = MeshBuilder::GenerateQuad("panel", Color(1, 1, 1));

	meshList[GEO_RADAR_CASTLE] = MeshBuilder::GenerateRadarQuad("radar castle icon", Color(0, 1, 0));
	meshList[GEO_RADAR_ENEMY] = MeshBuilder::GenerateRadarQuad("radar enemy icon", Color(1, 0, 0));
	meshList[GEO_RADAR_PLAYER] = MeshBuilder::GenerateRadarTriangle("radar player icon", Color(0, 0, 1));

	meshList[GEO_HEALTHPACK] = MeshBuilder::GenerateOBJ("healthpack", "OBJ/healthpack.obj");
	meshList[GEO_HEALTHPACK]->textureID = LoadTGA("Image/healthpack.tga");

	meshList[GEO_TERRAIN] = MeshBuilder::GenerateQuad("terrain", Color(1, 1, 1), 5000, 500);
	meshList[GEO_TERRAIN]->textureID = LoadTGA("Image/grass.tga", true, true);

	meshList[GEO_BULLET] = MeshBuilder::GenerateSphere("bullet", Color(0.7f, 0.7f, 0.7f), 16, 16);
	//meshList[GEO_BULLET]->textureID = LoadTGA("Image/pistol.tga");
	meshList[GEO_CANNON_BALL] = MeshBuilder::GenerateSphere("cannonball", Color(0.7f, 0.7f, 0.7f), 16, 16);

	meshList[GEO_SCOPE] = MeshBuilder::GenerateRadarQuad("scope", Color(1, 1, 0));
	meshList[GEO_SCOPE]->textureID = LoadTGA("Image/scope.tga");

	meshList[GEO_ROCKETLAUNCER] = MeshBuilder::GenerateOBJ("rocketlauncher", "OBJ/rocketlauncher.obj");
	meshList[GEO_ROCKETLAUNCER]->textureID = LoadTGA("Image/rocketlauncher.tga");

	meshList[GEO_SNIPER] = MeshBuilder::GenerateOBJ("sniper", "OBJ/sniper.obj");
	meshList[GEO_SNIPER]->textureID = LoadTGA("Image/pistol.tga");

	meshList[GEO_ROCKET_SHELL] = MeshBuilder::GenerateOBJ("rocketshell", "OBJ/rocketshell.obj");
	meshList[GEO_ROCKET_SHELL]->textureID = LoadTGA("Image/rocketshell.tga");

	meshList[GEO_TURRET_ICON] = MeshBuilder::GenerateRadarQuad("GEO_TURRET_ICON", Color(1, 1, 1));
	meshList[GEO_TURRET_ICON]->textureID = LoadTGA("Image/turret_icon.tga");

	meshList[GEO_ROCKETLAUNCHER_ICON] = MeshBuilder::GenerateRadarQuad("rocketlauncher icon", Color(1, 1, 1));
	meshList[GEO_ROCKETLAUNCHER_ICON]->textureID = LoadTGA("Image/rocketlauncher_icon.tga");

	meshList[GEO_SNIPER_ICON] = MeshBuilder::GenerateRadarQuad("sniper icon", Color(1, 1, 1));
	meshList[GEO_SNIPER_ICON]->textureID = LoadTGA("Image/sniper_icon.tga");

	meshList[GEO_HIGH_LOD_TANK_BODY] = MeshBuilder::GenerateOBJ("highlodtankbody", "OBJ//Tank/HighDetailTankBody.obj");
	meshList[GEO_HIGH_LOD_TANK_BODY]->textureID = LoadTGA("Image/Tank.tga");
	meshList[GEO_HIGH_LOD_TANK_TURRET] = MeshBuilder::GenerateOBJ("highlodtankturret", "OBJ//Tank/HighDetailTankTurret.obj");
	meshList[GEO_HIGH_LOD_TANK_TURRET]->textureID = LoadTGA("Image/Tank.tga");

	meshList[GEO_MID_LOD_TANK_BODY] = MeshBuilder::GenerateOBJ("midlodtankbody", "OBJ//Tank/MidDetailTankBody.obj");
	meshList[GEO_MID_LOD_TANK_BODY]->textureID = LoadTGA("Image/Tank.tga");
	meshList[GEO_MID_LOD_TANK_TURRET] = MeshBuilder::GenerateOBJ("midlotankturret", "OBJ//Tank/MidDetailTankTurret.obj");
	meshList[GEO_MID_LOD_TANK_TURRET]->textureID = LoadTGA("Image/Tank.tga");

	meshList[GEO_LOW_LOD_TANK_BODY] = MeshBuilder::GenerateOBJ("lowlodtankbody", "OBJ//Tank/LowDetailTankBody.obj");
	meshList[GEO_LOW_LOD_TANK_BODY]->textureID = LoadTGA("Image/Tank.tga");
	meshList[GEO_LOW_LOD_TANK_TURRET] = MeshBuilder::GenerateOBJ("lowlodtankturret", "OBJ//Tank/LowDetailTankTurret.obj");
	meshList[GEO_LOW_LOD_TANK_TURRET]->textureID = LoadTGA("Image/Tank.tga");

	meshList[GEO_HIGH_LOD_TREE] = MeshBuilder::GenerateOBJ("highlodTree", "OBJ//Tree/HighDetailTreeTop.obj");
	meshList[GEO_HIGH_LOD_TREE]->textureID = LoadTGA("Image/Tree.tga");
	meshList[GEO_MID_LOD_TREE] = MeshBuilder::GenerateOBJ("midlodtree", "OBJ//Tree/MidDetailTreeTop.obj");
	meshList[GEO_MID_LOD_TREE]->textureID = LoadTGA("Image/Tree.tga");
	meshList[GEO_LOW_LOD_TREE] = MeshBuilder::GenerateOBJ("lowlodtree", "OBJ//Tree/LowDetailTreeTop.obj");
	meshList[GEO_LOW_LOD_TREE]->textureID = LoadTGA("Image/Tree.tga");

	meshList[GEO_HIGH_LOD_ROBOT_HEAD] = MeshBuilder::GenerateOBJ("highlodrobothead", "OBJ//Robot/HighDetailRobotHead.obj");
	meshList[GEO_HIGH_LOD_ROBOT_HEAD]->textureID = LoadTGA("Image/RobotColor.tga");
	meshList[GEO_HIGH_LOD_ROBOT_ARM] = MeshBuilder::GenerateOBJ("highlodrobotarm", "OBJ//Robot/HighDetailRobotArm.obj");
	meshList[GEO_HIGH_LOD_ROBOT_ARM]->textureID = LoadTGA("Image/RobotColor.tga");
	meshList[GEO_HIGH_LOD_ROBOT_BODY] = MeshBuilder::GenerateOBJ("highlodrobotbody", "OBJ//Robot/HighDetailRobotBody.obj");
	meshList[GEO_HIGH_LOD_ROBOT_BODY]->textureID = LoadTGA("Image/RobotColor.tga");

	meshList[GEO_MID_LOD_ROBOT_HEAD] = MeshBuilder::GenerateOBJ("GEO_MID_LOD_ROBOT_HEAD", "OBJ//Robot/MidDetailRobotHead.obj");
	meshList[GEO_MID_LOD_ROBOT_HEAD]->textureID = LoadTGA("Image/RobotColor.tga");
	meshList[GEO_MID_LOD_ROBOT_ARM] = MeshBuilder::GenerateOBJ("GEO_MID_LOD_ROBOT_ARM", "OBJ//Robot/MidDetailRobotArm.obj");
	meshList[GEO_MID_LOD_ROBOT_ARM]->textureID = LoadTGA("Image/RobotColor.tga");
	meshList[GEO_MID_LOD_ROBOT_BODY] = MeshBuilder::GenerateOBJ("GEO_MID_LOD_ROBOT_BODY", "OBJ//Robot/MidDetailRobotBody.obj");
	meshList[GEO_MID_LOD_ROBOT_BODY]->textureID = LoadTGA("Image/RobotColor.tga");

	meshList[GEO_LOW_LOD_ROBOT_HEAD] = MeshBuilder::GenerateOBJ("GEO_LOW_LOD_ROBOT_HEAD", "OBJ//Robot/LowDetailRobotHead.obj");
	meshList[GEO_LOW_LOD_ROBOT_HEAD]->textureID = LoadTGA("Image/RobotColor.tga");
	meshList[GEO_LOW_LOD_ROBOT_ARM] = MeshBuilder::GenerateOBJ("GEO_LOW_LOD_ROBOT_ARM", "OBJ//Robot/LowDetailRobotArm.obj");
	meshList[GEO_LOW_LOD_ROBOT_ARM]->textureID = LoadTGA("Image/RobotColor.tga");
	meshList[GEO_LOW_LOD_ROBOT_BODY] = MeshBuilder::GenerateOBJ("GEO_LOW_LOD_ROBOT_BODY", "OBJ//Robot/LowDetailRobotBody.obj");
	meshList[GEO_LOW_LOD_ROBOT_BODY]->textureID = LoadTGA("Image/RobotColor.tga");

	meshList[GEO_HIGH_LOD_ROCK] = MeshBuilder::GenerateOBJ("GEO_HIGH_LOD_LAMP_POST", "OBJ/Rock//HighDetailRock.obj");
	meshList[GEO_HIGH_LOD_ROCK]->textureID = LoadTGA("Image/rock.tga");
	meshList[GEO_MID_LOD_ROCK] = MeshBuilder::GenerateOBJ("GEO_MID_LOD_LAMP_POST", "OBJ//Rock//MidDetailRock.obj");
	meshList[GEO_MID_LOD_ROCK]->textureID = LoadTGA("Image/rock.tga");
	meshList[GEO_LOW_LOD_ROCK] = MeshBuilder::GenerateOBJ("GEO_LOW_LOD_LAMP_POST", "OBJ//Rock//LowDetailRock.obj");
	meshList[GEO_LOW_LOD_ROCK]->textureID = LoadTGA("Image/rock.tga");

	meshList[GEO_HIGH_LOD_TURRET_HEAD] = MeshBuilder::GenerateOBJ("GEO_HIGH_LOD_TURRET_HEAD", "OBJ//TurretUpperBody.obj");
	meshList[GEO_HIGH_LOD_TURRET_HEAD]->textureID = LoadTGA("Image/Tank.tga");
	meshList[GEO_HIGH_LOD_TURRET_BODY] = MeshBuilder::GenerateOBJ("GEO_HIGH_LOD_TURRET_BODY", "OBJ//TurretLowerBody.obj");
	meshList[GEO_HIGH_LOD_TURRET_BODY]->textureID = LoadTGA("Image/Tank.tga");

	meshList[GEO_MID_LOD_TURRET_HEAD] = MeshBuilder::GenerateOBJ("GEO_MID_LOD_TURRET_HEAD", "OBJ//TurretUpperBody.obj");
	meshList[GEO_MID_LOD_TURRET_HEAD]->textureID = LoadTGA("Image/Tank.tga");
	meshList[GEO_MID_LOD_TURRET_BODY] = MeshBuilder::GenerateOBJ("GEO_MID_LOD_TURRET_BODY", "OBJ//TurretLowerBody.obj");
	meshList[GEO_MID_LOD_TURRET_BODY]->textureID = LoadTGA("Image/Tank.tga");

	meshList[GEO_LOW_LOD_TURRET_HEAD] = MeshBuilder::GenerateOBJ("GEO_LOW_LOD_TURRET_HEAD", "OBJ//TurretUpperBody.obj");
	meshList[GEO_LOW_LOD_TURRET_HEAD]->textureID = LoadTGA("Image/Tank.tga");
	meshList[GEO_LOW_LOD_TURRET_BODY] = MeshBuilder::GenerateOBJ("GEO_LOW_LOD_TURRET_BODY", "OBJ//TurretLowerBody.obj");
	meshList[GEO_LOW_LOD_TURRET_BODY]->textureID = LoadTGA("Image/Tank.tga");

	meshList[GEO_LINE] = MeshBuilder::GetInstance()->GenerateRay("laser", 1.0f);
	meshList[GEO_WAY_POINT_LINE] = MeshBuilder::GetInstance()->GenerateCube("waypointline", Color(1, 0, 0));


	Math::InitRNG();

	// Init Scene Graph
	GameObject* root = new EmptyObject(this);
	root->SetName("root");
	sceneGraph.SetRoot(root);

	// Set up the Spatial Partition and pass it to the GameObjectManager to manage
	CSpatialPartition::GetInstance()->Init(100, 100, 10, 10);
	CSpatialPartition::GetInstance()->ApplyMesh(meshList[GEO_GRID_UNSELECTED]);
	CSpatialPartition::GetInstance()->SetCamera(&camera);
	CSpatialPartition::GetInstance()->SetLevelOfDetails(600, 1000);
	goManager.SetSpatialPartition(CSpatialPartition::GetInstance());

	// Load hardware abstraction
	if (playerInfo->theKeyBoard->LoadControls("Controls.csv"))
		cout << "Loaded 'Controls.csv' succesfully!" << endl;
	else
		cout << "ERROR: 'Controls.csv' failed to load, missing file?" << endl;

	theTank = new Tank(this, Vector3(12.5f, -5.f, 0), Tank::TANK_BODY);
	theTank->InitLOD(meshList[GEO_HIGH_LOD_TANK_BODY], meshList[GEO_MID_LOD_TANK_BODY], meshList[GEO_LOW_LOD_TANK_BODY]);
	goManager.CreateObject(theTank, true);
	theTank->SetName("tankbody");

	theCannon = new Tank(this, Vector3(0, 4.f, 0), Tank::TANK_TURRET);
	theCannon->InitLOD(meshList[GEO_HIGH_LOD_TANK_TURRET], meshList[GEO_MID_LOD_TANK_TURRET], meshList[GEO_LOW_LOD_TANK_TURRET]);
	goManager.CreateObject(theCannon, false);
	theCannon->SetName("tankturret");
	theTank->AddChild(theCannon);

	//Testing purpose
	CLuaInterface::GetInstance()->SetLuaState("EnemySetting");
	CLuaInterface::GetInstance()->LoadEnemy();
	
	CLuaInterface::GetInstance()->SetLuaState("");
	int noOFTree = CLuaInterface::GetInstance()->getIntValue("numberOfTrees");
	int counterTree = 0;

	while (counterTree < noOFTree)
	{
		GameObject * tempTree = new GameObject(this, GEO_HIGH_LOD_TREE);
		tempTree->InitLOD(meshList[GEO_HIGH_LOD_TREE], meshList[GEO_MID_LOD_TREE], meshList[GEO_LOW_LOD_TREE]);
		tempTree->getCollider().setBoundingBoxSize(Vector3(10.f, 30.f, 10.f));
		tempTree->getCollider().setOffset(Vector3(0, 0, 0));
		tempTree->getCollider().type = Collider::C_AABB;
		tempTree->active = true;
		float randX = Math::RandFloatMinMax(-500.f, 500.f);
		float randZ = Math::RandFloatMinMax(-300.f, 300.f);
		tempTree->pos.Set(randX, 5.f, randZ);
		tempTree->scale.Set(10.f, 10.f, 10.f);
		goManager.CreateObject(tempTree, true);
		tempTree->SetName("Tree: " + to_string(randX) + " " + to_string(randZ));

		counterTree++;
	}


	glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	MouseController::GetInstance()->SetKeepMouseCentered(true);

	// GUI Buttons
	Vector2 btnSize(20, 7);

	btnResume = new GUIButton(0, Vector2(0.5f, 0.6f), btnSize, "Resume");
	m_GUI.AddButton(btnResume);

	btnMainMenu = new GUIButton(1, Vector2(0.5f, 0.5f), btnSize, "Main Menu");
	m_GUI.AddButton(btnMainMenu);

	btnQuitGame = new GUIButton(2, Vector2(0.5f, 0.4f), btnSize, "Quit");
	m_GUI.AddButton(btnQuitGame);


}

void SceneText::Update(double dt)
{
	SceneBase::Update(dt);

	if (KeyboardController::GetInstance()->IsKeyReleased(GLFW_KEY_ESCAPE)) {

		Application::SetIsPaused(!Application::IsPaused());

		if (Application::IsPaused()) {
			glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			MouseController::GetInstance()->SetKeepMouseCentered(false);
		}
		else {
			glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			MouseController::GetInstance()->SetKeepMouseCentered(true);
		}

	}

	if (Application::IsPaused()) {

		if (btnResume->OnClick()) {
			Application::SetIsPaused(false);
			glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			MouseController::GetInstance()->SetKeepMouseCentered(true);
		}

		if (btnMainMenu->OnClick()) {
			SceneManager::getInstance()->changeScene(new SceneMainMenu());
		}

		if (btnQuitGame->OnClick()) {
			glfwSetWindowShouldClose(glfwGetCurrentContext(), GLFW_TRUE);
		}

		return;
	}


	if (KeyboardController::GetInstance()->IsKeyReleased('Z')) {
		playerInfo->IncreaseScraps(50);
	}

	if (KeyboardController::GetInstance()->IsKeyReleased('N'))
	{
		CSpatialPartition::GetInstance()->PrintSelf();
	}

	//Show LOD color mode
	if (KeyboardController::GetInstance()->IsKeyReleased('L'))
	{
		showLODInfo = !showLODInfo;
	}

	//Show waypoint lines
	if (KeyboardController::GetInstance()->IsKeyReleased('J'))
	{
		showWayPointLines = !showWayPointLines;
	}


	//Ride the tank
	if (KeyboardController::GetInstance()->IsKeyReleased('G') && b_isTankBlowApart == false)
	{
		if (playerInfo->GetTypeOfCamera() == CPlayerInfo::PLAYER_CAMERA) {
			playerInfo->SetTypeOfCamera(CPlayerInfo::TANK_CAMERA);
			playerInfo->InitTankCamera(theCannon, theTank);
			b_countDowntank = true;
		}

	}

	m_RobotCount = 0;

	for (auto it : goManager.m_goList) {
		if (it->active && dynamic_cast<Robot*>(it)) {
			m_RobotCount++;
		}
	}

	// All robots defeated
	if (m_RobotCount == 0 && !b_IsGameComplete) {
		SaveHighScore(playerInfo->m_Score); // Save the highscore into lua file
		b_IsGameComplete = true;
	}

	if (playerInfo->GetHealth() > 0 && b_IsGameComplete == false) {

		//The player is now in the tank, starts the timer to tank self destruction
		if (b_countDowntank == true && b_isTankBlowApart == false) {
			theTank->b_isPlayerInTank = true;

			m_lifeTimeTank -= (float)dt;

			//Swap back to player camera and movement after , tank's life time is up
			if (m_lifeTimeTank <= 0) {
				playerInfo->SetTypeOfCamera(CPlayerInfo::PLAYER_CAMERA);

				Vector3 dir = camera.target - camera.position;

				camera.currentPitch = 0;
				// camera.currentYaw = Math::RadianToDegree(atan2(dir.x, dir.z));
				camera.currentYaw = Math::RadianToDegree(playerInfo->camTheta) + 180;
				b_isTankBlowApart = true;

			}
		}

		if (b_isTankBlowApart == true && b_isTranslated == false) {
			theTank->b_isPlayerInTank = false;

			Vector3 impulse;

			theTank->m_force = 1000.f;
			impulse = theTank->m_force * (float)dt;

			Vector3 bodyMove = (1 / theTank->mass) * impulse;
			theTank->Translate(bodyMove.x, bodyMove.y, bodyMove.z);

			Vector3 cannonMove = (-1 / theCannon->mass) * impulse;
			theCannon->Translate(-bodyMove.x, -bodyMove.y, -bodyMove.z);

			m_translationTime += (float)dt;

			if (m_translationTime >= TRANSLATION_END) {
				b_isTranslated = true;
			}
		}

		if (b_isTranslated == true) {
			theTank->Translate(0, -5 * (float)dt, 0);
			theCannon->Translate(0, -5 * (float)dt, 0);

			if (b_isGone == false) {
				if (theCannon->getCollider().getPosition().y <= -10.f) {
					b_isGone = true;
					sceneGraph.FindAndDestroy(theCannon);
					sceneGraph.FindAndDestroy(theTank);
				}
			}

		}

		if (KeyboardController::GetInstance()->IsKeyReleased(playerInfo->theKeyBoard->keyMap.at("Debug")))
			showDebugInfo = !showDebugInfo;

		// Update the player position and other details based on keyboard and mouse inputs
		switch (playerInfo->GetTypeOfCamera())
		{
		case CPlayerInfo::PLAYER_CAMERA:
			playerInfo->Update(dt);
			break;
		case CPlayerInfo::TANK_CAMERA:
			playerInfo->UpdateTankCamera(dt);
			break;
		case CPlayerInfo::AIRSTRIKE_CAMERA:
			playerInfo->UpdateAirStrikeCamera(dt);
			break;
		case CPlayerInfo::MAPANALYSER_CAMERA:
			playerInfo->UpdateMapAnalyserCamera(dt);
			break;
		}

		goManager.UpdateObjects();
	}
	else {
		// player victory/defeat
		if (KeyboardController::GetInstance()->IsKeyReleased(GLFW_KEY_ENTER))
			SceneManager::getInstance()->changeScene(new SceneText());
	}

	GraphicsManager::GetInstance()->UpdateLights(dt);
}

void SceneText::Render()
{
	SceneBase::Render();

	// Shortcut variable
	float winHeight = (float)Application::windowHeight() / 10;
	float winWidth = (float)Application::windowWidth() / 10;

	if (Application::IsFullScreenMode()) {
		m_FOV = 55 + m_ScopedFOV;
	}
	else {
		m_FOV = 45 + m_ScopedFOV;
	}

	// Setup 3D pipeline then render 3D
	GraphicsManager::GetInstance()->SetPerspectiveProjection(m_FOV, Application::windowWidth() / Application::windowHeight(), 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Skyplane
	modelStack->PushMatrix();
	modelStack->Translate(500 + camera.position.x, 3000 + camera.position.y, -500 + camera.position.z);
	modelStack->Scale(4, 4, 4);
	RenderHelper::RenderMesh(meshList[GEO_SKYPLANE]);
	modelStack->PopMatrix();

	// Terrain
	modelStack->PushMatrix();
	modelStack->Translate(0, -10, 0);
	modelStack->Rotate(-90, 1, 0, 0);

	CLuaInterface::GetInstance()->SetLuaState("");
	lua_getglobal(CLuaInterface::GetInstance()->theLuaState, "groundTerrainScale");
	Vector3 terrainScale = Vector3(CLuaInterface::GetInstance()->GetField("x"), CLuaInterface::GetInstance()->GetField("y"), CLuaInterface::GetInstance()->GetField("z"));
	modelStack->Scale(terrainScale.x, terrainScale.y, terrainScale.z);

	RenderHelper::RenderMesh(meshList[GEO_TERRAIN]);
	modelStack->PopMatrix();

	if (playerInfo->GetTypeOfCamera() == CPlayerInfo::PLAYER_CAMERA)
	{
		switch (playerInfo->currentWeapon) {

		case 0: // Pistol
		{
			modelStack->PushMatrix();
			modelStack->Translate(camera.position.x, camera.position.y - 0.5f, camera.position.z);
			modelStack->Rotate(camera.currentYaw + 180, 0, 1, 0);
			modelStack->Rotate(camera.currentPitch, 1, 0, 0);
			modelStack->Scale(1, 1, 1);
			RenderHelper::RenderMeshWithLight(meshList[GEO_SNIPER]);
			modelStack->PopMatrix();
		}

		break;

		case 1: // Rocket Launcher
		{
			modelStack->PushMatrix();
			modelStack->Translate(camera.position.x, camera.position.y - 0.5f, camera.position.z);
			modelStack->Rotate(camera.currentYaw + 180, 0, 1, 0);
			modelStack->Rotate(camera.currentPitch, 1, 0, 0);
			modelStack->Scale(1, 1, 1);
			RenderHelper::RenderMeshWithLight(meshList[GEO_ROCKETLAUNCER]);
			modelStack->PopMatrix();
		}
		break;

		}
	}

	textManager.dequeueMesh();
	goManager.RenderSpatialGrid();


	// Setup 2D pipeline then render 2D
	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	if (Application::IsPaused()) {

		textManager.RenderMeshOnScreen(meshList[GEO_SELECTOR], winWidth * 0.5f, winHeight * 0.525f, Vector3(90, 0, 0), Vector3(10, 1, 15));
		textManager.renderTextOnScreen(Vector3(0.5f, 0.675f, 1), Vector3(1, 1, 1), "Game Paused", Color(1, 1, 1));

	}
	else if (b_IsGameComplete == false && playerInfo->GetHealth() > 0) {

		stringstream text;
		text << "FPS: " << (int)(1 / _dt);
		textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_TOP_RIGHT });

		text.str("");
		text << "Scraps: " << playerInfo->GetScraps();
		textManager.renderTextOnScreen(UIManager::Text{ text.str(), ((playerInfo->GetScraps() >= 100) ? Color(0,1,0) : Color(1,1,1)), UIManager::ANCHOR_TOP_CENTER });

		if (playerInfo->b_showDamageInfo == true)
		{
			switch (playerInfo->m_damageAreaStack.top())
			{
			case CPlayerInfo::DamagedHead:
			{
				text.str("");
				text << "HeadShot!";
				textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,0,0), UIManager::ANCHOR_TOP_CENTER });
			}
			break;
			case CPlayerInfo::DamageBody:
			{
				text.str("");
				text << "BodyShot!";
				textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,0,0), UIManager::ANCHOR_TOP_CENTER });
			}
			break;
			case CPlayerInfo::DamagedArm:
			{
				text.str("");
				text << "ArmShot!";
				textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,0,0), UIManager::ANCHOR_TOP_CENTER });
			}
			break;
			}
		}




		text.str("");
		text << "Current Score: " << playerInfo->m_Score;
		textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_TOP_LEFT });

		text.str("");
		text << "Health: " << playerInfo->GetHealth();
		textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_BOT_RIGHT });

		text.str("");
		text << "Ammo: " << playerInfo->primaryWeapon->magRounds << " / " << playerInfo->primaryWeapon->GetTotalRound();
		textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,1), UIManager::ANCHOR_BOT_RIGHT });

		if (playerInfo->primaryWeapon->b_IsInfiniteAmmo && m_elapsedTime < m_NextTextDisappearTime) {
			text.str("");
			textManager.renderTextOnScreen(UIManager::Text{ "", Color(0,0,0), UIManager::ANCHOR_CENTER_CENTER });
			textManager.renderTextOnScreen(UIManager::Text{ "", Color(0,0,0), UIManager::ANCHOR_CENTER_CENTER });
			text << "<Weapon POWERED UP>";
			textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(1,1,0), UIManager::ANCHOR_CENTER_CENTER });
		}

		if (playerInfo->GetTypeOfCamera() == CPlayerInfo::PLAYER_CAMERA || playerInfo->GetTypeOfCamera() == CPlayerInfo::TANK_CAMERA)
		{
			// Render Radar
			radar.RenderRadar(winWidth * 0.075f, winHeight * 0.105f);
		}


		if (playerInfo->GetTypeOfCamera() == CPlayerInfo::PLAYER_CAMERA)
		{

			textManager.RenderMeshOnScreen(meshList[GEO_CROSSHAIR], winWidth * 0.5f, winHeight * 0.5f, Vector3(90, 0, 0), Vector3(1, 1, 1));

			switch (playerInfo->currentWeapon) {

			case 0: // Pistol
			{
				textManager.RenderMeshOnScreen(meshList[GEO_SELECTOR], winWidth * 0.325f, winHeight * 0.05f, Vector3(90, 0, 0), Vector3(5, 5, 2.5f));
			}

			break;

			case 1: // Rifle
			{
				textManager.RenderMeshOnScreen(meshList[GEO_SELECTOR], winWidth * 0.475f, winHeight * 0.05f, Vector3(90, 0, 0), Vector3(6.25f, 5, 3));
			}
			break;

			}

			if (playerInfo->GetScraps() >= 25)
				textManager.RenderMeshOnScreen(meshList[GEO_SELECTOR], winWidth * 0.6f, winHeight * 0.04f, Vector3(90, 0, 0), Vector3(3, 1, 2.5f));

			textManager.RenderMeshOnScreen(meshList[GEO_SNIPER_ICON], winWidth * 0.325f, winHeight * 0.05f, Vector3(90, 0, 0), Vector3(4.8f, 5, 6));
			textManager.RenderMeshOnScreen(meshList[GEO_ROCKETLAUNCHER_ICON], winWidth * 0.475f, winHeight * 0.06f, Vector3(90, 0, 0), Vector3(9, 1, 14));
			textManager.RenderMeshOnScreen(meshList[GEO_TURRET_ICON], winWidth * 0.6f, winHeight * 0.04f, Vector3(90, 0, 0), Vector3(2.2f, 1, 1.8f));
		}
		else if (playerInfo->GetTypeOfCamera() == CPlayerInfo::TANK_CAMERA) {
			textManager.renderTextOnScreen(UIManager::Text{ std::string("Tank Duration: " + std::to_string((int)(m_lifeTimeTank + 1))), Color(1,1,1), UIManager::ANCHOR_BOT_CENTER });
		}
	}
	else {
		textManager.RenderMeshOnScreen(meshList[GEO_SELECTOR], winWidth * 0.5f, winHeight * 0.5f, Vector3(90, 0, 0), Vector3(23, 1, 23));

		if (b_IsGameComplete) {

			CLuaInterface::GetInstance()->SetLuaState("HighScore");

			stringstream text;
			text << "Victory! Your score is: " << playerInfo->m_Score;
			textManager.renderTextOnScreen(Vector3(0.5f, 0.725f, 1), Vector3(1, 1, 1), text.str(), Color(0, 1, 0));

			textManager.renderTextOnScreen(Vector3(0.5f, 0.625f, 1), Vector3(1, 1, 1), "Top 5 Scores:", Color(0, 0, 0));

			textManager.renderTextOnScreen(Vector3(0.5f, 0.575f, 1), Vector3(1, 1, 1), "1st: " + to_string(CLuaInterface::GetInstance()->getIntValue("Score1")), Color(0, 0, 0));

			textManager.renderTextOnScreen(Vector3(0.5f, 0.525f, 1), Vector3(1, 1, 1), "2nd: " + to_string(CLuaInterface::GetInstance()->getIntValue("Score2")), Color(0, 0, 0));

			textManager.renderTextOnScreen(Vector3(0.5f, 0.475f, 1), Vector3(1, 1, 1), "3rd: " + to_string(CLuaInterface::GetInstance()->getIntValue("Score3")), Color(0, 0, 0));

			textManager.renderTextOnScreen(Vector3(0.5f, 0.425f, 1), Vector3(1, 1, 1), "4th: " + to_string(CLuaInterface::GetInstance()->getIntValue("Score4")), Color(0, 0, 0));

			textManager.renderTextOnScreen(Vector3(0.5f, 0.375f, 1), Vector3(1, 1, 1), "5th: " + to_string(CLuaInterface::GetInstance()->getIntValue("Score5")), Color(0, 0, 0));

			textManager.renderTextOnScreen(Vector3(0.5f, 0.25f, 1), Vector3(1, 1, 1), "Press <Enter> to play again", Color(0, 1, 0));

			CLuaInterface::GetInstance()->SetLuaState("");
		}
		else {
			stringstream text;
			text << "You died.. Press <Enter> to restart!";
			textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(0,0,0), UIManager::ANCHOR_CENTER_CENTER });

			textManager.renderTextOnScreen(UIManager::Text{ "", Color(1,1,1), UIManager::ANCHOR_CENTER_CENTER });

			text.str("");
			text << ">Restart<";
			textManager.renderTextOnScreen(UIManager::Text{ text.str(), Color(0,1,0), UIManager::ANCHOR_CENTER_CENTER });
		}
	}

	if (Application::IsPaused())
		m_GUI.RenderButtons();

	textManager.dequeueText();
	textManager.RenderTimedMeshOnScreen();
	textManager.reset(); // Must be called at the end of Render()

}

void SceneText::Exit()
{

	if (playerInfo->theKeyBoard)
		delete playerInfo->theKeyBoard;

	m_GUI.Exit();

	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();
	playerInfo->DetachCamera();

	playerInfo->DropInstance();

	goManager.Exit();

	GraphicsManager::GetInstance()->RemoveLight("lights[0]");

	SceneBase::Exit();

}

void SceneText::SaveHighScore(int score) {

	CLuaInterface::GetInstance()->SetLuaState("HighScore");

	vector<int> highscores;

	// Push all the scores into a container
	highscores.push_back(CLuaInterface::GetInstance()->getIntValue("Score1"));
	highscores.push_back(CLuaInterface::GetInstance()->getIntValue("Score2"));
	highscores.push_back(CLuaInterface::GetInstance()->getIntValue("Score3"));
	highscores.push_back(CLuaInterface::GetInstance()->getIntValue("Score4"));
	highscores.push_back(CLuaInterface::GetInstance()->getIntValue("Score5"));
	highscores.push_back(score);

	// Sort them in descending order
	std::sort(highscores.begin(), highscores.end(), std::greater<>());

	// Then choose the first 5 index to save into score
	CLuaInterface::GetInstance()->saveIntValue("Score1", highscores[0], "HighScore", true);
	CLuaInterface::GetInstance()->saveIntValue("Score2", highscores[1], "HighScore", false);
	CLuaInterface::GetInstance()->saveIntValue("Score3", highscores[2], "HighScore", false);
	CLuaInterface::GetInstance()->saveIntValue("Score4", highscores[3], "HighScore", false);
	CLuaInterface::GetInstance()->saveIntValue("Score5", highscores[4], "HighScore", false);


	CLuaInterface::GetInstance()->SetLuaState("");

}
