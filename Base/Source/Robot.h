#pragma once

#include "EmptyObject.h"
#include "SceneText.h"

class Robot : public EmptyObject {

public:
	enum ROBOT_PART
	{
		ROBOT_HEAD,
		ROBOT_BODY,
		ROBOT_ARM,
	};

	ROBOT_PART m_robotPart;

	//Feed in the parent(body) pos , the rest of the body parts are translated accordingly
	Robot(Scene* scene, Vector3 pos , ROBOT_PART _robotPart);
	~Robot();

	virtual bool Update();
	virtual void OnCollisionHit(GameObject* other);



};


