#pragma once 

#include "SceneBase.h"
#include "Mtx44.h"
#include "Mesh.h"
#include "MatrixStack.h"

class SceneSplash : public SceneBase {

public:
	SceneSplash() {};
	~SceneSplash() {};

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

private:
	Light* lights[1];

	float m_CurrentSplashAlpha = 0;
	float m_CurrentSplashTimer;
	
};