#pragma once
#include "Vector3.h"
#include "FPSCamera.h"
#include "WeaponInfo.h"
#include "Collider.h"
#include <stack>

class CKeyboard;
class Tank;

#include <fstream>
#include <string>
#include <sstream>
using namespace std;

class CPlayerInfo
{
	friend class CController;

protected:
	static CPlayerInfo *s_instance;
	CPlayerInfo(void);

	// The postures of the FPS player/camera
	enum CURRENT_POSTURE
	{
		STAND = 0,
		CROUCH,
		PRONE,
		NUM_POSTURE,
	};
	CURRENT_POSTURE theCurrentPosture;

public:
	static CPlayerInfo *GetInstance()
	{
		if (!s_instance)
			s_instance = new CPlayerInfo;
		return s_instance;
	}
	static bool DropInstance()
	{
		if (s_instance)
		{
			delete s_instance;
			s_instance = NULL;
			return true;
		}
		return false;

	}
	~CPlayerInfo(void);

	// Initialise this class instance
	void Init(void);
	// Reset this player instance to default
	void Reset(void);

	// Set position
	void SetPos(const Vector3& pos);
	// Set target
	void SetTarget(const Vector3& target);
	// Set Up
	void SetUp(const Vector3& up);

	// Get position
	Vector3 GetPos(void) const;
	// Get target
	Vector3 GetTarget(void) const;
	// Get Up
	Vector3 GetUp(void) const;

	// Update
	void Update(double dt = 0.0333f);

	// Constrain the position within the borders
	void Constrain(void);

	// Handling Camera
	void AttachCamera(FPSCamera* _cameraPtr);
	void DetachCamera();

	bool Move_FrontBack(const float deltaTime, const bool direction, const float speedMultiplier);
	bool Move_LeftRight(const float deltaTime, const bool direction, const float  speedMultiplier);

	CKeyboard* theKeyBoard;

	int m_Score = 0;
	int currentWeapon;
	CWeaponInfo* primaryWeapon;
	CWeaponInfo* m_pistol;
	CWeaponInfo* m_rifle;
	CWeaponInfo * m_cannon;

	FPSCamera* attachedCamera;
	Vector3 position, target, up;
	double mouse_diff_x, mouse_diff_y;

	Vector3 positionBeforeAirStrike;

	int GetHealth() { return m_Health; }

	void ReduceHealth(int value);
	void IncreaseHealth(int value);

	void ReduceScraps(int value);
	void IncreaseScraps(int value);

	int GetScraps() { return m_Scraps; }

	Collider m_Collider;

	enum CAMERA_MODE
	{
		PLAYER_CAMERA,
		TANK_CAMERA,
		AIRSTRIKE_CAMERA,
		MAPANALYSER_CAMERA
	};

	void SetTypeOfCamera(CAMERA_MODE _typeOfCamera);
	CAMERA_MODE GetTypeOfCamera();

	void InitTankCamera(Tank* cannon, Tank * body);
	bool Move_TankUpDown(const float deltaTime, const bool direction, const float  speedMultiplier);
	bool Rotate_Tank(const float deltaTime, const bool direction, const float  speedMultiplier, const float  angleRotation);
	void UpdateTankCamera(double dt = 0.0333f);
	void UpdateAirStrikeCamera(double dt = 0.0333f);
	void UpdateMapAnalyserCamera(double dt = 0.0333f);

	void TakeDamage(bool _isDamage);

	bool IsAirstrikeFalling() { return m_AirStrikeStartFalling; }

	// Restores the camera values using the previously backed up camera values
	void RestoreCamera() {
		position = m_BackupPosition;
		target = m_BackupTarget;
		up = m_BackupUp;
		attachedCamera->currentYaw = m_BackupYaw;
		attachedCamera->currentPitch = m_BackupPitch;
	}

	// Back up the current camera values to be used for restoring previous camera position
	void BackupCamera() {
		m_BackupPosition = position;
		m_BackupTarget = target;
		m_BackupUp = up;
		m_BackupYaw = attachedCamera->currentYaw;
		m_BackupPitch = attachedCamera->currentPitch;
	}

	Vector3 m_BackupPosition, m_BackupTarget, m_BackupUp;
	float m_BackupYaw, m_BackupPitch;

	// Tank Camera rotation
	float camPhi = 0;
	float camTheta = 0;
	float camBodyTheta = 0;

	enum DamageArea
	{
		DamagedHead,
		DamageBody,
		DamagedArm,
	};

	stack<DamageArea> m_damageAreaStack;

	bool b_showDamageInfo = false;
	bool b_isThereAnotherDamagedInfo = false;
	float m_damageInfoTime = 5.f;

	void UpdateDamageInfo();
	

	void SetPlayerSpeed(float speed) { m_dSpeed = speed; }
	void SetPlayerHealth(int health) { m_Health = health; }

private:
	Vector3 defaultPosition, defaultTarget, defaultUp;
	Vector3 maxBoundary, minBoundary;

	double m_dSpeed;
	double m_dAcceleration;


	double m_dElapsedTime;

	const float m_MouseSensitivity = 3.f;

	float m_MinTerrainHeight = 0;

	int m_Health = 0;

	int m_Scraps = 0; // Torbjorn's scraps

	// Camera Sway
	float m_iCameraSwayAngle;
	float m_iCameraSwayDeltaAngle;
	// The limits for left and right sway
	float m_iCameraSwayAngle_LeftLimit;
	float m_iCameraSwayAngle_RightLimit;
	bool m_bCameraSwayDirection = false; // false = left, true = right

	CAMERA_MODE typeOfCamera;

	Tank * m_tank[2];

	// Air Strike Camera helpers
	int xIndex = 0;
	int zIndex = 0;
	bool m_AirStrikeStartFalling = false;
	float m_FallVelocity = 0;

	// Map Analyser

	// Invincible frames
	const float m_Damage_Cooldown = 2.f;
	float m_NextDamageTime = 0;
	bool b_isDamaged = false;
	bool b_isResetDamageCoolDown = false;

};
