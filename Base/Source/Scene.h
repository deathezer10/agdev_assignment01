#ifndef SCENE_H
#define SCENE_H


#include "Radar.h"
#include "UIManager.h"
#include "SceneGraph.h"
#include "GameObjectManager.h"

#include "FPSCamera.h"
#include "PlayerInfo.h"

#include "GUI.h"


class ShaderProgram;
class Player;
class Portal;

class Scene
{
public:

	enum UNIFORM_TYPE
	{
		U_MVP = 0,
		U_MESH_COLOR,
		U_MESH_ALPHA,
		U_MODELVIEW,
		U_MODELVIEW_INVERSE_TRANSPOSE,
		U_MATERIAL_AMBIENT,
		U_MATERIAL_DIFFUSE,
		U_MATERIAL_SPECULAR,
		U_MATERIAL_SHININESS,
		U_LIGHTENABLED,
		U_NUMLIGHTS,
		U_LIGHT0_TYPE,
		U_LIGHT0_POSITION,
		U_LIGHT0_COLOR,
		U_LIGHT0_POWER,
		U_LIGHT0_KC,
		U_LIGHT0_KL,
		U_LIGHT0_KQ,
		U_LIGHT0_SPOTDIRECTION,
		U_LIGHT0_COSCUTOFF,
		U_LIGHT0_COSINNER,
		U_LIGHT0_EXPONENT,
		U_LIGHT1_TYPE,
		U_LIGHT1_POSITION,
		U_LIGHT1_COLOR,
		U_LIGHT1_POWER,
		U_LIGHT1_KC,
		U_LIGHT1_KL,
		U_LIGHT1_KQ,
		U_LIGHT1_SPOTDIRECTION,
		U_LIGHT1_COSCUTOFF,
		U_LIGHT1_COSINNER,
		U_LIGHT1_EXPONENT,
		U_COLOR_TEXTURE,
		U_COLOR_TEXTURE_ENABLED,
		U_TEXT_ENABLED,
		U_TEXT_COLOR,
		U_TOTAL,
	};

	enum GEOMETRY_TYPE
	{
		GEO_NONE,
		GEO_LINE,
		GEO_WAY_POINT_LINE,

		GEO_BOXCOLLIDER,
		GEO_OUTERBOXCOLLIDER,

		GEO_GRID_UNSELECTED,
		GEO_GRID_SELECTED,

		GEO_LOD_HIGH,
		GEO_LOD_MED,
		GEO_LOD_LOW,

		GEO_TEXT,
		GEO_PANEL,

		GEO_BUTTON1_NORMAL,
		GEO_BUTTON1_HOVER,

		GEO_TERRAIN,
		GEO_SKYPLANE,

		GEO_MAINMENU,
		GEO_SPLASHIMAGE,

		GEO_RADAR_BACKGROUND,
		GEO_RADAR_PLAYER,
		GEO_RADAR_ENEMY,
		GEO_RADAR_CASTLE,

		GEO_DAMAGED,
		GEO_HEALED,

		GEO_CROSSHAIR,
		GEO_CASTLE,
		GEO_SELECTOR,
		GEO_FENCE,

		GEO_TARGET,
		GEO_ALIEN,
		GEO_STARGATE,
		GEO_WARP,
		GEO_HEALTHPACK,

		GEO_BULLET,
		GEO_ROCKET_SHELL,

		GEO_SCOPE,
		GEO_ROCKETLAUNCER,
		GEO_SNIPER,

		GEO_TURRET_ICON,
		GEO_ROCKETLAUNCHER_ICON,
		GEO_SNIPER_ICON,

		GEO_HIGH_LOD_TANK_TURRET,
		GEO_HIGH_LOD_TANK_BODY,
		GEO_MID_LOD_TANK_TURRET,
		GEO_MID_LOD_TANK_BODY,
		GEO_LOW_LOD_TANK_TURRET,
		GEO_LOW_LOD_TANK_BODY,

		GEO_HIGH_LOD_TREE,
		GEO_MID_LOD_TREE,
		GEO_LOW_LOD_TREE,

		GEO_HIGH_LOD_ROBOT_HEAD,
		GEO_HIGH_LOD_ROBOT_BODY,
		GEO_HIGH_LOD_ROBOT_ARM,

		GEO_MID_LOD_ROBOT_HEAD,
		GEO_MID_LOD_ROBOT_BODY,
		GEO_MID_LOD_ROBOT_ARM,

		GEO_LOW_LOD_ROBOT_HEAD,
		GEO_LOW_LOD_ROBOT_BODY,
		GEO_LOW_LOD_ROBOT_ARM,

		GEO_HIGH_LOD_ROCK,
		GEO_MID_LOD_ROCK,
		GEO_LOW_LOD_ROCK,


		GEO_HIGH_LOD_TURRET_HEAD,
		GEO_HIGH_LOD_TURRET_BODY,
		GEO_MID_LOD_TURRET_HEAD,
		GEO_MID_LOD_TURRET_BODY,
		GEO_LOW_LOD_TURRET_HEAD,
		GEO_LOW_LOD_TURRET_BODY,

		//Tank's bullet
		GEO_CANNON_BALL,

		NUM_GEOMETRY,

	};

	enum SCENE_INDEX {

		LEVEL_NONE,
		LEVEL_01,
		LEVEL_02,
		LEVEL_03,
		LEVEL_TOTAL

	};

	Scene() : textManager(this), radar(this), sceneGraph(this) {}
	~Scene() {}

	virtual void Init() = 0;
	virtual void Update(double dt) = 0;
	virtual void Render() = 0;
	virtual void Exit() = 0;

	MS* modelStack;
	MS viewStack, projectionStack;

	Mesh* meshList[NUM_GEOMETRY];

	float _dt = 0;
	float m_elapsedTime = 0;

	unsigned m_parameters[U_TOTAL];

	unsigned m_level_Index = LEVEL_NONE;

	void RenderMesh(Mesh * mesh, bool enableLight);

	bool b_Defeated = false;

	// Managers
	GUI m_GUI;
	Radar radar;
	UIManager textManager;
	GameObjectManager goManager;
	SceneGraph sceneGraph;

	ShaderProgram* currProg;
	CPlayerInfo* playerInfo;
	FPSCamera camera;

	int type;
	float m_Timeleft = 60;
	bool showDebugInfo = false;
	bool showLODInfo = false;
	bool showWayPointLines = false;
};

#endif