#pragma once 

#include "SceneBase.h"
#include "Mtx44.h"
#include "Mesh.h"
#include "MatrixStack.h"

class SceneHighScore : public SceneBase {

public:
	SceneHighScore() {};
	~SceneHighScore() {};

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

private:
	Light* lights[1];

	GUIButton* btnQuit;

	float m_HighScore_1st = 0;
	float m_HighScore_2nd = 0;
	float m_HighScore_3rd = 0;
	float m_HighScore_4th = 0;
	float m_HighScore_5th = 0;
	
};