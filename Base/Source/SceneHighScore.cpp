#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "Application.h"
#include "SceneMainMenu.h"
#include "SceneHighScore.h"
#include "SceneManager.h"
#include "SceneText.h"
#include "GraphicsManager.h"
#include "MouseController.h"
#include "KeyboardController.h"
#include "RenderHelper.h"
#include "MeshBuilder.h"
#include "LoadTGA.h"
#include "LuaInterface.h"



void SceneHighScore::Init()
{
	SceneBase::Init();


	// Create the playerinfo instance, which manages all information about the player
	playerInfo = CPlayerInfo::GetInstance();
	playerInfo->Init();

	// Create and attach the camera to the scene
	camera.Init(playerInfo->GetPos(), playerInfo->GetTarget(), playerInfo->GetUp());
	playerInfo->AttachCamera(&camera);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	meshList[GEO_MAINMENU] = MeshBuilder::GenerateRadarQuad("GEO_MAINMENU", Color(1, 1, 1));
	meshList[GEO_MAINMENU]->textureID = LoadTGA("Image/mainmenubg.tga", false);

	glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	MouseController::GetInstance()->SetKeepMouseCentered(false);

	btnQuit = new GUIButton(3, Vector2(0.5f, 0.1f), Vector2(20, 7), "Back");
	m_GUI.AddButton(btnQuit);


	CLuaInterface::GetInstance()->SetLuaState("HighScore");

	m_HighScore_1st = CLuaInterface::GetInstance()->getFloatValue("Score1");
	m_HighScore_2nd = CLuaInterface::GetInstance()->getFloatValue("Score2");
	m_HighScore_3rd = CLuaInterface::GetInstance()->getFloatValue("Score3");
	m_HighScore_4th = CLuaInterface::GetInstance()->getFloatValue("Score4");
	m_HighScore_5th = CLuaInterface::GetInstance()->getFloatValue("Score5");

	CLuaInterface::GetInstance()->SetLuaState("");

}


void SceneHighScore::Update(double dt) {

	SceneBase::Update(dt);

	if (btnQuit->OnClick()) {
		SceneManager::getInstance()->changeScene(new SceneMainMenu());
	}

	GraphicsManager::GetInstance()->UpdateLights(dt);
}

void SceneHighScore::Render() {

	SceneBase::Render();

	GraphicsManager::GetInstance()->SetPerspectiveProjection(45, Application::windowWidth() / Application::windowHeight(), 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Setup 2D pipeline then render 2D
	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	textManager.dequeueMesh();

	float winHeight = (float)Application::windowHeight() / 10;
	float winWidth = (float)Application::windowWidth() / 10;

	textManager.RenderMeshOnScreen(meshList[GEO_MAINMENU], winWidth * 0.5f, winHeight * 0.5f, Vector3(90, 0, 0), Vector3(winWidth / 2, 1, winHeight / 2));

	textManager.renderTextOnScreen(Vector3(0.45f, 0.7f, 0), Vector3(2, 2, 1), "High Score Page", Color(1, 1, 1));

	Color highscoreColor(.2f, .2f, .2f);

	textManager.renderTextOnScreen(Vector3(0.5f, 0.6f, 0), Vector3(1, 1, 1), "1st: " + to_string((int)m_HighScore_1st), highscoreColor);
	textManager.renderTextOnScreen(Vector3(0.5f, 0.5f, 0), Vector3(1, 1, 1), "2nd: " + to_string((int)m_HighScore_2nd), highscoreColor);
	textManager.renderTextOnScreen(Vector3(0.5f, 0.4f, 0), Vector3(1, 1, 1), "3rd: " + to_string((int)m_HighScore_3rd), highscoreColor);
	textManager.renderTextOnScreen(Vector3(0.5f, 0.3f, 0), Vector3(1, 1, 1), "4th: " + to_string((int)m_HighScore_4th), highscoreColor);
	textManager.renderTextOnScreen(Vector3(0.5f, 0.2f, 0), Vector3(1, 1, 1), "5th: " + to_string((int)m_HighScore_5th), highscoreColor);

	m_GUI.RenderButtons();

	textManager.dequeueText();
	textManager.RenderTimedMeshOnScreen();
	textManager.reset(); // Must be called at the end of Render()
}

void SceneHighScore::Exit()
{
	m_GUI.Exit();

	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();
	playerInfo->DetachCamera();

	playerInfo->DropInstance();

	goManager.Exit();

	GraphicsManager::GetInstance()->RemoveLight("lights[0]");

	SceneBase::Exit();
}
