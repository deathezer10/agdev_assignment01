#include "SceneNode.h"
#include "GameObject.h"
#include "SceneManager.h"
#include "RobotBase.h"

SceneNode::SceneNode(const std::string & name, GameObject * go) {
	this->name = name;
	m_Go = go;
	m_Transform.SetToIdentity();

}

void SceneNode::AddChild(SceneNode* child) {
	m_Child.push_back(child);
	child->m_Parent = this;
}


void SceneNode::RecurseDestroy(SceneNode* currentNode) {

	SceneManager::getInstance()->getCurrentScene()->goManager.DestroyObjectQueue(currentNode->GetOBJ());

	for (auto it : currentNode->m_Child) {
		RecurseDestroy(it);
	}
}

void SceneNode::Destroy() {
	if (m_Parent)
		m_Parent->DetachChild(this);

	SceneManager::getInstance()->getCurrentScene()->goManager.DestroyObjectQueue(m_Go);
	DestroyAllChild();
}

bool SceneNode::DestroyChild(SceneNode * child) {
	if (DetachChild(child)) {
		RecurseDestroy(child);
		return true;
	}
	return false;
}

void SceneNode::DestroyAllChild() {
	for (auto it : m_Child) {
		RecurseDestroy(it);
	}
	m_Child.clear();
}

bool SceneNode::DetachChild(SceneNode * child) {
	for (auto it = m_Child.begin(); it != m_Child.end();) {
		if (*it == child) {
			child->m_Parent = nullptr;
			it = m_Child.erase(it);
			return true;
		}
		else {
			it++;
		}
	}
	return false;
}

void SceneNode::DetachParent() {
	m_Parent->DetachChild(this);
	m_Parent = nullptr;
}

SceneNode* SceneNode::FindNode(const std::string& name) {
	return RecurseFindNode(name, this);
}

SceneNode* SceneNode::FindNode(SceneNode* node) {
	return RecurseFindNode(node, this);
}

std::vector<SceneNode*> SceneNode::GetAllChild() {

	std::vector<SceneNode*> container;
	RecurseFindChild(container, this);
	return container;
}

void SceneNode::Translate(float x, float y, float z) {
	Mtx44 temp;
	temp.SetToTranslation(x, y, z);
	m_Transform = m_Transform * temp;
}

void SceneNode::Rotate(float degrees, float x, float y, float z) throw(DivideByZero) {
	Mtx44 temp;
	temp.SetToRotation(degrees, x, y, z);
	m_Transform = m_Transform * temp;
}

void SceneNode::Scale(float x, float y, float z) {
	Mtx44 temp;
	temp.SetToScale(x, y, z);
	m_Transform = m_Transform * temp;
}

SceneNode* SceneNode::RecurseFindNode(const std::string& name, SceneNode* currentNode) {

	SceneNode* result = nullptr;

	if (currentNode->name == name)
		return this;

	for (auto &it : currentNode->m_Child) {

		if (it->GetName() == name) {
			return it;
		}
		else {
			result = RecurseFindNode(name, it);

			if (result)
				return result;
		}

	}

	return result;
}

SceneNode* SceneNode::RecurseFindNode(SceneNode* nodeToFind, SceneNode* currentNode) {

	SceneNode* result = nullptr;

	if (currentNode == nodeToFind)
		return this;

	for (auto &it : currentNode->m_Child) {

		if (it == nodeToFind) {
			return it;
		}
		else {
			result = RecurseFindNode(nodeToFind, it);

			if (result)
				return result;
		}

	}

	return result;
}

void SceneNode::RecurseFindChild(std::vector<SceneNode*> &container, SceneNode* currentNode) {

	for (auto &child : currentNode->m_Child) {
		container.push_back(child);
		RecurseFindChild(container, child);
	}

}

void SceneNode::Render() {

	MS* modelStack = SceneManager::getInstance()->getCurrentScene()->modelStack;

	modelStack->PushMatrix();
	modelStack->MultMatrix(m_Transform);

	auto gg = modelStack->Top();
	m_Go->getCollider().setOffset(Vector3(gg.a[12], gg.a[13], gg.a[14]));

	if (m_Go->type != Scene::GEO_NONE)
		m_Go->Render();

	for (auto & it : m_Child) {
		it->Render();
	}

	modelStack->PopMatrix();


	Collider collider = m_Go->getCollider();

	// Render Box Collider of all Objects onto screen, Mesh: Cube Size: 0.5f, 0.5f, 0.5f
	if (m_Go->m_scene->showDebugInfo == true) {

		if (collider.outerBoxSize.IsZero() == false) {
			m_Go->m_scene->textManager.queueRenderMesh(UIManager::MeshQueue{

				m_Go->m_scene->meshList[Scene::GEO_OUTERBOXCOLLIDER],
				collider.getPosition(),
				Vector3(0, 0, 0),
				Vector3(collider.outerBoxSize.x, collider.outerBoxSize.y, collider.outerBoxSize.z)

			});
		}

		m_Go->m_scene->textManager.queueRenderMesh(UIManager::MeshQueue{

			m_Go->m_scene->meshList[Scene::GEO_BOXCOLLIDER],
			collider.getPosition(),
			Vector3(0, 0, 0),
			Vector3(collider.innerBoxSize.x, collider.innerBoxSize.y, collider.innerBoxSize.z)

		});

	}

	if (m_Go->GetLODStatus() == true) {
		if (m_Go->m_scene->showLODInfo == true) {
			switch (m_Go->GetDetailLevel())
			{
			case CLevelOfDetails::DETAIL_LEVEL::HIGH_DETAILS:
				m_Go->m_scene->textManager.queueRenderMesh(UIManager::MeshQueue{
					m_Go->m_scene->meshList[Scene::GEO_LOD_HIGH],
					collider.getPosition(),
					Vector3(0, 0, 0),
					Vector3(collider.innerBoxSize.x, collider.innerBoxSize.y, collider.innerBoxSize.z)
				});
				break;
			case CLevelOfDetails::DETAIL_LEVEL::MID_DETAILS:
				m_Go->m_scene->textManager.queueRenderMesh(UIManager::MeshQueue{
					m_Go->m_scene->meshList[Scene::GEO_LOD_MED],
					collider.getPosition(),
					Vector3(0, 0, 0),
					Vector3(collider.innerBoxSize.x, collider.innerBoxSize.y, collider.innerBoxSize.z)
				});
				break;
			case CLevelOfDetails::DETAIL_LEVEL::LOW_DETAILS:
				m_Go->m_scene->textManager.queueRenderMesh(UIManager::MeshQueue{
					m_Go->m_scene->meshList[Scene::GEO_LOD_LOW],
					collider.getPosition(),
					Vector3(0, 0, 0),
					Vector3(collider.innerBoxSize.x, collider.innerBoxSize.y, collider.innerBoxSize.z)
				});
				break;


			}
		}
	}

	if (m_Go->m_scene->showWayPointLines == true){

		RobotBase* _aRobot = dynamic_cast<RobotBase*>(m_Go);

		//If not null
		if (_aRobot)
		{
			_aRobot->RenderWayPointLines();
		}
	}

	/*

		m_Scene->modelStack->Translate(pos.x, pos.y, pos.z);
		m_Scene->modelStack->Rotate(rotation.y, 0, 1, 0);
		m_Scene->modelStack->Rotate(rotation.z, 0, 0, 1);
		m_Scene->modelStack->Rotate(rotation.x, 1, 0, 0);
		m_Scene->modelStack->Scale(scale.x, scale.y, scale.z);

		if (b_EnableLight)
			RenderHelper::RenderMeshWithLight(m_Scene->meshList[type]);
		else
			RenderHelper::RenderMesh(m_Scene->meshList[type]);

		m_Scene->modelStack->PopMatrix();*/

}

