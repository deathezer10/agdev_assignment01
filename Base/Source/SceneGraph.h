#pragma once

#include <string>


class Scene;
class SceneNode;


class SceneGraph {


public:
	SceneGraph(Scene* scene) { m_Scene = scene; };
	~SceneGraph() {};

	void SetRoot(SceneNode* root) { this->root = root; };
	SceneNode* GetRoot() { return root; }

	// Finds the given SceneNode and destroy it and its underlying Childrens
	// Returns TRUE if node was found and destroyed
	bool FindAndDestroy(SceneNode* node);

	// Releases all resources taken up by the SceneGraph
	void Exit();

	// Renders all SceneNodes in the collection to the Scene
	void Render();

private:
	SceneNode* root = nullptr;
	Scene* m_Scene = nullptr;

};