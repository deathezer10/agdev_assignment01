#include "Application.h"
#include "MouseController.h"
#include "GUI.h"
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "Mtx44.h"
#include "RenderHelper.h"
#include "GraphicsManager.h"
#include "SceneManager.h"




void GUI::AddButton(GUIButton* btn) {
	m_Buttons[btn->id] = btn;
}

void GUI::RemoveButton(int btnID) {
	delete m_Buttons[btnID];
	m_Buttons.erase(btnID);
}

void GUI::UpdateButtons() {

	double x, y;
	glfwGetCursorPos(glfwGetCurrentContext(), &x, &y);

	int w = Application::fullscreenWindowWidth();
	int h = Application::fullscreenWindowHeight();

	m_worldHeight = 100.f;
	m_worldWidth = m_worldHeight * (float)w / h;

	float posX = static_cast<float>(x) / w * m_worldWidth;
	float posY = (h - static_cast<float>(y)) / h * m_worldHeight;
	
	for (auto &it : m_Buttons) {
		auto button = it.second;

		if (!button->active)
			continue;

		if (button->btnState == GUIButton::CLICKED) {
			continue;
		}

		// If clicked, change button state
		if (button->PointToAABB(Vector2(posX , posY))) {

			if (button->btnState == GUIButton::DOWN) {
				if (!glfwGetMouseButton(glfwGetCurrentContext(), GLFW_MOUSE_BUTTON_1)) {
					button->btnState = GUIButton::CLICKED;
				}
			}
			else {
				if (glfwGetMouseButton(glfwGetCurrentContext(), GLFW_MOUSE_BUTTON_1)) {
					button->btnState = GUIButton::DOWN;
				}
				else if (button->btnState == GUIButton::NONE) {
					button->btnState = GUIButton::HOVER;
				}
			}
		}
		else {
			button->btnState = GUIButton::NONE;
		}

	}


}

Vector2 ConvertPercentToPosition(Vector2 percent) {
	float m_worldHeight = 100.f;
	float m_worldWidth = m_worldHeight * (float)Application::fullscreenWindowWidth() / Application::fullscreenWindowHeight();

	return Vector2(m_worldWidth * percent.x, m_worldHeight * percent.y);
}

void GUI::RenderButtons() {
	glDisable(GL_DEPTH_TEST);

	// Backup
	Mtx44 originalProjStack = GraphicsManager::GetInstance()->GetProjectionMatrix();
	Mtx44 originalViewStack = GraphicsManager::GetInstance()->GetViewMatrix();

	for (auto &it : m_Buttons) {

		if (!it.second->active)
			continue;

		auto button = it.second;

		Mtx44 ortho;
		ortho.SetToOrtho(0, m_worldWidth, 0, m_worldHeight, -100, 100); //size of screen UI	
		GraphicsManager::GetInstance()->projectionMatrix = ortho; // set to ortho first
		GraphicsManager::GetInstance()->viewMatrix.SetToIdentity();

		auto modelStack = SceneManager::getInstance()->getCurrentScene()->modelStack;

		modelStack->PushMatrix();
		modelStack->LoadIdentity();
		modelStack->Translate(m_worldWidth * button->position.x, m_worldHeight * button->position.y, 1);
		modelStack->Scale(button->scale.x, button->scale.y, 1);

		Vector3 btnCurrentScale;

		if (button->btnState == GUIButton::HOVER) {
			modelStack->Scale(0.9f, 0.9f, 1); // Hover makes the button smaller
			btnCurrentScale.Set(0.9f, 0.9f, 1);
			RenderHelper::RenderMesh(m_btnMeshHover);
		}
		else if (button->btnState == GUIButton::DOWN) {
			modelStack->Scale(0.8f, 0.8f, 1); // Hover makes the button smaller
			btnCurrentScale.Set(0.8f, 0.8f, 1);
			RenderHelper::RenderMesh(m_btnMeshHover);
		}
		else {
			btnCurrentScale.Set(1, 1, 1);
			RenderHelper::RenderMesh(m_btnMeshNormal);
		}

		if (m_UIManager) {
			m_UIManager->renderTextOnScreen(Vector3(button->position.x, button->position.y, 0), btnCurrentScale, button->text, Color(1, 1, 1));
		}

		modelStack->PopMatrix();

	}

	// Restore backup
	GraphicsManager::GetInstance()->projectionMatrix = originalProjStack;
	GraphicsManager::GetInstance()->viewMatrix = originalViewStack;

	glEnable(GL_DEPTH_TEST);
}

void GUI::Exit() {
	for (auto &it : m_Buttons) {
		delete it.second;
	}
	m_Buttons.clear();
}

bool GUIButton::PointToAABB(Vector2 point) {
	Vector2 newPosition = ConvertPercentToPosition(position);

	if ((point.x >= newPosition.x - (scale.x / 2) && point.x <= newPosition.x + (scale.x / 2)) && (point.y >= newPosition.y - (scale.y / 2) && point.y <= newPosition.y + (scale.y / 2))) {
		return true;
	}
	else {
		return false;
	}
}
