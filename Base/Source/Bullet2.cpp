#include <map>
#include <iterator>
#include "Scene.h"
#include "Bullet2.h"
#include "SpatialPartition.h"
#include "SceneManager.h"
#include "RenderHelper.h"
#include "LuaInterface.h"

using std::multimap;

// Player Bullet
BulletTwo::BulletTwo(Scene* scene, Vector3 pos, Vector3 dir, bool isEnemyProjectile) : GameObject(scene, Scene::GEO_LINE) {
	active = true;
	scale.Set(0.25f, 0.25f, 0.25f);
	// b_EnablePhysics = true;
	m_EnemyProjectile = isEnemyProjectile;

	this->pos = pos;
	CLuaInterface::GetInstance()->SetLuaState("PlayerSettings");
	m_BulletSpeed = CLuaInterface::GetInstance()->getFloatValue("sniperBulletSpeed");
	m_BulletMaxDistance = 250;
	m_Direction = dir.Normalized();
	
	collider.type = Collider::C_LINE;
}


bool BulletTwo::Update() {

	if (m_IsFirstFrame) {
		collider.setLineSize(pos, (pos + (m_Direction.Normalized() * 100)));
		m_IsFirstFrame = false;
		return true;
	}

	pos += m_Direction * m_BulletSpeed * m_scene->_dt;
	collider.setLineSize(pos, (pos + (m_Direction.Normalized() * 100)));

	m_CurrentLifeTime += m_scene->_dt;

	// Destroy object after a set time
	if (m_CurrentLifeTime >= 2) {
		active = false;
		m_scene->sceneGraph.FindAndDestroy(this);
		return false;
	}

	return true;
}

void  BulletTwo::CalculateAngles(void)
{
	rotation.x = acos(m_Direction.x / m_Direction.Length());
	if ((m_Direction.x < 0) && (m_Direction.z > 0))
		rotation *= -1.0f;
	rotation.y = acos(m_Direction.y / m_Direction.Length());
	rotation.z = acos(m_Direction.z / m_Direction.Length());
	if ((m_Direction.z < 0) && (m_Direction.x < 0))
		rotation *= -1.0f;
	if ((m_Direction.z > 0) && (m_Direction.x < 0))
		rotation *= -1.0f;

}

// Update the status

void BulletTwo::Render()
{
	if (m_CurrentLifeTime < 0.0f )
		return;

	MS * modelStack = SceneManager::getInstance()->getCurrentScene()->modelStack;
	modelStack->PushMatrix();
	// Reset the model stack
	modelStack->LoadIdentity();
	// We introduce a small offset to y position so that we can see the laser beam.
	modelStack->Translate(pos.x, pos.y - 0.001f, pos.z);
	//modelStack.Scale(scale.x, scale.y, scale.z);
	modelStack->PushMatrix();
	modelStack->Rotate(180 / Math::PI * rotation.z, 0.0f, 1.0f, 0.0f);
	modelStack->PushMatrix();
	//modelStack.Rotate(180 / Math::PI * angle_x, 1.0f, 0.0f, 0.0f);	// Not needed!
	modelStack->PushMatrix();
	modelStack->Rotate(180 / Math::PI * rotation.y, 1.0f, 0.0f, 0.0f);
	glLineWidth(5.0f);
	RenderHelper::RenderMesh(m_scene->meshList[type]);
	glLineWidth(1.0f);
	modelStack->PopMatrix();
	modelStack->PopMatrix();
	modelStack->PopMatrix();
	modelStack->PopMatrix();
}

int BulletTwo::RetrieveBulletDamage(WHOSEBULLET _bulletOwner)
{
	CLuaInterface::GetInstance()->SetLuaState("PlayerSettings");

	switch (_bulletOwner)
	{
	case SNIPER_BULLET:
		m_BulletDamage = CLuaInterface::GetInstance()->getIntValue("sniperBulletDamage");
		break;

	case ROCKET_BULLET:
		m_BulletDamage = CLuaInterface::GetInstance()->getIntValue("rifleBulletDamage");
		break;
	}

	return m_BulletDamage;
}

void BulletTwo::OnCollisionHit(GameObject * other){

	if (active) {
		active = false;
		//m_scene->sceneGraph.FindAndDestroy(other);
		m_scene->sceneGraph.FindAndDestroy(this);
	}
}
