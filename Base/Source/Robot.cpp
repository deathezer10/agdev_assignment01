#include "Robot.h"
#include "Bullet.h"
#include "Scraps.h"
#include "RobotBase.h"
#include "Bullet2.h"

Robot::Robot(Scene * scene, Vector3 pos, ROBOT_PART _robotPart) : EmptyObject(scene) {
	this->pos.Set(pos.x, pos.y, pos.z);
	m_robotPart = _robotPart;


}

Robot::~Robot() {
	m_scene->radar.removeUnit(this);
};


bool Robot::Update() {

	if (m_robotPart == ROBOT_BODY)
	{
		//Destroy robot upon 0 health and produce scrap metal as loot drop
		//And decrease enemy count
		if (m_Health <= 0.f) {
			RobotBase::m_enemyCount--;
			m_scene->goManager.CreateObject(new Scraps(m_scene, Vector3(pos.x, pos.y - 5.f, pos.z)), true);
			m_scene->sceneGraph.FindAndDestroy(m_Parent); // destroy parent since it is the one that holds all part including body, body is not a parent anymore!
			return false;
		}
	}


	if (collider.AABBvsAABB(m_scene->playerInfo->m_Collider)) {
		m_scene->playerInfo->TakeDamage(true);
	}

	return true;

}

void Robot::OnCollisionHit(GameObject* other) {

	SceneText* _scene = static_cast<SceneText*>(m_scene);

	//Tank's cannon ball
	if (other->type == Scene::GEO_CANNON_BALL)
	{
		if (m_robotPart == ROBOT_HEAD || m_robotPart == ROBOT_ARM) {
			m_scene->sceneGraph.FindAndDestroy(m_Parent);
		}

	}
	else
	{
		Bullet* _rocketBullet = dynamic_cast<Bullet*>(other);
		BulletTwo* _sniperBullet = dynamic_cast<BulletTwo*>(other);

		int bulletDamage;

		if (_sniperBullet || _rocketBullet) {

			if (_sniperBullet)
				bulletDamage = _sniperBullet->RetrieveBulletDamage(BulletTwo::SNIPER_BULLET);
			else
				bulletDamage = _rocketBullet->RetrieveBulletDamage(Bullet::ROCKET_BULLET);

			//if damage stack is not empty ,means that are damage area in it, so pop it 
			//and set the bool to false to reset _scene->playerInfo->m_damageInfoTime
			while (_scene->playerInfo->m_damageAreaStack.empty() == false)
			{
				_scene->playerInfo->m_damageAreaStack.pop();
				_scene->playerInfo->b_showDamageInfo = false;
			}


			// if is the body part , directly deduct the health
			if (m_robotPart == ROBOT_BODY) {
				m_Health -= bulletDamage;
				_scene->playerInfo->m_damageAreaStack.push(CPlayerInfo::DamageBody);
				_scene->playerInfo->m_Score += bulletDamage; // Increase Player score depending on where the bullet landed
				_scene->playerInfo->b_showDamageInfo = true;
			}
			else
			{
				//other parts of the robot ( arm and head)
				Robot * theBody = static_cast<Robot*>(m_Parent);

				// Different bullet different damage
				if (m_robotPart == ROBOT_HEAD) {
					theBody->m_Health -= bulletDamage * 2;
					_scene->playerInfo->m_damageAreaStack.push(CPlayerInfo::DamagedHead);
					_scene->playerInfo->m_Score += bulletDamage * 2;
				}
				else if (m_robotPart == ROBOT_ARM) {
					theBody->m_Health -= bulletDamage / 2;
					_scene->playerInfo->m_damageAreaStack.push(CPlayerInfo::DamagedArm);
					_scene->playerInfo->m_Score += bulletDamage / 2;
				}

				_scene->playerInfo->b_showDamageInfo = true;
			}
		}
	}
}

