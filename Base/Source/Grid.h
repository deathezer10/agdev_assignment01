#pragma once

#include "GameObject.h"
#include "Vector3.h"
#include <vector>
#include "FPSCamera.h"
using namespace std;

//Include GLEW
#include <GL/glew.h>
#include "SceneNode.h"

class Mesh;

class CGrid : public SceneNode {

protected:
	// We use a Vector3 to store the indices of this Grid within the Spatial Partition array.
	Vector3 index;
	// We use a Vector3 to store the size of this Grid within the Spatial Partition array.
	Vector3 size;
	// We use a Vector3 to store the x- and z-offset of this Grid.
	Vector3 offset;
	// We use a Vector3 to store the x- and z-offset of this Grid.
	Vector3 m_min, m_max;

	// The mesh to represent the grid
	Mesh* theMesh;

	// List of objects in this grid
	// vector<GameObject*> ListOfObjects;

	// The level of detail for this CGrid
	CLevelOfDetails::DETAIL_LEVEL theDetailLevel;
public:
	// Constructor
	CGrid(void);
	// Destructor
	~CGrid(void);

	// Init
	void Init(const int xIndex, const int zIndex,
		const int xGridSize, const int zGridSize,
		const float xOffset = 0, const float zOffset = 0);

	// Set a particular grid's Mesh
	void SetMesh(Mesh * aMesh);

	Mesh* GetMesh() { return theMesh; }

	// Update the grid
	void Update(vector<GameObject*>* migrationList);
	// Render the grid
	void Render(void);

	// Add a new object to this grid
	void Add(GameObject* theObject);
	// Remove but not delete all objects from this grid
	void Remove(void);
	// Remove but not delete an object from this grid
	bool Remove(GameObject* theObject);

	// Check if an object is in this grid
	bool IsHere(GameObject* theObject) const;

	// PrintSelf
	void PrintSelf();
	
	// Set the Level of Detail for objects in this CGrid
	void SetDetailLevel(const CLevelOfDetails::DETAIL_LEVEL theDetailLevel);
};
