#include "RobotBase.h"
#include "Robot.h"
#include "SceneManager.h"
#include "RenderHelper.h"
#include "GL\glew.h"
#include "LuaInterface.h"

int RobotBase::m_enemyDeadCount = 0;
int RobotBase::m_enemyCount = 0;

RobotBase::RobotBase(Scene * scene, Vector3 pos) : EmptyObject(scene), m_iWayPointIndex(-1) {
	m_enemyCount++;

	//Clear the waypoints
	listOfWayPoints.clear();

	//Body
	robotBody = new Robot(scene, Vector3(pos.x, pos.y, pos.z), Robot::ROBOT_BODY);
	robotBody->type = Scene::GEO_HIGH_LOD_ROBOT_BODY;
	robotBody->SetName("robotBody");
	robotBody->active = true;
	robotBody->getCollider().innerBoxSize = Vector3(6, 6, 6);
	robotBody->getCollider().type = Collider::C_AABB;
	robotBody->InitLOD(m_scene->meshList[Scene::GEO_HIGH_LOD_ROBOT_BODY], m_scene->meshList[Scene::GEO_MID_LOD_ROBOT_BODY], m_scene->meshList[Scene::GEO_LOW_LOD_ROBOT_BODY]);
	SceneManager::getInstance()->getCurrentScene()->goManager.CreateObject(robotBody, false);

	m_scene->radar.addUnit(robotBody);

	float armOffset = 4.5f;
	float headOffset = 5.f;

	//Head
	robotHead = new Robot(scene, Vector3(pos.x, pos.y + headOffset, pos.z), Robot::ROBOT_HEAD);
	robotHead->type = Scene::GEO_HIGH_LOD_ROBOT_HEAD;
	robotHead->active = true;
	robotHead->getCollider().innerBoxSize = Vector3(6, 5, 5);
	robotHead->getCollider().type = Collider::C_AABB;
	robotHead->InitLOD(m_scene->meshList[Scene::GEO_HIGH_LOD_ROBOT_HEAD], m_scene->meshList[Scene::GEO_MID_LOD_ROBOT_HEAD], m_scene->meshList[Scene::GEO_LOW_LOD_ROBOT_HEAD]);
	SceneManager::getInstance()->getCurrentScene()->goManager.CreateObject(robotHead, false);

	//Left  arm
	robotLeftArm = new Robot(scene, Vector3(pos.x - armOffset, pos.y, pos.z), Robot::ROBOT_ARM);
	robotLeftArm->type = Scene::GEO_HIGH_LOD_ROBOT_ARM;
	robotLeftArm->active = true;
	robotLeftArm->getCollider().innerBoxSize = Vector3(2.5f, 4.f, 2.5f);
	robotLeftArm->getCollider().type = Collider::C_AABB;
	robotLeftArm->InitLOD(m_scene->meshList[Scene::GEO_HIGH_LOD_ROBOT_ARM], m_scene->meshList[Scene::GEO_MID_LOD_ROBOT_ARM], m_scene->meshList[Scene::GEO_LOW_LOD_ROBOT_ARM]);
	SceneManager::getInstance()->getCurrentScene()->goManager.CreateObject(robotLeftArm, false);

	//Right arm
	robotRightArm = new Robot(scene, Vector3(pos.x + armOffset, pos.y, pos.z), Robot::ROBOT_ARM);
	robotRightArm->type = Scene::Scene::GEO_HIGH_LOD_ROBOT_ARM;
	robotRightArm->active = true;
	robotRightArm->getCollider().innerBoxSize = Vector3(2.5f, 4.f, 2.5f);
	robotRightArm->getCollider().type = Collider::C_AABB;
	robotRightArm->InitLOD(m_scene->meshList[Scene::GEO_HIGH_LOD_ROBOT_ARM], m_scene->meshList[Scene::GEO_MID_LOD_ROBOT_ARM], m_scene->meshList[Scene::GEO_LOW_LOD_ROBOT_ARM]);
	SceneManager::getInstance()->getCurrentScene()->goManager.CreateObject(robotRightArm, false);

	AddChild(robotBody);
	robotBody->AddChild(robotHead);
	robotBody->AddChild(robotLeftArm);
	robotBody->AddChild(robotRightArm);

	//Outer hitbox
	collider.outerBoxSize = Vector3(12.5f, 14.5f, 12.5f); // empty game object, only here for outerbox
	robotBody->getCollider().type = Collider::C_AABB;


	theWPManager = new  WayPointManager();
}

RobotBase::~RobotBase()
{
}

void RobotBase::Init()
{
	m_currentState = PATROL;

	m_defaultPosition.Set(pos.x, pos.y, pos.z);

	theWPManager->PrintSelf();


       //Set up the first waypoint for enemy to move to
	target = theWPManager->GetWaypoint(m_iWayPointIndex)->GetPosition();
	cout << "Next target: " << target << endl;


}

bool RobotBase::Update()
{

	switch (m_currentState)
	{
	case PATROL:
	{
		Vector3 differentInDist = (m_scene->playerInfo->position - robotBody->pos);

		if (differentInDist.Length() < m_distanceToSwitchState)
			m_currentState = CHASE;

		Vector3 viewVector;
		try
		{
			viewVector = (target - pos).Normalized() * m_patrolSpeed;
		}
		catch (DivideByZero)
		{
			viewVector.SetZero();
		}

		robotBody->pos += viewVector * 10.f * m_scene->_dt;
		robotHead->pos += viewVector * 10.f * m_scene->_dt;
		robotLeftArm->pos += viewVector * 10.f * m_scene->_dt;
		robotRightArm->pos += viewVector * 10.f * m_scene->_dt;

		pos = robotBody->pos;

		//Get the next waypoint provided is within 25.0f from the current waypoint
		if ((target - pos).LengthSquared() < 25.0f)
		{
			CWaypoint* nextWaypoint = GetNextWayPoint();
			if (nextWaypoint)
				target = nextWaypoint->GetPosition();
			else
				target = Vector3(0, 0, 0);
			cout << "Next target: " << target << endl;
		}

	}
	break;
	case CHASE:
	{
		

		Vector3 differentInDist = (m_scene->playerInfo->position - robotBody->pos);

		if (differentInDist.Length() >= m_distanceToSwitchState)
		{
			m_currentState = PATROL;
			CWaypoint * thePoint = theWPManager->GetNearestWaypoint(this->pos);
			target = thePoint->GetPosition();
			break;
		}

		//Stop enemy movment if is too near the player
		if (differentInDist.Length() >= 10.f) {
			//Chasing codes
			Vector3 dir = (differentInDist).Normalized() * m_Chase_Speed;
			dir.y = 0;

			robotBody->vel = dir;
			robotBody->rotation.y = Math::RadianToDegree(atan2(dir.x, dir.z)) + 180;
			robotBody->pos += robotBody->vel * m_scene->_dt;

			robotHead->vel = dir;
			robotHead->rotation.y = Math::RadianToDegree(atan2(dir.x, dir.z)) + 180;
			robotHead->pos += robotHead->vel * m_scene->_dt;

			robotLeftArm->vel = dir;
			robotLeftArm->rotation.y = Math::RadianToDegree(atan2(dir.x, dir.z)) + 180;
			robotLeftArm->pos += robotLeftArm->vel * m_scene->_dt;

			robotRightArm->vel = dir;
			robotRightArm->rotation.y = Math::RadianToDegree(atan2(dir.x, dir.z)) + 180;
			robotRightArm->pos += robotRightArm->vel * m_scene->_dt;

			pos = robotBody->pos;
		}

	}
	break;
	}

	return true;

}

CWaypoint * RobotBase::GetNextWayPoint()
{
	if ((int)listOfWayPoints.size() > 0)
	{
		m_iWayPointIndex++;
		if (m_iWayPointIndex >= (int)listOfWayPoints.size())
			m_iWayPointIndex = 0;
		return theWPManager->GetWaypoint(listOfWayPoints[m_iWayPointIndex]);
	}
	else
		return NULL;
}



void RobotBase::RenderWayPointLines()
{
	for (size_t i = 0; i < listOfWayPoints.size(); ++i)
	{
		Vector3 curLocation = theWPManager->GetWaypoint(i)->GetPosition();
		int nextIndex = i + 1;
		if (nextIndex >= (int)listOfWayPoints.size())
			nextIndex = 0;

		Vector3 nextLocation = theWPManager->GetWaypoint(nextIndex)->GetPosition();

		Vector3 lineDir = nextLocation - curLocation;
		

		try
		{
			lineDir.Normalized();
		}
		catch (DivideByZero)
		{

		}

		float rotateValue = Math::RadianToDegree(atan2(lineDir.x, lineDir.z)) - 90 ;
		Vector3 scaleLength = (lineDir).Length() / 2.f;

		Vector3 difference =( nextLocation - curLocation) / 2.f;

		m_scene->modelStack->PushMatrix();
		m_scene->modelStack->Translate(curLocation.x + difference.x,curLocation.y - 10.f, curLocation.z + difference.z);
		m_scene->modelStack->Rotate(rotateValue , 0 ,1 ,0);
		m_scene->modelStack->Scale(scaleLength.x , 1, scaleLength.z);
		RenderHelper::RenderMesh(m_scene->meshList[Scene::GEO_WAY_POINT_LINE]);
		m_scene->modelStack->PopMatrix();
	
	}

}

void RobotBase::FillUpListOfWayPoint(int sizeOfWayPoints)
{
	//Fill up the vector containing IDs of the waypoint
	for (int j = 0; j < sizeOfWayPoints; ++j)
	{
		listOfWayPoints.push_back(j);
	}
}

void RobotBase::SetCurrentWayPointIndex(int index)
{
	m_iWayPointIndex = index;
}

