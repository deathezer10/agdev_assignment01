#pragma once

#include "UIManager.h"
#include "Vector2.h"
#include <string>
#include <map>

class Mesh;


struct GUIButton {

	friend class GUI;

public:
	GUIButton() {
		id = -1;
		position.Set(0, 0);
		scale.Set(0, 0);
		text = "";
		btnState = NONE;
	};

	// Button position is in percentage of m_worldWidth & m_worldHeight, e.g. x = 0.5f is half of the screen's width
	GUIButton(int _id, Vector2 _pos, Vector2 _scale, const std::string _text) {
		id = _id;
		position = _pos;
		scale = _scale;
		text = _text;
		btnState = NONE;
	}

	// Button position is in percentage of m_worldWidth & m_worldHeight, e.g. x = 0.5f is half of the screen's width
	GUIButton(int _id, Vector2 _pos, Vector2 _scale, char _text) {
		id = _id;
		position = _pos;
		scale = _scale;
		text = _text;
		btnState = NONE;
	}

	enum STATE_BUTTON {
		NONE,
		HOVER,
		DOWN,
		CLICKED
	};

	bool active = true;
	int id;
	Vector2 position; // Button position is in percentage of m_worldWidth & m_worldHeight, e.g. x = 0.5f is half of the screen's width
	Vector2 scale;
	std::string text;	

	bool OnIdle() { return btnState == NONE; }
	bool OnDown() { return btnState == DOWN; }
	bool OnClick() {
		if (btnState == CLICKED) {
			btnState = NONE;
			return true;
		}
		return false;
	}

private:
	STATE_BUTTON btnState;

	bool PointToAABB(Vector2 point);

};

class GUI {

public:
	GUI() {}
	~GUI() {};

	// Sets the meshes for the GUIButton, all buttons uses the same mesh currently
	void SetButtonMesh(Mesh* btnNormal, Mesh* btnHover, UIManager* uiManager = nullptr) {
		m_btnMeshNormal = btnNormal;
		m_btnMeshHover = btnHover;
		m_UIManager = uiManager;
	}

	// Adds the created button into collection
	// Call GUI::UpdateButtons() & GUI::RenderButtons() to make buttons work
	void AddButton(GUIButton* btn);

	// Delete the btnID from collection
	void RemoveButton(int btnID);

	// Processes mouse events for all buttons added in the collection
	void UpdateButtons();
	
	// Renders the texture and state of all the buttons
	void RenderButtons();

	// Exit and clean up resources
	void Exit();

private:
	Mesh* m_btnMeshNormal;
	Mesh* m_btnMeshHover;
	UIManager* m_UIManager = nullptr;

	// <ButtonID, GUIButton>
	std::map<int, GUIButton*> m_Buttons;

	float m_worldWidth;
	float m_worldHeight;
	
};
