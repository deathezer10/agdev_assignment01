#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "Application.h"
#include "SceneMainMenu.h"
#include "SceneOptions.h"
#include "SceneManager.h"
#include "SceneText.h"
#include "GraphicsManager.h"
#include "MouseController.h"
#include "KeyboardController.h"
#include "RenderHelper.h"
#include "MeshBuilder.h"
#include "LoadTGA.h"
#include "LuaInterface.h"



void SceneOptions::Init()
{
	SceneBase::Init();


	// Create the playerinfo instance, which manages all information about the player
	playerInfo = CPlayerInfo::GetInstance();
	playerInfo->Init();

	// Create and attach the camera to the scene
	camera.Init(playerInfo->GetPos(), playerInfo->GetTarget(), playerInfo->GetUp());
	playerInfo->AttachCamera(&camera);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	meshList[GEO_MAINMENU] = MeshBuilder::GenerateRadarQuad("GEO_MAINMENU", Color(1, 1, 1));
	meshList[GEO_MAINMENU]->textureID = LoadTGA("Image/mainmenubg.tga", false);

	glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	MouseController::GetInstance()->SetKeepMouseCentered(false);

	// Use PlayerControls first
	CLuaInterface::GetInstance()->SetLuaState("PlayerControls");

	hotkeySkybox = CLuaInterface::GetInstance()->getIntValue("Skybox");
	hotkeyW = CLuaInterface::GetInstance()->getIntValue("MoveFront");
	hotkeyS = CLuaInterface::GetInstance()->getIntValue("MoveBack");
	hotkeyA = CLuaInterface::GetInstance()->getIntValue("MoveLeft");
	hotkeyD = CLuaInterface::GetInstance()->getIntValue("MoveRight");
	hotkey1 = CLuaInterface::GetInstance()->getIntValue("Primary");
	hotkey2 = CLuaInterface::GetInstance()->getIntValue("Secondary");
	hotkeyLShift = CLuaInterface::GetInstance()->getIntValue("Sprint");

	// Revert to default lua
	CLuaInterface::GetInstance()->SetLuaState("");

	btnSave = new GUIButton(2, Vector2(0.45f, 0.1f), Vector2(12, 7), "Save");
	m_GUI.AddButton(btnSave);

	btnQuit = new GUIButton(3, Vector2(0.55f, 0.1f), Vector2(12, 7), "Back");
	m_GUI.AddButton(btnQuit);

	btnSkybox = new GUIButton(4, Vector2(0.5f, 0.75f), Vector2(17, 7), ((hotkeySkybox == 0) ? "Version 1" : "Version 2"));
	m_GUI.AddButton(btnSkybox);

	btnW = new GUIButton(5, Vector2(0.5f, 0.65f), Vector2(10, 7), glfwGetKeyName(hotkeyW, 0));
	m_GUI.AddButton(btnW);

	btnS = new GUIButton(6, Vector2(0.5f, 0.55f), Vector2(10, 7), glfwGetKeyName(hotkeyS, 0));
	m_GUI.AddButton(btnS);

	btnA = new GUIButton(7, Vector2(0.5f, 0.45f), Vector2(10, 7), glfwGetKeyName(hotkeyA, 0));
	m_GUI.AddButton(btnA);

	btnD = new GUIButton(8, Vector2(0.5f, 0.35f), Vector2(10, 7), glfwGetKeyName(hotkeyD, 0));
	m_GUI.AddButton(btnD);

	btn1 = new GUIButton(9, Vector2(0.8f, 0.75f), Vector2(10, 7), glfwGetKeyName(hotkey1, 0));
	m_GUI.AddButton(btn1);

	btn2 = new GUIButton(10, Vector2(0.8f, 0.65f), Vector2(10, 7), glfwGetKeyName(hotkey2, 0));
	m_GUI.AddButton(btn2);

	auto keyname = GetModKeyName(hotkeyLShift);

	if (!keyname) {
		keyname = glfwGetKeyName(hotkeyLShift, 0);
	}

	btnLShift = new GUIButton(11, Vector2(0.5f, 0.25f), Vector2(15, 7), keyname);
	m_GUI.AddButton(btnLShift);

}

const char* SceneOptions::GetModKeyName(int key) {

	switch (key) {

	case GLFW_KEY_LEFT_SHIFT:
		return "LShift";

	case GLFW_KEY_LEFT_ALT:
		return "LAlt";

	case GLFW_KEY_LEFT_CONTROL:
		return "LCtrl";

	case GLFW_KEY_RIGHT_SHIFT:
		return "RShift";

	case GLFW_KEY_RIGHT_ALT:
		return "RAlt";

	case GLFW_KEY_RIGHT_CONTROL:
		return "RCtrl";

	case GLFW_KEY_SPACE:
		return "Space";

	case GLFW_KEY_BACKSPACE:
		return "Backspace";

	case GLFW_KEY_ESCAPE:
		return "Escape";

	default:
		return nullptr;

	}

}

void SceneOptions::SetBindingKey(int key) {
	if (isBinding && currHotkey && currButtonBinding) {

		// Double Check for valid key name
		auto keyname = glfwGetKeyName(key, 0);

		if (!keyname) {
			keyname = GetModKeyName(key);
		}

		if (keyname) {
			currButtonBinding->text = keyname;
			*currHotkey = key;
		}

		// Reset variables once done
		currHotkey = nullptr;
		isBinding = false;
		currButtonBinding = nullptr;
	}
}

void SceneOptions::Update(double dt) {

	SceneBase::Update(dt);

	if (isBinding) {

	}
	else {

		if (btnSave->OnClick()) {

			// TODO: save to lua file
			auto state = CLuaInterface::GetInstance()->thePlayerControlLuaState;
			CLuaInterface::GetInstance()->saveIntValue("MoveFront", hotkeyW, "PlayerControls", true);
			CLuaInterface::GetInstance()->saveIntValue("MoveBack", hotkeyS, "PlayerControls");
			CLuaInterface::GetInstance()->saveIntValue("MoveLeft", hotkeyA, "PlayerControls");
			CLuaInterface::GetInstance()->saveIntValue("MoveRight", hotkeyD, "PlayerControls");
			CLuaInterface::GetInstance()->saveIntValue("Primary", hotkey1, "PlayerControls");
			CLuaInterface::GetInstance()->saveIntValue("Secondary", hotkey2, "PlayerControls");
			CLuaInterface::GetInstance()->saveIntValue("Sprint", hotkeyLShift, "PlayerControls");
			CLuaInterface::GetInstance()->saveIntValue("Skybox", hotkeySkybox, "PlayerControls");
			SceneManager::getInstance()->changeScene(new SceneMainMenu());
		}

		if (btnQuit->OnClick()) {
			SceneManager::getInstance()->changeScene(new SceneMainMenu());
		}

		if (btnSkybox->OnClick()) {
			hotkeySkybox = ((hotkeySkybox == 0) ? 1 : 0); // Toggle between 0 and 1
			btnSkybox->text = ((hotkeySkybox == 0) ? "Version 1" : "Version 2");
		}

		if (btnW->OnClick()) {
			isBinding = true;
			currHotkey = &hotkeyW;
			currButtonBinding = btnW;
		}

		if (btnS->OnClick()) {
			isBinding = true;
			currHotkey = &hotkeyS;
			currButtonBinding = btnS;
		}

		if (btnA->OnClick()) {
			isBinding = true;
			currHotkey = &hotkeyA;
			currButtonBinding = btnA;
		}

		if (btnD->OnClick()) {
			isBinding = true;
			currHotkey = &hotkeyD;
			currButtonBinding = btnD;
		}

		if (btn1->OnClick()) {
			isBinding = true;
			currHotkey = &hotkey1;
			currButtonBinding = btn1;
		}

		if (btn2->OnClick()) {
			isBinding = true;
			currHotkey = &hotkey2;
			currButtonBinding = btn2;
		}

		if (btnLShift->OnClick()) {
			isBinding = true;
			currHotkey = &hotkeyLShift;
			currButtonBinding = btnLShift;
		}

	}

	GraphicsManager::GetInstance()->UpdateLights(dt);
}

void SceneOptions::Render() {

	SceneBase::Render();

	GraphicsManager::GetInstance()->SetPerspectiveProjection(45, Application::windowWidth() / Application::windowHeight(), 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Setup 2D pipeline then render 2D
	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();

	textManager.dequeueMesh();

	float winHeight = (float)Application::windowHeight() / 10;
	float winWidth = (float)Application::windowWidth() / 10;

	textManager.RenderMeshOnScreen(meshList[GEO_MAINMENU], winWidth * 0.5f, winHeight * 0.5f, Vector3(90, 0, 0), Vector3(winWidth / 2, 1, winHeight / 2));

	textManager.renderTextOnScreen(Vector3(0.475f, 0.9f, 0), Vector3(2, 2, 1), "Controls", Color(1, 1, 1));

	Color textColor(1, 1, 1);

	if (isBinding) {
		textManager.renderTextOnScreen(Vector3(0.5f, 0.5f, 0), Vector3(1, 1, 1), "Press any <Key>", textColor);
	}
	else {
		m_GUI.RenderButtons();

		textManager.renderTextOnScreen(Vector3(0.4f, 0.75f, 0), Vector3(1, 1, 1), "Skybox:", textColor);
		textManager.renderTextOnScreen(Vector3(0.4f, 0.65f, 0), Vector3(1, 1, 1), "Forward:", textColor);
		textManager.renderTextOnScreen(Vector3(0.4f, 0.55f, 0), Vector3(1, 1, 1), "Backward:", textColor);
		textManager.renderTextOnScreen(Vector3(0.4f, 0.45f, 0), Vector3(1, 1, 1), "Left:", textColor);
		textManager.renderTextOnScreen(Vector3(0.4f, 0.35f, 0), Vector3(1, 1, 1), "Right:", textColor);
		textManager.renderTextOnScreen(Vector3(0.685f, 0.75f, 0), Vector3(1, 1, 1), "Primary Gun:", textColor);
		textManager.renderTextOnScreen(Vector3(0.675f, 0.65f, 0), Vector3(1, 1, 1), "Secondary Gun:", textColor);
		textManager.renderTextOnScreen(Vector3(0.4f, 0.25f, 0), Vector3(1, 1, 1), "Sprint:", textColor);
	}


	textManager.dequeueText();
	textManager.RenderTimedMeshOnScreen();
	textManager.reset(); // Must be called at the end of Render()
}

void SceneOptions::Exit()
{
	m_GUI.Exit();

	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();
	playerInfo->DetachCamera();

	playerInfo->DropInstance();

	goManager.Exit();

	GraphicsManager::GetInstance()->RemoveLight("lights[0]");

	SceneBase::Exit();
}
