#ifndef BULLET2_H
#define BULLET2_H


#include "GameObject.h"

class BulletTwo : public GameObject {
	
public:
	enum WHOSEBULLET
	{
		SNIPER_BULLET,
		ROCKET_BULLET,
	};

	BulletTwo(Scene* scene, Vector3 pos, Vector3 dir, bool isEnemyProjectile);
	~BulletTwo() {};

	virtual bool Update();
	virtual void Render();
	void CalculateAngles(void);

	//return bullet damage based on which bullet is it
	int RetrieveBulletDamage(WHOSEBULLET _bulletOwner);
private:
	int m_BulletDamage;
	float m_BulletSpeed;
	bool m_EnemyProjectile;

	float m_BulletMaxDistance;
	float m_CurrentLifeTime = 0;

	Vector3 m_Direction;

	virtual void OnCollisionHit(GameObject* other);

	bool m_IsFirstFrame = true;

};
#endif