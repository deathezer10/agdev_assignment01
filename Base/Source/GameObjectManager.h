#pragma once

#include <vector>
#include <queue>
#include "GameObject.h"
#include <list>

using std::vector;

class CSpatialPartition;

class GameObjectManager {

public:

	GameObjectManager() {};
	~GameObjectManager();

	// Adds the Object into queue so that it gets added to the vector next frame
	// Used when adding Objects while iterating through m_goList
	void CreateObjectQueue(GameObject * obj);

	// Stores the Object into collection to allow interaction
	void CreateObject(GameObject* obj , bool addToSP = true);

	// Destroys the Object after this frame finishes
	void DestroyObjectQueue(GameObject * obj);

	// Remove the given Object from the vector
	void DestroyObject(GameObject* obj);

	// Render all Objects in the collection into the Scene
	void RenderObjects();
	
	// Process interactions for all Object, should be called during Update()
	void UpdateObjects();

	// Updates the current iterator, used when an element is deleted from the container to prevent other iterators from being invalidated
	void ValidateIterator(std::list<GameObject*>::iterator it);

	// Push all pending Objects into m_goList
	void DequeueObjects();

	// Do neccessary cleaning up of memory allocated
	void Exit();

	std::list<GameObject*> m_goList;

	// Set the spatial partition
	void SetSpatialPartition(CSpatialPartition* theSpatialPartition);

	void RenderSpatialGrid();

private:
	// Was the iterator updated?
	bool _iteratorUpdated = false;

	std::list<GameObject*>::iterator m_goListIterator;
	std::queue<GameObject*> m_goQueue;
	std::queue<GameObject*> m_destroyQueue;
	// Handler to Spatial Partition
	CSpatialPartition* m_spatialPartition;

};