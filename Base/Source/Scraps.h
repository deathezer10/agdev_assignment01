#ifndef SCRAPS_H
#define SCRAPS_H


#include "GameObject.h"

class Scraps : public GameObject {


public:
	Scraps(Scene* scene, Vector3 pos);
	~Scraps();

	virtual bool Update();

private:

};
#endif