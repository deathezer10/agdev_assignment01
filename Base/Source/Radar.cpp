#include "Application.h"
#include "Radar.h"
#include "Scene.h"

#include "GL\glew.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"
#include "Turret.h"

#include <algorithm>


Radar::Radar(Scene* scene) {
	_scene = scene;

	_RadarRange = 10.0f; // Distance before the Radar can detect any enemy, change this to increase Radar's detection range
	_RadarSize = 6; // Background size of the radar
	_RadarEnemySize = 0.01f; // Enemy Icon Size: Scaling is relative to _RadarSize :: (This * _RadarSize)
	_RadarPlayerSize = 0.01f; // Player Icon Size: Scaling is relative to _RadarSize :: (This * _RadarSize)
	_RadarSizeSquared = (_RadarSize * _RadarSize)  * _RadarRange; // Helper variable: Square length of the Radar
}

void Radar::SetRadarRange(float range) {
	_RadarRange = range; // Distance before the Radar can detect any enemy, change this to increase Radar's detection range
	_RadarSizeSquared = (_RadarSize * _RadarSize)  * _RadarRange; // Helper variable: Square length of the Radar
}

void Radar::addUnit(GameObject* unit) {
	unitContainer.push_back(unit);
}

void Radar::removeUnit(GameObject* unit) {
	unitContainer.erase(std::remove(unitContainer.begin(), unitContainer.end(), unit), unitContainer.end());
}

void Radar::RenderRadar(float x, float y) {
	glDisable(GL_DEPTH_TEST);

	// Backup
	Mtx44 originalProjStack = GraphicsManager::GetInstance()->GetProjectionMatrix();
	Mtx44 originalViewStack = GraphicsManager::GetInstance()->GetViewMatrix();

	Mtx44 ortho;
	ortho.SetToOrtho(0, Application::windowWidth() / 10, 0, Application::windowHeight() / 10, -10, 10); //size of screen UI
	GraphicsManager::GetInstance()->projectionMatrix = ortho; // set to ortho first
	GraphicsManager::GetInstance()->viewMatrix.SetToIdentity();

	_scene->modelStack->PushMatrix();
	_scene->modelStack->LoadIdentity();
	_scene->modelStack->Translate(x, y, 1);
	_scene->modelStack->Rotate(90, 1, 0, 0);
	_scene->modelStack->Scale(_RadarSize, 1, _RadarSize);
	RenderHelper::RenderMesh(_scene->meshList[Scene::GEO_RADAR_BACKGROUND]);

	// Go through the number of enemies and translate them relative to the Radar's position
	for (auto it = unitContainer.begin(); it != unitContainer.end(); ++it) {
		Vector3 direction = _scene->playerInfo->position - (*it)->getCollider().getPosition();

		// Skip if enemy is too far from player
		if (((direction.HorizontalLengthSquared() / 100) / _RadarRange) >= _RadarSizeSquared)
			continue;

		const float borderSize = 13; // Higher = enemies faster disappear when reaching border

		_scene->modelStack->PushMatrix();
		_scene->modelStack->Rotate(-_scene->camera.currentYaw, 0, 1, 0); // Rotate first
		_scene->modelStack->Translate((direction.x / (borderSize * _RadarSize)) / _RadarRange, 1, (direction.z / (borderSize * _RadarSize)) / _RadarRange);
		_scene->modelStack->Scale(_RadarSize * _RadarEnemySize, 1, _RadarSize * _RadarEnemySize);

		if (dynamic_cast<Turret*>(*it))
			RenderHelper::RenderMesh(_scene->meshList[Scene::GEO_RADAR_CASTLE]);
		else
			RenderHelper::RenderMesh(_scene->meshList[Scene::GEO_RADAR_ENEMY]);

		_scene->modelStack->PopMatrix();
	}

	// Render Player
	_scene->modelStack->PushMatrix();
	_scene->modelStack->Translate(0, 2, 0);
	_scene->modelStack->Scale(_RadarSize * _RadarPlayerSize, 1, _RadarSize * _RadarPlayerSize);
	RenderHelper::RenderMesh(_scene->meshList[Scene::GEO_RADAR_PLAYER]);
	_scene->modelStack->PopMatrix();

	GraphicsManager::GetInstance()->projectionMatrix = originalProjStack;
	GraphicsManager::GetInstance()->viewMatrix = originalViewStack;
	_scene->modelStack->PopMatrix();
	glEnable(GL_DEPTH_TEST);
}