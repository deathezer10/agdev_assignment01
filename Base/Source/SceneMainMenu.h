#pragma once 

#include "SceneBase.h"
#include "Mtx44.h"
#include "Mesh.h"
#include "MatrixStack.h"

class SceneMainMenu : public SceneBase {

public:
	SceneMainMenu() {};
	~SceneMainMenu() {};

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

private:
	Light* lights[1];

	GUIButton* btnStartGame;
	GUIButton* btnHighScore;
	GUIButton* btnOptions;
	GUIButton* btnQuit;
	
};