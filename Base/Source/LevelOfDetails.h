#pragma once
#include "Vector3.h"
#include "Mesh.h"

class CLevelOfDetails
{
public:
	Mesh* modelMesh_HighDetails;
	Mesh* modelMesh_MidDetails;
	Mesh* modelMesh_LowDetails;

	enum DETAIL_LEVEL
	{
		NO_DETAILS = 0,
		HIGH_DETAILS,
		MID_DETAILS,
		LOW_DETAILS,
		NUM_DETAIL_LEVEL,
	};

	CLevelOfDetails();
	virtual ~CLevelOfDetails();

	bool InitLOD(Mesh * _meshHigh, Mesh * _meshMid, Mesh * _meshLow);
	bool DestroyLOD(void);

	void SetLODStatus(const bool bActive);
	bool GetLODStatus(void) const;

	bool SetLODMesh(Mesh* theMesh, const DETAIL_LEVEL theDetailLevel);
	Mesh* GetLODMesh(void) const;
	Mesh* GetLODMesh(const DETAIL_LEVEL theDetailLevel) const;
	DETAIL_LEVEL GetDetailLevel(void) const;
	bool SetDetailLevel(const DETAIL_LEVEL theDetailLevel);

protected:
	bool m_bActive;
	DETAIL_LEVEL theDetailLevel;
};
