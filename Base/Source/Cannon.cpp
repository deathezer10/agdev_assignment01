#include "Cannon.h"
#include "Bullet.h"
#include "Scene.h"
#include "SceneManager.h"

CCannon::CCannon() : CWeaponInfo("Cannon")
{
}


CCannon::~CCannon()
{
}

// Initialise this instance to default values
void CCannon::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 3;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 3;
	// The current total number of rounds currently carried by this player
	totalRounds = 0;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 0;
	//
	maxClip = 3;
	//
	currentClip = maxClip;

	reloadTime = 2;

	// The time between shots
	timeBetweenShots = 1.0f;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	bulletMeshName = "cube";
}

void CCannon::Discharge(Vector3 position, Vector3 target, CPlayerInfo * _source)
{
	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	float eTime = SceneManager::getInstance()->getCurrentScene()->m_elapsedTime;


	if (bFire) {
		// If there is still ammo in the magazine, then fire
		if (eTime > nextShootTime) {

			const float yCannonOffset = 5;

			Vector3 tempPos = position;
			Vector3 tempTarget = target;
			Vector3 dir = (tempTarget - tempPos).Normalized();
			tempPos += (tempTarget - tempPos).Normalized() * 50;
			tempPos.y = 0;


			Bullet* bullet = new Bullet(_scene, tempPos, dir, false);
			bullet->type = Scene::GEO_CANNON_BALL;
			bullet->getCollider().setBoundingBoxSize(Vector3(8, 8, 8));
			bullet->scale.Set(5, 5, 5);
			_scene->goManager.CreateObject(bullet);

			bFire = false;
			
			nextShootTime = (float)timeBetweenShots + eTime;
		}
	}
}
