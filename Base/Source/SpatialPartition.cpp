#include "SpatialPartition.h"
#include "stdio.h"
#include "RenderHelper.h"
#include "SceneManager.h"

template <typename T> vector<T> concat(vector<T> &a, vector<T> &b) {
	vector<T> ret = vector<T>();
	copy(a.begin(), a.end(), back_inserter(ret));
	copy(b.begin(), b.end(), back_inserter(ret));
	return ret;
}

// Allocating and initializing CPlayerInfo's static data member.  
// The pointer is allocated but not the object's constructor.
CSpatialPartition *CSpatialPartition::sp_instance = 0;

/********************************************************************************
 Constructor
 ********************************************************************************/
CSpatialPartition::CSpatialPartition(void)
	: theGrid(NULL)
	, xSize(0)
	, zSize(0)
	, xGridSize(0)
	, zGridSize(0)
	, xNumOfGrid(0)
	, zNumOfGrid(0)
	, yOffset(0.0f)
	, theCamera(NULL)
{
}

/********************************************************************************
 Destructor
 ********************************************************************************/
CSpatialPartition::~CSpatialPartition(void)
{
	theCamera = NULL;
	delete[] theGrid;
}

/********************************************************************************
 Initialise the spatial partition
 ********************************************************************************/
bool CSpatialPartition::Init(const int xGridSize, const int zGridSize,
	const int xNumOfGrid, const int zNumOfGrid,
	const float yOffset)
{
	if ((xGridSize > 0) && (zGridSize > 0)
		&& (xNumOfGrid > 0) && (zNumOfGrid > 0))
	{
		this->xNumOfGrid = xNumOfGrid;
		this->zNumOfGrid = zNumOfGrid;
		this->xGridSize = xGridSize;
		this->zGridSize = zGridSize;
		this->xSize = xGridSize * xNumOfGrid;
		this->zSize = zGridSize * zNumOfGrid;
		this->yOffset = yOffset;

		// Create an array of grids
		theGrid = new CGrid[xNumOfGrid*zNumOfGrid];

		SceneNode* root = SceneManager::getInstance()->getCurrentScene()->sceneGraph.GetRoot();

		// Initialise the array of grids
		for (int i = 0; i < xNumOfGrid; i++)
		{
			for (int j = 0; j < zNumOfGrid; j++)
			{
				theGrid[i*zNumOfGrid + j].Init(i, j, xGridSize, zGridSize, (float)(xSize >> 1), (float)(zSize >> 1));
				root->AddChild(&theGrid[i*zNumOfGrid + j]);
			}
		}


		// Create a migration list vector
		MigrationList.clear();

		return true;
	}
	return false;
}


/********************************************************************************
Set a particular grid's Mesh
 ********************************************************************************/
void CSpatialPartition::ApplyMesh(Mesh * mesh)
{

	int counter = 0;

	for (int i = 0; i < xNumOfGrid; i++) {
		for (int j = 0; j < zNumOfGrid; j++) {
			theGrid[i*zNumOfGrid + j].SetMesh(mesh);
		}
	}
}

/********************************************************************************
Update the spatial partition
********************************************************************************/
void CSpatialPartition::Update(void)
{
	for (int i = 0; i < xNumOfGrid; i++)
	{
		for (int j = 0; j < zNumOfGrid; j++)
		{
			theGrid[i*zNumOfGrid + j].Update(&MigrationList);

			// Check visibility
			if (IsVisible(theCamera->GetCameraPos(),
				theCamera->GetCameraTarget() - theCamera->GetCameraPos(), i, j) == true)
			{
				// Calculate LOD for this CGrid
				float distance = CalculateDistance(&(theCamera->GetCameraPos()), i, j);
				if (distance < LevelOfDetails_Distances[0])
				{
					theGrid[i*zNumOfGrid + j].SetDetailLevel(CLevelOfDetails::HIGH_DETAILS);
				}
				else if (distance >= LevelOfDetails_Distances[0] && distance < LevelOfDetails_Distances[1])
				{
					theGrid[i*zNumOfGrid + j].SetDetailLevel(CLevelOfDetails::MID_DETAILS);
				}
				else if (distance >= LevelOfDetails_Distances[1])
				{
					theGrid[i*zNumOfGrid + j].SetDetailLevel(CLevelOfDetails::LOW_DETAILS);
				}
			}
			else
				theGrid[i*zNumOfGrid + j].SetDetailLevel(CLevelOfDetails::NO_DETAILS);

		}
	}

	// If there are objects due for migration, then process them
	if (MigrationList.empty() == false)
	{
		// Check each object to see if they are no longer in this grid
		for (size_t i = 0; i < MigrationList.size(); ++i)
		{
			Add(MigrationList[i]);
		}

		MigrationList.clear();
	}
}

/********************************************************************************
Render the spatial partition
********************************************************************************/
void CSpatialPartition::Render(Vector3* theCameraPosition)
{
	// Render the Spatial Partitions
	MS * modelStack = SceneManager::getInstance()->getCurrentScene()->modelStack;
	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	//Render player position on the airstrike camera
	if ((!_scene->playerInfo->IsAirstrikeFalling() && _scene->playerInfo->GetTypeOfCamera() == CPlayerInfo::AIRSTRIKE_CAMERA))
	{
		modelStack->PushMatrix();
		modelStack->Translate(_scene->playerInfo->positionBeforeAirStrike.x, 1.0f, _scene->playerInfo->positionBeforeAirStrike.z);
		modelStack->Scale(5, 1, 5.f);
		RenderHelper::RenderMesh(_scene->meshList[Scene::GEO_RADAR_PLAYER]);
		modelStack->PopMatrix();
	}


	modelStack->PushMatrix();
	modelStack->Translate((float)(xGridSize / 2), yOffset, (float)(zGridSize / 2));
	for (int i = 0; i < xNumOfGrid; i++)
	{
		for (int j = 0; j < zNumOfGrid; j++)
		{
			if ((_scene->playerInfo->GetTypeOfCamera() != CPlayerInfo::AIRSTRIKE_CAMERA && _scene->playerInfo->GetTypeOfCamera() != CPlayerInfo::MAPANALYSER_CAMERA)
				&& theGrid[i*zNumOfGrid + j].m_Child.empty())
				continue;

			if (_scene->playerInfo->GetTypeOfCamera() != CPlayerInfo::AIRSTRIKE_CAMERA)
				glDisable(GL_DEPTH_TEST);

			modelStack->PushMatrix();
			modelStack->Translate((float)(xGridSize*i - (xSize >> 1)), 1.0f, (float)(zGridSize*j - (zSize >> 1)));
			modelStack->PushMatrix();
			modelStack->Scale((float)xGridSize, 1.f, (float)zGridSize);
			//modelStack->Rotate(-90, 1, 0, 0);

			if ((!_scene->playerInfo->IsAirstrikeFalling() && _scene->playerInfo->GetTypeOfCamera() == CPlayerInfo::AIRSTRIKE_CAMERA) || _scene->playerInfo->GetTypeOfCamera() == CPlayerInfo::MAPANALYSER_CAMERA)
			{

				//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				RenderHelper::RenderMesh(theGrid[i*zNumOfGrid + j].GetMesh());
				//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

				if (_scene->playerInfo->GetTypeOfCamera() == CPlayerInfo::MAPANALYSER_CAMERA
					&& IsVisible(_scene->playerInfo->m_BackupPosition, _scene->playerInfo->target - _scene->playerInfo->m_BackupPosition, i, j)) {

					modelStack->PushMatrix();
					modelStack->Translate(0, 3.f, 0);
					RenderHelper::RenderMesh(_scene->meshList[Scene::GEO_GRID_SELECTED]);
					modelStack->PopMatrix();
				}
			}
			glEnable(GL_DEPTH_TEST);

			modelStack->PushMatrix();
			modelStack->LoadIdentity();
			theGrid[i*zNumOfGrid + j].Render();
			modelStack->PopMatrix();


			modelStack->PopMatrix();
			modelStack->PopMatrix();
		}
	}

	modelStack->PopMatrix();
}

/********************************************************************************
 Get xSize of the entire spatial partition
********************************************************************************/
int CSpatialPartition::GetxSize(void) const
{
	return xSize;
}
/********************************************************************************
 Get zSize of the entire spatial partition
********************************************************************************/
int CSpatialPartition::GetzSize(void) const
{
	return zSize;
}
/********************************************************************************
 Get xSize
********************************************************************************/
int CSpatialPartition::GetxGridSize(void) const
{
	return xGridSize;
}
/********************************************************************************
 Get zNumOfGrid
********************************************************************************/
int CSpatialPartition::GetzGridSize(void) const
{
	return zGridSize;
}
/********************************************************************************
Get xNumOfGrid
********************************************************************************/
int CSpatialPartition::GetxNumOfGrid(void) const
{
	return xNumOfGrid;
}
/********************************************************************************
Get zNumOfGrid
********************************************************************************/
int CSpatialPartition::GetzNumOfGrid(void) const
{
	return zNumOfGrid;
}

/********************************************************************************
 Get a particular grid
 ********************************************************************************/
CGrid CSpatialPartition::GetGrid(const int xIndex, const int yIndex) const
{
	return theGrid[xIndex*zNumOfGrid + yIndex];
}

/********************************************************************************
 Get vector of objects from this Spatial Partition
 ********************************************************************************/
vector<SceneNode*> CSpatialPartition::GetObjects(Vector3 position)
{
	// Get the indices of the object's position
	int xIndex = (((int)position.x - (-xSize >> 1)) / (xSize / xNumOfGrid));
	int zIndex = (((int)position.z - (-zSize >> 1)) / (zSize / zNumOfGrid));

	if (!IsInsideGrid(xIndex, zIndex)) {
		cout << "GetObjects()'s position is out of grid" << endl;
		return vector<SceneNode*>();
	}

	int index = xIndex * zNumOfGrid + zIndex;
	return theGrid[index].m_Child;
}

vector<SceneNode*> CSpatialPartition::GetObjectsWithNeighbour(Vector3 position) {

	vector<SceneNode*> container;

	int xIndex = (((int)position.x - (-xSize >> 1)) / (xSize / xNumOfGrid));
	int zIndex = (((int)position.z - (-zSize >> 1)) / (zSize / zNumOfGrid));

	// i = 1
	// start = -1
	// end = 1

	for (int x = -1; x <= 1; ++x) {
		for (int z = -1; z <= 1; ++z) {
			int index = (xIndex + x) * zNumOfGrid + (zIndex + z);

			if (IsInsideGrid(xIndex + x, zIndex + z)) {
				container.reserve(theGrid[index].m_Child.size());
				container.insert(container.end(), theGrid[index].m_Child.begin(), theGrid[index].m_Child.end());
			}

		}
	}

	return container;
}

vector<SceneNode*> CSpatialPartition::GetObjectsWithNeighbour(int _xIndex, int _zIndex) {

	vector<SceneNode*> container;

	int xIndex = _xIndex;
	int zIndex = _zIndex;

	// i = 1
	// start = -1
	// end = 1

	for (int x = -1; x <= 1; ++x) {
		for (int z = -1; z <= 1; ++z) {
			int index = (xIndex + x) * zNumOfGrid + (zIndex + z);

			if (IsInsideGrid(xIndex + x, zIndex + z)) {
				container.reserve(theGrid[index].m_Child.size());
				container.insert(container.end(), theGrid[index].m_Child.begin(), theGrid[index].m_Child.end());
			}

		}
	}

	return container;
}
/********************************************************************************
 Add a new object model
 ********************************************************************************/
void CSpatialPartition::Add(GameObject* theObject)
{
	// Get the indices of the object's position
	int xIndex = (((int)theObject->getCollider().getPosition().x - (-xSize >> 1)) / (xSize / xNumOfGrid));
	int zIndex = (((int)theObject->getCollider().getPosition().z - (-zSize >> 1)) / (zSize / zNumOfGrid));

	// Add them to each grid
	if (((xIndex >= 0) && (xIndex < xNumOfGrid)) && ((zIndex >= 0) && (zIndex < zNumOfGrid)))
	{
		theGrid[xIndex*zNumOfGrid + zIndex].Add(theObject);
	}
}

// Remove but not delete object from this grid
void CSpatialPartition::Remove(GameObject* theObject)
{
	/*
	// Get the indices of the object's position
	int xIndex = (((int)theObject->GetPosition().x - (-xSize >> 1)) / (xSize / xNumOfGrid));
	int zIndex = (((int)theObject->GetPosition().z - (-zSize >> 1)) / (zSize / zNumOfGrid));

	// Add them to each grid
	if (((xIndex >= 0) && (xIndex<xNumOfGrid)) && ((zIndex >= 0) && (zIndex<zNumOfGrid)))
	{
		theGrid[xIndex*zNumOfGrid + zIndex].Remove(theObject);
	}
	*/
}

/********************************************************************************
 Calculate the squared distance from camera to a grid's centrepoint
 ********************************************************************************/
float CSpatialPartition::CalculateDistance(Vector3* theCameraPosition, const int xIndex, const int zIndex)
{

	float xDistance = (float)(xIndex * (xSize / xNumOfGrid) + (-xSize >> 1));
	float zDistance = (float)(zIndex * (zSize / zNumOfGrid) + (-zSize >> 1));

	Vector3 gridPos(xDistance, 0, zDistance);

	float length = (gridPos - *theCameraPosition).Length();

	return length;
}



/********************************************************************************
PrintSelf
********************************************************************************/
void CSpatialPartition::PrintSelf() const
{
	cout << "******* Start of CSpatialPartition::PrintSelf() **********************************" << endl;
	cout << "xSize\t:\t" << xSize << "\tzSize\t:\t" << zSize << endl;
	cout << "xNumOfGrid\t:\t" << xNumOfGrid << "\tzNumOfGrid\t:\t" << zNumOfGrid << endl;
	if (theGrid)
	{
		cout << "theGrid : OK" << endl;
		cout << "Printing out theGrid below: " << endl;
		for (int i = 0; i < xNumOfGrid; i++)
		{
			for (int j = 0; j < zNumOfGrid; j++)
			{
				theGrid[i*zNumOfGrid + j].PrintSelf();
			}
		}
	}
	else
		cout << "theGrid : NULL" << endl;
	cout << "******* End of CSpatialPartition::PrintSelf() **********************************" << endl;
}

/********************************************************************************
Store a camera pointer into this class
********************************************************************************/
void CSpatialPartition::SetCamera(FPSCamera* _cameraPtr)
{
	theCamera = _cameraPtr;
}

/********************************************************************************
Remove the camera pointer from this class
********************************************************************************/
void CSpatialPartition::RemoveCamera(void)
{
	theCamera = nullptr;
}

/********************************************************************************
Set LOD distances
********************************************************************************/

void CSpatialPartition::SetLevelOfDetails(const float distance_High2Mid, const float distance_Mid2Low)
{
	LevelOfDetails_Distances[0] = distance_High2Mid;
	LevelOfDetails_Distances[1] = distance_Mid2Low;
}

bool CSpatialPartition::IsVisible(Vector3 theCameraPosition, Vector3 theCameraDirection, const int xIndex, const int zIndex)
{
	float xDistance = (xGridSize*xIndex + (xGridSize >> 1) - (xSize >> 1)) - theCameraPosition.x;
	float zDistance = (zGridSize*zIndex + (zGridSize >> 1) - (zSize >> 1)) - theCameraPosition.z;
	// If the camera is within the CGrid, then display by default
	// Otherwise, the entity may not get displayed.
	if (xDistance*xDistance + zDistance*zDistance < (xGridSize*xGridSize + zGridSize*zGridSize))
		return true;
	Vector3 gridCentre(xDistance, 0, zDistance);
	if (theCameraDirection.Dot(gridCentre) < 0)
	{
		return false;
	}
	return true;
}

bool CSpatialPartition::IsInsideGrid(int xIndex, int zIndex) {
	return (xIndex >= 0 && xIndex < xNumOfGrid) && (zIndex >= 0 && zIndex < zNumOfGrid);
}
