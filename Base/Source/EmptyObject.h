#pragma once

#include "GameObject.h"

class EmptyObject : public GameObject {

public:
	EmptyObject(Scene* scene);
	~EmptyObject();

	virtual bool Update();
	
};