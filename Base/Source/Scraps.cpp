#include <iterator>
#include "Scene.h"
#include "Scraps.h"
#include "SceneManager.h"
#include "SceneGraph.h"
#include "SceneNode.h"

using std::multimap;

// Player Bullet
Scraps::Scraps(Scene* scene, Vector3 pos) : GameObject(scene, Scene::GEO_HEALTHPACK) {

	active = true;
	this->pos = pos;
	this->pos.y += 5;
	b_EnablePhysics = true;
	b_EnableGravity = false;

	scale.Set(3, 3, 3);
	collider.setBoundingBoxSize(Vector3(5, 5, 5));
	collider.setOffset(Vector3(0, 0, 0));
	collider.type = Collider::C_AABB;
}

Scraps::~Scraps() {

}

bool Scraps::Update() {

	rotation.y += 90 * m_scene->_dt;

	if (collider.AABBvsAABB(m_scene->playerInfo->m_Collider)) {
		m_scene->playerInfo->IncreaseScraps(25);
		m_scene->sceneGraph.FindAndDestroy(this);
		return false;
	}

	return true;
}

