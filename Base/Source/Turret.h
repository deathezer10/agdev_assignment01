#pragma once

#include "EmptyObject.h"
#include "Scene.h"
#include "SpatialPartition.h"
#include "Bullet.h"
#include "RobotBase.h"

class Turret : public EmptyObject {

public:
	enum TURRET_PART
	{
		TURRET_HEAD,
		TURRET_BODY,
	};

	TURRET_PART m_TurretPart;

	Turret(Scene* scene, Vector3 pos, TURRET_PART _turretPart) : EmptyObject(scene) {
		active = true;
		m_TurretPart = _turretPart;
		Translate(pos.x, pos.y, pos.z);
		b_EnablePhysics = true;
		m_scene->radar.addUnit(this);

		switch (_turretPart) {
		case TURRET_HEAD:
			type = Scene::GEO_HIGH_LOD_TURRET_HEAD;
			collider.innerBoxSize = Vector3(7, 2, 7);
			break;
		case TURRET_BODY:
			type = Scene::GEO_HIGH_LOD_TURRET_BODY;
			collider.innerBoxSize = Vector3(5, 5, 5);
			collider.outerBoxSize = Vector3(10, 10, 10);
			break;
		}
		collider.type = Collider::C_AABB;
	}

	~Turret() { m_scene->radar.removeUnit(this); };

	virtual bool Update() {

		// Disable shooting until setted up
		if (!m_IsSetUp) {

			if (m_TurretPart == TURRET_BODY) {
				m_SetUpDuration -= m_scene->_dt;
				Translate(0, m_scene->_dt, 0);

				if (m_SetUpDuration <= 0) {
					m_IsSetUp = true;

					static_cast<Turret*>(m_Child[0]->GetOBJ())->m_IsSetUp = true;
				}
			}

			return true;
		}

		if (m_TurretPart == TURRET_HEAD) {

			auto container = CSpatialPartition::GetInstance()->GetObjectsWithNeighbour(collider.getPosition());

			Vector3 nearestPos;
			float nearestLength;

			for (auto &enemy : container) {

				RobotBase* robot = dynamic_cast<RobotBase*>(enemy->GetOBJ());

				if (robot) {

					Vector3 enemyPos = robot->getCollider().getPosition();

					if (nearestPos.IsZero()) {
						nearestPos = enemyPos;
						nearestLength = (nearestPos - collider.getPosition()).HorizontalLengthSquared();
					}
					else if ((enemyPos - nearestPos).HorizontalLengthSquared() < nearestLength) {
						nearestPos = enemyPos;
						nearestLength = (nearestPos - collider.getPosition()).HorizontalLengthSquared();
					}

				}

			}

			const float m_RangeThreshold = 100;

			// Fire bullet if found enemy
			if (nearestPos.IsZero() == false && (collider.getPosition() - nearestPos).Length() < m_RangeThreshold) {

				Vector3 dir = (collider.getPosition() - nearestPos).Normalized();
				rotation.y = Math::RadianToDegree(atan2(dir.x, dir.z)) + 90;

				// Apply cooldown and fire bullet
				if (m_scene->m_elapsedTime >= m_NextShootTime) {

					Vector3 spawnPos = collider.getPosition();
					spawnPos += (nearestPos - spawnPos).Normalized() * 7; // spawn 7 units further
					spawnPos.y += 1;

					Bullet* bullet = new Bullet(m_scene, spawnPos, nearestPos - spawnPos, false);
					bullet->RetrieveBulletDamage(Bullet::ROCKET_BULLET);

					m_scene->goManager.CreateObject(bullet);
					m_NextShootTime = m_scene->m_elapsedTime + 0.5f;
				}
			}
			else { // No enemies
				rotation.y += 45 * m_scene->_dt;
			}

		}
		else {
		}

		return true;
	}

	float m_NextShootTime = 0;
	int m_Health;

	float m_SetUpDuration = 3;
	bool m_IsSetUp = false;

private:

};
