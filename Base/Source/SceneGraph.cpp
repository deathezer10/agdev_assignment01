#include "Scene.h"
#include "SceneGraph.h"
#include "SceneNode.h"


bool SceneGraph::FindAndDestroy(SceneNode* node) {

	SceneNode* deletee = root->FindNode(node);

	if (deletee) {
		deletee->Destroy();
		return true;
	}

	return false;
}

void SceneGraph::Exit() {
	root->DestroyAllChild();
	delete root;
	root = nullptr;
}

void SceneGraph::Render() {
	root->Render();
}
