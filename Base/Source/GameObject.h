#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "Vector3.h"
#include "Collider.h"
#include "LevelOfDetails.h"
#include "SceneNode.h"

class Scene;
class SceneNode;
class CGrid;

class GameObject : public CLevelOfDetails, public SceneNode {

public:
	bool active = false;
	unsigned type; // Geometry mesh index

	Vector3 pos;
	Vector3 scale;
	Vector3 rotation;

	Vector3 vel;
	float mass = 0.01f;

	Vector3 dir; // direction/orientation
	float momentOfInertia = 0;
	float angularVelocity = 0; // in radians

	Vector3 m_force;
	Vector3 m_torque;

	float MAX_VELOCITY = 0;
	float MAX_ANGULAR_VELOCITY = 0;	

	GameObject(Scene* scene, unsigned meshType, float boxWidth = 1, float boxHeight = 1, float boxDepth = 1);
	virtual ~GameObject();

	// Process the Object's logic, return false if Object is flagged for deletion
	virtual bool Update();
	virtual void UpdatePhysics(); // Calculate velocity, angular velocity, direction
	virtual void Render();
	virtual void OnCollisionHit(GameObject* other) {};

	// Called when this game object is about to get removed from collection
	virtual void OnDestroy();

	Scene* m_scene;

	Collider& getCollider() { return collider; }

	// Allow Object to have physics
	bool b_EnablePhysics = false;
	bool b_EnableGravity = true;
	bool b_EnableLight = true;
	bool b_RenderMe = true;
	bool b_Collidable;

	int m_Health = 0;
	
	CGrid * m_grid = nullptr;

protected:
	Collider collider;	
	static int index;
};

#endif