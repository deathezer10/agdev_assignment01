#include <GLFW\glfw3.h>
#include "Application.h"
#include "SceneManager.h"

#include  <iomanip>

SceneManager *SceneManager::_instance = nullptr;


SceneManager* SceneManager::getInstance() {
	if (!_instance) {
		_instance = new SceneManager();
	}
	return _instance;
}

SceneManager::~SceneManager() {
	if (!_instance)
		Exit();
}

void SceneManager::Exit() {
	delete _instance; // Delete the singleton instance on exit
}

void SceneManager::Update() {

	m_timer.startTimer(); // Start timer to calculate how long it takes to render this frame

	GLFWwindow* window = glfwGetCurrentContext();

	while (!glfwWindowShouldClose(window))
	{
		// If there is any pending Scene, delete the current one and switch Scene
		if (_hasPendingScene) {
			StopWatch time = StopWatch();
			time.startTimer();
			std::cout << "Switching State.." << std::endl;
			currentScene->Exit();
			delete currentScene; // free memory
			currentScene = pendingScene;
			currentScene->Init();
			_hasPendingScene = false;
			pendingScene = nullptr;
			double ggoxz = m_timer.getElapsedTime();
			std::cout << "Loaded State - Time taken: " << std::setprecision(2) << time.getElapsedTime() << "s" << std::endl;
		}

		glfwPollEvents();
		Application::GetInstance().UpdateInput();

		currentScene->Update(m_timer.getElapsedTime());
		currentScene->Render();
		//Swap buffers
		glfwSwapBuffers(window);
		m_timer.waitUntil(Application::frameTime);       // Frame rate limiter. Limits each frame to a specified time in ms.   
		Application::GetInstance().PostInputUpdate();
	}

	currentScene->Exit();
	delete currentScene;

}

void SceneManager::changeScene(Scene* scene) {
	if (!currentScene) {
		currentScene = scene;
		scene->Init();
	}
	else {
		pendingScene = scene;
		_hasPendingScene = true;
	}
}