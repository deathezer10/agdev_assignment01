#include "PlayerInfo.h"
#include <iostream>

#include "MouseController.h"
#include "KeyboardController.h"
#include "Mtx44.h"
#include "Pistol.h"
#include "Rifle.h"
#include "SceneManager.h"
#include "SceneText.h"
#include "Keyboard.h"
#include "Tank.h"
#include "Collider.h"
#include "Turret.h"
#include "Cannon.h"
#include "LuaInterface.h"

// Allocating and initializing CPlayerInfo's static data member.  
// The pointer is allocated but not the object's constructor.
CPlayerInfo *CPlayerInfo::s_instance = 0;

CPlayerInfo::CPlayerInfo(void)
	: m_dAcceleration(10.0)
	, m_dElapsedTime(0.0)
	, attachedCamera(NULL)
	, primaryWeapon(NULL)
	, theCurrentPosture(STAND)
	, m_Collider(&position, 1, 1, 1)
	, typeOfCamera(PLAYER_CAMERA)
{
}

CPlayerInfo::~CPlayerInfo(void) {

	if (m_rifle)
		delete m_rifle;

	if (m_pistol)
		delete m_pistol;

	if (m_cannon)
		delete m_cannon;

}

// Initialise this class instance
void CPlayerInfo::Init(void)
{
	m_iCameraSwayAngle = 0.f;
	m_iCameraSwayDeltaAngle = 30;
	// The limits for left and right sway
	m_iCameraSwayAngle_LeftLimit = -5;
	m_iCameraSwayAngle_RightLimit = 5;
	m_bCameraSwayDirection = false; // false = left, true = right

	// Set the current values
	//position.Set(0, 0, 50);

	target.Set(position.x, position.y, position.z + 5);
	up.Set(0, 1, 0);

   // Set the default values
	defaultPosition.Set(position.x, position.y, position.z);
	defaultTarget.Set(target.x, target.y, target.z);
	defaultUp.Set(0, 1, 0);


	positionBeforeAirStrike.Set(0, 0, 0);

	// Set Boundary
	maxBoundary.Set(1, 1, 1);
	minBoundary.Set(-1, -1, -1);

	// Collider Size
	m_Collider.setBoundingBoxSize(Vector3(15, 20, 15));
	m_Collider.setOffset(Vector3(0, 5, 0));
	m_Collider.type = Collider::C_AABB;

	// Set the pistol as the primary weapon
	m_rifle = new CRifle();
	m_rifle->Init();

	// Set the pistol as the primary weapon
	m_pistol = new CPistol();
	m_pistol->Init();

	m_cannon = new CCannon();
	m_cannon->Init();

	theKeyBoard = new CKeyboard();
	theKeyBoard->Create(this);

	m_Scraps = 0;
}

// Set position
void CPlayerInfo::SetPos(const Vector3& pos)
{
	position = pos;
}

// Set target
void CPlayerInfo::SetTarget(const Vector3& target)
{
	this->target = target;
}

// Set position
void CPlayerInfo::SetUp(const Vector3& up)
{
	this->up = up;
}

// Reset this player instance to default
void CPlayerInfo::Reset(void)
{
	// Set the current values to default values
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
	attachedCamera->currentYaw = 0;
	attachedCamera->currentPitch = 0;
	typeOfCamera = CAMERA_MODE::PLAYER_CAMERA;
}

// Get position x of the player
Vector3 CPlayerInfo::GetPos(void) const {
	return position;
}

// Get target
Vector3 CPlayerInfo::GetTarget(void) const {
	return target;
}
// Get Up
Vector3 CPlayerInfo::GetUp(void) const {
	return up;
}


/********************************************************************************
 Hero Update
 ********************************************************************************/
void CPlayerInfo::Update(double dt)
{
	//Hardware Abstraction
	theKeyBoard->Read((float)dt);

	MouseController::GetInstance()->GetMouseDelta(mouse_diff_x, mouse_diff_y);
	
	// if Mouse Buttons were activated, then act on them
	if (MouseController::GetInstance()->IsButtonDown(theKeyBoard->keyMap.at("Shoot"))) {
		if (primaryWeapon)
			primaryWeapon->Discharge(position, target, this);
	}

	//Only allow sniper to scope now 
	if (MouseController::GetInstance()->IsButtonDown(theKeyBoard->keyMap.at("Scope")) && currentWeapon == 0)
	{
		((SceneText*)SceneManager::getInstance()->getCurrentScene())->m_ScopedFOV = -40;

		if (primaryWeapon) {
			primaryWeapon->isScope = true;
		}

	}
	else {
		((SceneText*)SceneManager::getInstance()->getCurrentScene())->m_ScopedFOV = 0;

		if (primaryWeapon) {
			primaryWeapon->isScope = false;
		}
	}

	// Player camera skills
	if (typeOfCamera == CAMERA_MODE::PLAYER_CAMERA) {

		// Spawn turret
		if (GetScraps() >= 25 && KeyboardController::GetInstance()->IsKeyReleased('3')) {

			ReduceScraps(25); // reduce scrap counter

			Scene* _scene = SceneManager::getInstance()->getCurrentScene();

			Vector3 spawnPos = m_Collider.getPosition();
			Vector3 spawnDir = (target - position).Normalized() * 20;
			spawnDir.y = 0;
			spawnPos += spawnDir;
			spawnPos.y -= 17;

			Turret* turretBody = new Turret(_scene, spawnPos, Turret::TURRET_BODY);
			_scene->goManager.CreateObject(turretBody);
			turretBody->SetName("Turret Body");

			Turret* turretHead = new Turret(_scene, Vector3(0, 2, 0), Turret::TURRET_HEAD);
			turretHead->rotation.y = attachedCamera->currentYaw - 90;
			_scene->goManager.CreateObject(turretHead, false);
			turretHead->SetName("Turret Head");
			turretBody->AddChild(turretHead);

		}

		// Air strike
		if (GetScraps() >= 100 && KeyboardController::GetInstance()->IsKeyReleased('E')) {

			//Store position befoer airstrike
			positionBeforeAirStrike.Set(position.x, position.y, position.z);

			ReduceScraps(100);

			typeOfCamera = AIRSTRIKE_CAMERA;
			m_AirStrikeStartFalling = false;

			// Back up values
			BackupCamera();

			// Set up camera
			position.Set(0, 500, 0);
			target.Set(0, 0, 0);
			up.Set(1, 0, 0);

		}

		// Map analyser
		if (KeyboardController::GetInstance()->IsKeyReleased('T')) {
			typeOfCamera = MAPANALYSER_CAMERA;

			// Back up values
			BackupCamera();

			// Set up camera

			position.y = 1500;
			target.y = 500;
			up.Set(1, 0, 0);
		}

	}

	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	if (b_isDamaged == true && b_isResetDamageCoolDown == false) {
		ReduceHealth(10);
		b_isResetDamageCoolDown = true;
	}
	if (b_isResetDamageCoolDown == true) {
		m_NextDamageTime += (float)dt;

		if (m_NextDamageTime >= m_Damage_Cooldown) {
			m_NextDamageTime = 0.f;
			b_isResetDamageCoolDown = false;
			b_isDamaged = false;
		}
	}

	//Update info about which robot body part is shot
	UpdateDamageInfo();

	double camera_yaw = mouse_diff_x * 0.0174555555555556;		// 3.142 / 180.0
	double camera_pitch = mouse_diff_y * 0.0174555555555556;	// 3.142 / 180.0

	//Update the camera direction based on mouse move
	{
		Vector3 viewUV = (target - position).Normalized();
		Vector3 rightUV;

		if (mouse_diff_x != 0) {
			float yaw = (float)(-m_dSpeed * camera_yaw * (float)dt * m_MouseSensitivity);

			attachedCamera->currentYaw += yaw;

			Mtx44 rotation;
			rotation.SetToRotation(yaw, 0, 1, 0);
			viewUV = rotation * viewUV;
			target = position + viewUV;
			rightUV = viewUV.Cross(up);
			rightUV.y = 0;
			rightUV.Normalize();
			up = rightUV.Cross(viewUV).Normalized();
		}
		if (mouse_diff_y != 0) {

			float pitch = (float)(-m_dSpeed * camera_pitch * (float)dt * m_MouseSensitivity);

			if (fabs(attachedCamera->currentPitch + pitch) < 60) { // limit pitch

				attachedCamera->currentPitch += pitch;

				rightUV = viewUV.Cross(up);
				rightUV.y = 0;
				rightUV.Normalize();
				up = rightUV.Cross(viewUV).Normalized();

				Mtx44 rotation;
				rotation.SetToRotation(pitch, rightUV.x, rightUV.y, rightUV.z);
				viewUV = rotation * viewUV;
				target = position + viewUV;

			}

		}
	}


	// If the user presses SPACEBAR, then make him jump
	if (KeyboardController::GetInstance()->IsKeyDown(theKeyBoard->keyMap.at("Jump")) &&
		position.y == m_MinTerrainHeight &&
		(theCurrentPosture == STAND))
	{
	}

	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE::SCROLL_TYPE_YOFFSET) > 0) {
		currentWeapon++;
	}
	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE::SCROLL_TYPE_YOFFSET) < 0) {
		currentWeapon--;
	}

	if (KeyboardController::GetInstance()->IsKeyDown(theKeyBoard->keyMap.at("SecondaryWeapon"))) {
		currentWeapon = 0;
	}
	else if (KeyboardController::GetInstance()->IsKeyDown(theKeyBoard->keyMap.at("PrimaryWeapon"))) {
		currentWeapon = 1;
	}

	currentWeapon = Math::Clamp<int>(currentWeapon, 0, 1);

	//current weapon
	switch (currentWeapon) {

	case 0:
		primaryWeapon = m_pistol;
		break;

	case 1:
		primaryWeapon = m_rifle;
		break;
	}

	if (KeyboardController::GetInstance()->IsKeyReleased(theKeyBoard->keyMap.at("Reload"))) {
		if (primaryWeapon) {
			primaryWeapon->Reload();
		}
	}
	if (KeyboardController::GetInstance()->IsKeyDown(theKeyBoard->keyMap.at("Refill"))) {
		primaryWeapon->ReloadClip((float)dt);
	}
	else {
		primaryWeapon->StopReloadClip();
	}

	if (primaryWeapon)
		primaryWeapon->Update(dt);

	if (KeyboardController::GetInstance()->IsKeyReleased(theKeyBoard->keyMap.at("Sprint"))) {
		up.Set(0, 1, 0);
	}

	// If a camera is attached to this playerInfo class, then update it
	if (attachedCamera) {
		attachedCamera->SetCameraPos(position);
		attachedCamera->SetCameraTarget(target);
		attachedCamera->SetCameraUp(up);
	}

	// Apply gravity
	if (position.y > m_MinTerrainHeight) {
		position.y -= 9.8f * (float)dt;
		target.y -= 9.8f * (float)dt;
	}

}

// Constrain the position within the borders
void CPlayerInfo::Constrain(void)
{
	// if the player is not jumping nor falling, then adjust his y position
	if (position.y == m_MinTerrainHeight) {

		Vector3 viewDirection = target - position;

		switch (theCurrentPosture) {
		case STAND:
			position.y = m_MinTerrainHeight;
			target = position + viewDirection;
			break;
		case CROUCH:
			position.y = m_MinTerrainHeight;
			target = position + viewDirection;
			up.Set(0, 1, 0);
			break;
		case PRONE:
			position.y = m_MinTerrainHeight;
			target = position + viewDirection;
			up.Set(0, 1, 0);
			break;
		default:
			break;
		}

	}
}

void CPlayerInfo::AttachCamera(FPSCamera* _cameraPtr)
{
	attachedCamera = _cameraPtr;
}

void CPlayerInfo::DetachCamera()
{
	attachedCamera = nullptr;
}

//Detect and process front / back movement on the controller
bool CPlayerInfo::Move_FrontBack(const float deltaTime, const bool direction, const float speedMultiplier)
{
	Vector3 viewVector = target - position;
	Vector3 horizontalView = viewVector;
	horizontalView.y = 0;

	Vector3 rightUV;
	if (direction)
	{

		if ((theCurrentPosture == STAND) && (KeyboardController::GetInstance()->IsKeyDown(theKeyBoard->keyMap.at("Sprint"))))
			position += horizontalView.Normalized() * (float)m_dSpeed * 2.0f * (float)deltaTime;
		else if (theCurrentPosture == CROUCH)
			position += horizontalView.Normalized() * (float)m_dSpeed * 0.75f * (float)deltaTime;
		else if (theCurrentPosture == PRONE)
			position += horizontalView.Normalized() * (float)m_dSpeed * 0.25f * (float)deltaTime;
		else
			position += horizontalView.Normalized() * (float)m_dSpeed * speedMultiplier * (float)deltaTime;

		if (theCurrentPosture == CURRENT_POSTURE::STAND && KeyboardController::GetInstance()->IsKeyDown(theKeyBoard->keyMap.at("Sprint"))) {

			if (m_bCameraSwayDirection) {

				m_iCameraSwayAngle += m_iCameraSwayDeltaAngle * deltaTime;

				Mtx44 Rotation;
				Rotation.SetToRotation(m_iCameraSwayDeltaAngle  * deltaTime, 0, 0, 1);
				up = Rotation * up;
				//attachedCamera->SetCameraUp(up);

				if (m_iCameraSwayAngle > m_iCameraSwayAngle_RightLimit) {
					m_bCameraSwayDirection = false;
				}
			}
			else {
				m_iCameraSwayAngle -= m_iCameraSwayDeltaAngle  * deltaTime;

				Mtx44 Rotation;
				Rotation.SetToRotation(-m_iCameraSwayDeltaAngle  * deltaTime, 0, 0, 1);
				up = Rotation * up;
				//attachedCamera->SetCameraUp(up);

				if (m_iCameraSwayAngle < m_iCameraSwayAngle_LeftLimit) {
					m_bCameraSwayDirection = true;
				}
			}

			m_iCameraSwayAngle = Math::Clamp<float>(m_iCameraSwayAngle, m_iCameraSwayAngle_LeftLimit, m_iCameraSwayAngle_RightLimit);
		}

		Constrain();
		//Update the target
		target = position + viewVector;
		return true;
	}
	else { // Backwards
		position -= horizontalView.Normalized() * (float)m_dSpeed * speedMultiplier * (float)deltaTime;

		Constrain();
		//Update the target
		target = position + viewVector;
		return true;

	}
	return false;
}

bool CPlayerInfo::Move_LeftRight(const float deltaTime, const bool direction, const float speedMultiplier)
{
	Vector3 viewVector = target - position;
	Vector3 rightUV;

	float m_sideSpeed = 0;

	if (theCurrentPosture == CROUCH)
		m_sideSpeed = 0.75f;
	else if (theCurrentPosture == PRONE)
		m_sideSpeed = 0.25f;
	else
		m_sideSpeed = 1;


	if (direction)
	{
		rightUV = (viewVector.Normalized()).Cross(up);
		rightUV.y = 0;
		rightUV.Normalize();
		position -= rightUV * (float)m_dSpeed * m_sideSpeed * (float)deltaTime;

		Constrain();

		//Update the target
		target = position + viewVector;
		return true;
	}

	else
	{
		rightUV = (viewVector.Normalized()).Cross(up);
		rightUV.y = 0;
		rightUV.Normalize();
		position += rightUV * (float)m_dSpeed * m_sideSpeed * (float)deltaTime;

		Constrain();
		//Update the target
		target = position + viewVector;
		return true;

	}
	return false;
}

void CPlayerInfo::ReduceHealth(int value) {

	m_Health -= value;

	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	/*
	_scene->textManager.addTimedMeshToScreen(UIManager::MeshQueue{ _scene->meshList[Scene::GEO_DAMAGED],
		Vector3(Application::windowWidth() / 10 * 0.5f, Application::windowHeight() / 10 * 0.5f, 0), // Center of screen
		Vector3(90,0,0), // by default, the quad is lying down flat, so we need to rotate by 90 degrees
		Vector3(50,1,30) // scaling
	}, 2);
	*/

	m_Health = Math::Clamp(m_Health, 0, 100);
}

void CPlayerInfo::IncreaseHealth(int value) {
	m_Health += value;
	m_Health = Math::Clamp(m_Health, 0, 100);

	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	_scene->textManager.addTimedMeshToScreen(UIManager::MeshQueue{ _scene->meshList[Scene::GEO_HEALED],
		Vector3(Application::windowWidth() / 10 * 0.5f, Application::windowHeight() / 10 * 0.5f, 0), // Center of screen
		Vector3(90,0,0), // by default, the quad is lying down flat, so we need to rotate by 90 degrees
		Vector3(30,1,30) // scaling
	}, 1.0f);

}

void CPlayerInfo::ReduceScraps(int value) {
	m_Scraps -= value;
	m_Scraps = Math::Clamp(m_Scraps, 0, 100);
}

void CPlayerInfo::IncreaseScraps(int value) {
	m_Scraps += value;
	m_Scraps = Math::Clamp(m_Scraps, 0, 100);

	m_Score += value;
}

void CPlayerInfo::SetTypeOfCamera(CAMERA_MODE _typeOfCamera)
{
	typeOfCamera = _typeOfCamera;
}

CPlayerInfo::CAMERA_MODE CPlayerInfo::GetTypeOfCamera()
{
	return CAMERA_MODE(typeOfCamera);
}


void CPlayerInfo::InitTankCamera(Tank * cannon, Tank * body)
{
	m_tank[0] = body;
	m_tank[1] = cannon;
	//SceneNode* result = sceneGraph.GetRoot()->FindNode("tankbody");

	position = Vector3(m_tank[1]->getCollider().getPosition().x, m_tank[1]->getCollider().getPosition().y + 10.f, m_tank[1]->getCollider().getPosition().z + 15.f);
	target = Vector3(position.x, position.y, position.z - 5.f);
	if (attachedCamera) {
		attachedCamera->SetCameraPos(position);
		attachedCamera->SetCameraTarget(target);
		attachedCamera->SetCameraUp(up);
	}

	//Set the primary weapon as cannon
	primaryWeapon = m_cannon;
}



bool CPlayerInfo::Move_TankUpDown(const float deltaTime, const bool direction, const float speedMultiplier)
{
	Vector3 viewVector = target - position;
	Vector3 horizontalView = viewVector;
	horizontalView.y = 0;

	Vector3 rightUV;
	if (direction)
	{
		position += horizontalView.Normalized() * (float)m_dSpeed * speedMultiplier *(float)deltaTime;

		Constrain();
		//Update the target
		target = position + viewVector;

		Vector3 difference = horizontalView.Normalized() * (float)m_dSpeed *  speedMultiplier *(float)deltaTime;
		m_tank[0]->Translate(0, 0, difference.z);
		return true;
	}
	else { // Backwards
		position -= horizontalView.Normalized() * (float)m_dSpeed * speedMultiplier * (float)deltaTime;

		Constrain();
		//Update the target
		target = position + viewVector;

		Vector3 difference = horizontalView.Normalized() * (float)m_dSpeed * speedMultiplier * (float)deltaTime;
		m_tank[0]->Translate(0, 0, -difference.z);
		return true;

	}
	return false;
}

bool CPlayerInfo::Rotate_Tank(const float deltaTime, const bool direction, const float speedMultiplier, const float  angleRotation)
{
	//float rotateAngle = (float)(angleRotation * speedMultiplier * (float)deltaTime);
	CLuaInterface::GetInstance()->SetLuaState("Math");
	float rotateAngle = CLuaInterface::GetInstance()->GetTankRotateAngle("CaculateTankAngleRotation" , angleRotation, speedMultiplier, deltaTime);


	if (direction)
	{
		camBodyTheta += Math::DegreeToRadian(rotateAngle);
		m_tank[0]->Rotate(rotateAngle, 0, 1, 0);
	}
	else
	{
		camBodyTheta -= Math::DegreeToRadian(rotateAngle);
		m_tank[0]->Rotate(-rotateAngle, 0, 1, 0);

	}
	return false;
}


void CPlayerInfo::UpdateTankCamera(double dt)
{

	if (KeyboardController::GetInstance()->IsKeyDown('W'))
		Move_TankUpDown((float)dt, true, m_tank[0]->TANKSPEED);
	if (KeyboardController::GetInstance()->IsKeyDown('S'))
		Move_TankUpDown((float)dt, false, m_tank[0]->TANKSPEED);
	if (KeyboardController::GetInstance()->IsKeyDown('A'))
		Rotate_Tank((float)dt, true, m_tank[0]->TANKSPEED, 60.f);
	if (KeyboardController::GetInstance()->IsKeyDown('D'))
		Rotate_Tank((float)dt, false, m_tank[0]->TANKSPEED, 60.f);


	MouseController::GetInstance()->GetMouseDelta(mouse_diff_x, mouse_diff_y);
	double camera_yaw = mouse_diff_x * 0.0174555555555556;		// 3.142 / 180.0
	double camera_pitch = mouse_diff_y * 0.0174555555555556;	// 3.142 / 180.0

	{
		Vector3 viewUV = (target - position).Normalized();
		Vector3 rightUV;

		if (mouse_diff_x != 0) {
			float yaw = (float)(-m_dSpeed * camera_yaw * (float)dt * m_MouseSensitivity);
			Mtx44 rotation;
			rotation.SetToRotation(yaw, 0, 1, 0);
			viewUV = rotation * viewUV;
			target = position + viewUV;
			rightUV = viewUV.Cross(up);
			rightUV.y = 0;
			rightUV.Normalize();
			up = rightUV.Cross(viewUV).Normalized();

			// Tank cannon rotation
			m_tank[1]->rotation.y += yaw;
			camTheta = Math::DegreeToRadian(m_tank[1]->rotation.y);
		}

	}

	float radius = 30;
	float yOffset = 10;

	Vector3 camPos;

	CLuaInterface::GetInstance()->SetLuaState("Math");
	CLuaInterface::GetInstance()->GetTankCameraPosition("CalculateTankCameraPos",
		m_tank[0]->getCollider().getPosition().x, radius, cos(camPhi) , sin(camTheta + camBodyTheta) , camPos.x );
	CLuaInterface::GetInstance()->GetTankCameraPosition("CalculateTankCameraPos",
		m_tank[0]->getCollider().getPosition().y, radius, sin(camPhi), sin(camTheta + camBodyTheta), camPos.y);
	CLuaInterface::GetInstance()->GetTankCameraPosition("CalculateTankCameraPos",
		m_tank[0]->getCollider().getPosition().z, radius, 1.f , cos(camTheta + camBodyTheta), camPos.z);

	//camPos.x = m_tank[0]->getCollider().getPosition().x + radius * cos(camPhi) * sin(camTheta + camBodyTheta);
	//camPos.y = m_tank[0]->getCollider().getPosition().y + radius * sin(camPhi) * sin(camTheta + camBodyTheta);
	//camPos.z = m_tank[0]->getCollider().getPosition().z + radius * cos(camTheta + camBodyTheta);
	//std::cout << camPos << std::endl;
	camPos.y += yOffset;

	Vector3 camTarget = m_tank[0]->getCollider().getPosition();
	camTarget.y += yOffset;

	if (attachedCamera) {
		attachedCamera->SetCameraPos(camPos);
		attachedCamera->SetCameraTarget(camTarget);
		attachedCamera->SetCameraUp(up);
	}


	if (MouseController::GetInstance()->IsButtonReleased(MouseController::LMB))
	{
		if (primaryWeapon) {
			static_cast<CCannon*>(m_cannon)->Discharge(camPos, camTarget, this);
		}
	}

	//Update cannon firing
	if (primaryWeapon)
		primaryWeapon->Update(dt);


}

void CPlayerInfo::UpdateAirStrikeCamera(double dt) {

	CSpatialPartition* spatial = CSpatialPartition::GetInstance();
	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	if (!m_AirStrikeStartFalling) {
		if (KeyboardController::GetInstance()->IsKeyPressed('W')) {
			xIndex++;
		}
		else if (KeyboardController::GetInstance()->IsKeyPressed('S')) {
			xIndex--;
		}
		else if (KeyboardController::GetInstance()->IsKeyPressed('A')) {
			zIndex--;
		}
		else if (KeyboardController::GetInstance()->IsKeyPressed('D')) {
			zIndex++;
		}

		xIndex = Math::Clamp(xIndex, (-spatial->GetxNumOfGrid() / 2) + 1, spatial->GetxNumOfGrid() / 2);
		zIndex = Math::Clamp(zIndex, (-spatial->GetzNumOfGrid() / 2) + 1, spatial->GetzNumOfGrid() / 2);

		position.Set((float)(xIndex * (float)spatial->GetxGridSize()), 500.f, (float)(zIndex * spatial->GetzGridSize()));
		position.x -= spatial->GetxGridSize() / 2;
		position.z -= spatial->GetzGridSize() / 2;
		target.Set(position.x, 0, position.z);

		// Left click destroy all objects
		if (MouseController::GetInstance()->IsButtonReleased(MouseController::LMB)) {
			m_AirStrikeStartFalling = true; // make camera fall down
			m_FallVelocity = 0;
		}
	}
	else { // Camera starts to drop here

		// Gradually increase falling speed
		m_FallVelocity += 98 * _scene->_dt;
		position.y -= m_FallVelocity * _scene->_dt;

		// Spin the camera
		Mtx44 rotation;
		rotation.SetToRotation((m_FallVelocity / 2) * _scene->_dt, 0, 1, 0);
		up = rotation * up;

		if (position.y <= m_MinTerrainHeight) {
			m_AirStrikeStartFalling = false;

			auto container = spatial->GetObjects(position);

			// Destroy all objects in the partition
			if (container.empty() == false) {
				container[0]->GetParent()->DestroyAllChild();
				m_Score += container.size() * 50;
			}

			// Reset camera
			typeOfCamera = CAMERA_MODE::PLAYER_CAMERA;
			RestoreCamera();
		}

	}

	Vector3 meshPosition = position;
	meshPosition.y = 0;

	_scene->textManager.queueRenderMesh(
		UIManager::MeshQueue{
		_scene->meshList[Scene::GEO_GRID_SELECTED],
		meshPosition,
		Vector3(0,0,0),
		Vector3(100,1,100),
		false,
		false
	}
	);

	attachedCamera->SetCameraPos(position);
	attachedCamera->SetCameraTarget(target);
	attachedCamera->SetCameraUp(up);

}

void CPlayerInfo::UpdateMapAnalyserCamera(double dt) {

	MouseController::GetInstance()->GetMouseDelta(mouse_diff_x, mouse_diff_y);
	double camera_yaw = mouse_diff_x * 0.0174555555555556;		// 3.142 / 180.0

	Vector3 viewUV = (target - position).Normalized();
	Vector3 rightUV;

	if (mouse_diff_x != 0) {
		float yaw = (float)(-m_dSpeed * camera_yaw * (float)dt * m_MouseSensitivity);
		attachedCamera->currentYaw += yaw;

		Mtx44 rotation;
		rotation.SetToRotation(yaw, 0, 1, 0);
		viewUV = rotation * viewUV;
		target = position + viewUV;
		rightUV = viewUV.Cross(up);
		rightUV.y = 0;
		rightUV.Normalize();
		up = rightUV.Cross(viewUV).Normalized();
	}


	if (KeyboardController::GetInstance()->IsKeyReleased('T')) {
		typeOfCamera = PLAYER_CAMERA;
		RestoreCamera();
	}

	attachedCamera->SetCameraPos(position);
	attachedCamera->SetCameraTarget(target);
	attachedCamera->SetCameraUp(up);

}

void CPlayerInfo::TakeDamage(bool _isDamage)
{
	if (b_isDamaged == false) {
		b_isDamaged = _isDamage;
	}
}

void CPlayerInfo::UpdateDamageInfo()
{
	Scene* _scene = SceneManager::getInstance()->getCurrentScene();

	if (b_showDamageInfo == true)
	{
		m_damageInfoTime -= _scene->_dt;

		if (m_damageInfoTime <= 0.f)
		{
			m_damageInfoTime = 5.f;
			b_showDamageInfo = false;
		}
	}
	else // Reset damageInfotime
	{
		m_damageInfoTime = 5.f;
	}
}
