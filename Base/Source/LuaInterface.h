#pragma once

//include the lua headers
#include  <lua.hpp>
#include <string>
#include "Vector3.h"



using std::string;

class CLuaInterface 
{
protected:
	static CLuaInterface *s_instance;
	CLuaInterface();
public:
	static CLuaInterface *GetInstance()
	{
		if (!s_instance)
			s_instance = new CLuaInterface;
		return s_instance;
	}
	static bool DropInstance()
	{
		if (s_instance)
		{
			// Drop the Lua Interface Class
			s_instance->Drop();

			delete s_instance;
			s_instance = NULL;
			return true;
		}
		return false;
	}

	virtual ~CLuaInterface();
	// Initialisation of the Lua Interface Class
	bool Init();
	// Run the Lua Interface Class
	void Run();
	// Drop the Lua Interface Class
	void Drop();
	// Pointer to the Lua State
	lua_State *theLuaState;
	// pointer to Lua error state
	lua_State * theErrorState;

	lua_State * thePlayerLuaState;

	// Player Hotkey Settings
	lua_State * thePlayerControlLuaState;

	//Extract a field from a table
	float GetField(const char * key);

	//Get error message for using an error code
	void error(const char * errorCode);

	//Change the file to read from 
	void SetLuaState(string fileName);

	int getIntValue(string key);
	float getFloatValue(string key);
	string getStringValue(string key );
	Vector3 getVector3Values(string key);

	//bOverWrite = true to overwrite the whole savefile conent and vice versa
	void saveFloatValue(const char* varName, const float value, const string fileName , const bool bOverwrite = false);
	void saveIntValue(const string varName, const int value, const string fileName ,const bool bOverwrite = false );
	
	void LoadEnemy();
	void LoadPlayer();

	string empty = std::string();
	void GetElementFromTable(string input, int *value1 = 0, string *value2 = {}, float * value3 = 0 , bool isFloat =  false);

	float GetDistance(const char* varName, Vector3 source, Vector3 destination);
	void GetTankCameraPosition(const char * varName, float collidePos, float radius, float value1, float value2, float & camPos);
	float GetTankRotateAngle(const char * varName, float angle, float multipler, float dt);

};