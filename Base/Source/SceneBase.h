#pragma once

#include "Scene.h"
#include "Mtx44.h"
#include "Mesh.h"
#include "MatrixStack.h"

class Light;

class SceneBase : public Scene {

public:
	SceneBase() {};
	~SceneBase() {};

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();
	
private:
	Light* lights[1];

};