#pragma once

#include "EmptyObject.h"
#include "SceneText.h"
#include "WayPoint.h"
#include"WayPointManager.h"

#include <vector>
using std::vector;

class Robot;

class RobotBase : public EmptyObject {

public:
	enum ROBOT_STATES
	{
		PATROL,
		CHASE,
	};

	ROBOT_STATES m_currentState;

	static int m_enemyDeadCount;
	static int m_enemyCount;

	//Feed in the parent(body) pos , the rest of the body parts are translated accordingly
	RobotBase(Scene* scene, Vector3 pos);
	~RobotBase();

	void Init();
	virtual bool Update();
	
	//Get next waypoint for this enemy
	CWaypoint * GetNextWayPoint();

	void RenderWayPointLines();

	void FillUpListOfWayPoint(int sizeOfWayPoints);
	void SetCurrentWayPointIndex(int index);
	void SetChaseSpeed(float speed) { m_Chase_Speed = speed; }
	void SetPatrolSpeed(float speed) { m_patrolSpeed = speed; }
	void SetDistanceToSwitchState(float distance) { m_distanceToSwitchState = distance; }

	WayPointManager * theWPManager;


	//Body
	Robot * robotBody;
private:

	//Head
	Robot * robotHead;
	//Left  arm
	Robot * robotLeftArm;
	//Right arm
	Robot * robotRightArm;

	//Vector containing IDs of Waypoints
	vector<int> listOfWayPoints;
	//Current ID of waypoint
	int m_iWayPointIndex;

	//Default position
	Vector3 m_defaultPosition;
	Vector3 target;


	float m_Chase_Speed;
	float m_patrolSpeed;
	float m_distanceToSwitchState;
};


