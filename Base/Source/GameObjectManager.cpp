#include "GameObjectManager.h"
#include "Scene.h"
#include "SpatialPartition.h"
#include <algorithm>

GameObjectManager::~GameObjectManager() {
	Exit();
}

void GameObjectManager::CreateObjectQueue(GameObject * obj) {
	m_goQueue.push(obj);
}

void GameObjectManager::CreateObject(GameObject * obj, bool addToSP) {
	m_goList.push_back(obj);

	// Add to the Spatial Partition
	if (m_spatialPartition && addToSP)
		m_spatialPartition->Add(obj);
}

void GameObjectManager::DestroyObjectQueue(GameObject * obj) {
	m_destroyQueue.push(obj);
}

void GameObjectManager::DestroyObject(GameObject * obj) {

	if (obj->m_grid) {
		obj->m_grid->Remove(obj);
	}

	// Find the GameObject to delete
	for (auto &it = m_goList.begin(); it != m_goList.end();) {
		if (*it == obj) {
			(*it)->OnDestroy(); // Invoke OnDestroy before deleting
			it = m_goList.erase(it);
			ValidateIterator(it); // return the updated iterator
			delete obj; // de-allocate memory
			return;
		}
		else {
			++it;
		}
	}
}

void GameObjectManager::UpdateObjects() {

	DequeueObjects();

	GameObject* obj;
	m_goListIterator = m_goList.begin();

	for (m_goListIterator; m_goListIterator != m_goList.end();) {

		obj = *m_goListIterator;

		if (obj->active) {

			// Update() return false if the Object gets deleted
			if (obj->Update() == true) {

			}

			//// Update physics only when enabled
			//if (obj->b_EnablePhysics) {
			//	// Apply velocity, angular acceleration, momentum, etc
			//	obj->UpdatePhysics();
			//}

		}

		if (!_iteratorUpdated) {
			++m_goListIterator;
		}
		else {
			// skip increment since vector.erase() already returned the value of the next valid iterator
			_iteratorUpdated = false;
		}
	}

	if (m_spatialPartition)
		m_spatialPartition->Update();

}

void GameObjectManager::RenderObjects() {
	// render all objects in map
	for (auto &i : m_goList) {
		if (i->active)
			i->Render();
	}
}

void GameObjectManager::ValidateIterator(std::list<GameObject*>::iterator it) {
	m_goListIterator = it;
	_iteratorUpdated = true;
}

void GameObjectManager::DequeueObjects() {
	while (!m_goQueue.empty()) {
		m_goList.push_back(m_goQueue.front());
		m_goQueue.pop();
	}
	while (!m_destroyQueue.empty()) {
		DestroyObject(m_destroyQueue.front());
		m_destroyQueue.pop();
	}
}

void GameObjectManager::Exit()
{
	if (m_goList.size()) {
		for (auto &i : m_goList)
			delete i;
	}
	m_goList.clear();
}

void GameObjectManager::SetSpatialPartition(CSpatialPartition * theSpatialPartition)
{
	m_spatialPartition = theSpatialPartition;
}

void GameObjectManager::RenderSpatialGrid()
{
	// Render the Spatial Partition
	if (m_spatialPartition)
		m_spatialPartition->Render();
}
