#include "LuaInterface.h"
#include "RobotBase.h"
#include "WayPointManager.h"
#include "SceneManager.h"
#include "Robot.h"


#include <iostream>
using namespace std;

CLuaInterface * CLuaInterface::s_instance = 0;

CLuaInterface::CLuaInterface()
	: theLuaState(NULL)
{
}

CLuaInterface::~CLuaInterface()
{
}

bool CLuaInterface::Init()
{
	bool result = false;

	//1. Create lua state
	theLuaState = lua_open();

	if (theLuaState)
	{
		//2. Load lua auxiliary libraries
		luaL_openlibs(theLuaState);

		//3. Load lua script
		luaL_dofile(theLuaState, "DM2240.lua");

		result = true;
	}

	theErrorState = lua_open();

	if ((theLuaState && (theErrorState)))
	{
		luaL_openlibs(theErrorState);

		//3. Load lua script
		luaL_dofile(theErrorState, "errorLookup.lua");

	}

	thePlayerLuaState = lua_open();

	if (thePlayerLuaState)
	{
		luaL_openlibs(thePlayerLuaState);

		luaL_dofile(thePlayerLuaState, "DM2240_PlayerSettings.lua");
	}

	thePlayerControlLuaState = lua_open();

	if (thePlayerControlLuaState)
	{
		luaL_openlibs(thePlayerControlLuaState);

		luaL_dofile(thePlayerControlLuaState, "DM2240_PlayerControls.lua");
	}


	return result;
}

void CLuaInterface::Run()
{
	if (theLuaState == NULL)
		return;

	//4. Read the variables
	//lua_getglobal(lua_State* , const char*)
	lua_getglobal(theLuaState, "title");
	//extract value, index -1 as variable is already retrieved using lua_getglobal
	const char *title = lua_tostring(theLuaState, -1);

	lua_getglobal(theLuaState, "width");
	int width = lua_tointeger(theLuaState, -1);

	lua_getglobal(theLuaState, "height");
	int height = lua_tointeger(theLuaState, -1);

	//Display on screen
	cout << title << endl;
	cout << "------------------------------" << endl;
	cout << "Width of screen : " << width << endl;
	cout << "Height of screen : " << height << endl;

}

void CLuaInterface::Drop()
{
	if (theLuaState)
	{
		//close the lua state
		lua_close(theLuaState);
	}
}

// Extract a field from a table
float CLuaInterface::GetField(const char *key)
{
	int result = false;

	// Check if the variables in the Lua stack belongs to a table
	if (!lua_istable(theLuaState, -1))
		error("error100");

	lua_pushstring(theLuaState, key);
	lua_gettable(theLuaState, -2);

	if (!lua_isnumber(theLuaState, -1))
		error("error101");

	result = (int)lua_tonumber(theLuaState, -1);
	lua_pop(theLuaState, 1);  /* remove number */
	return result;
}

void CLuaInterface::error(const char * errorCode)
{
	if (theErrorState == nullptr)
		return;

	lua_getglobal(theErrorState, errorCode);
	const char* errorMsg = lua_tostring(theErrorState, -1);
	if (errorMsg != NULL)
		cout << errorMsg << endl;
	else
		cout << errorCode << " is not valid.\n*** PLease contact the developer ***" << endl;

}

void CLuaInterface::SetLuaState(string fileName)
{
	// Fix crash?
	lua_close(theLuaState);
	theLuaState = lua_open();

	if (fileName.empty() == false)
		luaL_dofile(theLuaState, string("DM2240_" + fileName + ".lua").c_str());
	else
		luaL_dofile(theLuaState, "DM2240.lua");
}

int CLuaInterface::getIntValue(string key)
{
	lua_getglobal(theLuaState, key.c_str());
	return lua_tointeger(theLuaState, -1);
}

float CLuaInterface::getFloatValue(string key)
{
	lua_getglobal(theLuaState, key.c_str());
	return (float)lua_tonumber(theLuaState, -1);
}

string CLuaInterface::getStringValue(string key)
{
	lua_getglobal(theLuaState, key.c_str());

	size_t length = 0;
	string ouputValue = lua_tostring(theLuaState, -1, &length);

	return ouputValue;

}

Vector3 CLuaInterface::getVector3Values(string key)
{
	lua_getglobal(theLuaState, key.c_str());
	lua_rawgeti(theLuaState, -1, 1);
	int x = lua_tonumber(theLuaState, -1);
	lua_pop(theLuaState, 1);
	lua_rawgeti(theLuaState, -1, 2);
	int y = lua_tonumber(theLuaState, -1);
	lua_pop(theLuaState, 1);
	lua_rawgeti(theLuaState, -1, 3);
	int z = lua_tonumber(theLuaState, -1);
	lua_pop(theLuaState, 1);

	return Vector3(x, y, z);
}


void CLuaInterface::saveIntValue(const string varName, const int value, const string fileName, const bool bOverwrite)
{
	theLuaState = lua_open();
	luaL_openlibs(theLuaState);
	luaL_dofile(theLuaState, "DM2240.lua");
	lua_getglobal(theLuaState, "SaveToLuaFile");
	string outputString;
	outputString = varName + " = " + to_string(value) + "\n";
	lua_pushstring(theLuaState, outputString.c_str());
	lua_pushinteger(theLuaState, bOverwrite);
	string appendedFileName = "DM2240_" + fileName + ".lua";
	lua_pushstring(theLuaState, appendedFileName.c_str());
	lua_call(theLuaState, 3, 0); cout << "....................";
}

void CLuaInterface::saveFloatValue(const char* varName, const float value, const string fileName, const bool bOverwrite)
{
	theLuaState = lua_open();
	luaL_openlibs(theLuaState);
	luaL_dofile(theLuaState, "DM2240.lua");
	lua_getglobal(theLuaState, "SaveToLuaFile");
	char outputString[80];
	sprintf(outputString, "%s = %6.4f\n", varName, value);
	lua_pushstring(theLuaState, outputString);
	lua_pushinteger(theLuaState, bOverwrite);
	string appendedFileName = "DM2240_" + fileName + ".lua";
	lua_pushstring(theLuaState, appendedFileName.c_str());
	lua_call(theLuaState, 3, 0);
}


void CLuaInterface::LoadEnemy()
{
	int robotCount = getIntValue("enemyCount");

	int theValue;
	string theStringValue;
	float theFloatingValue;

	for (int i = 0; i < robotCount; ++i)
	{
		lua_getglobal(theLuaState, string("enemy" + to_string(i)).c_str());


		lua_pushstring(theLuaState, "enemypos");
		lua_gettable(theLuaState, -2);
		Vector3 initialPos = Vector3(GetField("x"), GetField("y"), GetField("z"));
		lua_pop(theLuaState, 1);

		RobotBase * theRobot = new RobotBase(SceneManager::getInstance()->getCurrentScene(), initialPos);
		GetElementFromTable("health", &theValue);
		theRobot->robotBody->m_Health = theValue;


		GetElementFromTable("waypointCount", &theValue);

		int noOfWayPoint = theValue;

		for (int i = 0; i < noOfWayPoint; ++i)
		{
			//Example - waypoint0
			lua_pushstring(theLuaState, string("waypoint" + to_string(i)).c_str());
			lua_gettable(theLuaState, -2);
			Vector3 coordinate = Vector3(GetField("x"), GetField("y"), GetField("z"));
			lua_pop(theLuaState, 1);
			theRobot->theWPManager->AddWaypoint(coordinate);
		}


		theRobot->FillUpListOfWayPoint(noOfWayPoint);
		//Get current way point state from lua file
		GetElementFromTable("currentWayPointIndex", &theValue);
		theRobot->SetCurrentWayPointIndex(theValue);

		//Set patrol speed
		GetElementFromTable("patrolSpeed", &theValue, &theStringValue, &theFloatingValue, true);
		theRobot->SetPatrolSpeed(theFloatingValue);

		//Set chase speed
		GetElementFromTable("chaseSpeed", &theValue, &theStringValue, &theFloatingValue, true);
		theRobot->SetChaseSpeed(theFloatingValue);

		//Set distance to switch state
		GetElementFromTable("distanceToSwitchState", &theValue, &theStringValue, &theFloatingValue, true);
		theRobot->SetDistanceToSwitchState(theFloatingValue);

		//Init default position
		theRobot->Init();

		//Push back into container
		SceneManager::getInstance()->getCurrentScene()->goManager.CreateObject(theRobot, true);
	}

}


void CLuaInterface::LoadPlayer()
{
	Scene * m_currentScene = SceneManager::getInstance()->getCurrentScene();
	m_currentScene->playerInfo = CPlayerInfo::GetInstance();

	//Set player speed from lua
	m_currentScene->playerInfo->SetPlayerSpeed(getFloatValue("playerspeed"));
	//Set player health from lua
	m_currentScene->playerInfo->SetPlayerHealth(getIntValue("playerhealth"));
	//Set player pos from lua
	lua_getglobal(theLuaState, "playerpos");
	m_currentScene->playerInfo->SetPos(Vector3(GetField("x"), GetField("y"), GetField("z")));

	//Set current weapon: 0 - sniper(pistol) , 1 rifle
	m_currentScene->playerInfo->currentWeapon = getIntValue("currentWeapon");

	m_currentScene->playerInfo->Init();
}

void CLuaInterface::GetElementFromTable(string input, int * value1, string *value2, float * value3, bool isFloat)
{
	lua_pushstring(theLuaState, input.c_str());
	lua_gettable(theLuaState, -2);
	if (lua_isnumber(theLuaState, -1))
	{
		if (isFloat == false)
			*value1 = lua_tointeger(theLuaState, -1);
		else
			*value3 = (float)lua_tonumber(theLuaState, -1);
	}
	else if (lua_isstring(theLuaState, -1))
	{
		*value2 = lua_tostring(theLuaState, -1);
	}

	lua_pop(theLuaState, 1);

}

float CLuaInterface::GetDistance(const char* varName, Vector3 source, Vector3 destination)
{
	lua_getglobal(theLuaState, varName);
	lua_pushnumber(theLuaState, source.x);
	lua_pushnumber(theLuaState, source.y);
	lua_pushnumber(theLuaState, source.z);
	lua_pushnumber(theLuaState, destination.x);
	lua_pushnumber(theLuaState, destination.y);
	lua_pushnumber(theLuaState, destination.z);
	lua_call(theLuaState, 6, 1);
	float distance = (float)lua_tonumber(theLuaState, -1);
	lua_pop(theLuaState, 1);
	return distance;
}

void CLuaInterface::GetTankCameraPosition(const char * varName, float collidePos, float radius, float value1, float value2 , float & camPos)
{
	lua_getglobal(theLuaState, varName);
	lua_pushnumber(theLuaState, collidePos);
	lua_pushnumber(theLuaState, radius);
	lua_pushnumber(theLuaState, value1);
	lua_pushnumber(theLuaState, value2);
	lua_call(theLuaState, 4, 1);
	camPos = (float)lua_tonumber(theLuaState, -1);
	lua_pop(theLuaState, 1);
}

float CLuaInterface::GetTankRotateAngle(const char * varName, float angle, float multipler, float dt)
{
	lua_getglobal(theLuaState, varName);
	lua_pushnumber(theLuaState, angle);
	lua_pushnumber(theLuaState, multipler);
	lua_pushnumber(theLuaState, dt);
	lua_call(theLuaState, 3, 1);
	float calculatedAngle = (float)lua_tonumber(theLuaState, -1);
	lua_pop(theLuaState, 1);
	return calculatedAngle;
}
