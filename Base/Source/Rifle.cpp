#include "Rifle.h"
#include "LuaInterface.h"

CRifle::CRifle() : CWeaponInfo("Rifle")
{
}


CRifle::~CRifle()
{
}

// Initialise this instance to default values
void CRifle::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = CLuaInterface::GetInstance()->getIntValue("rifleMagRounds");
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = CLuaInterface::GetInstance()->getIntValue("rifleMaxMagRounds");
	// The current total number of rounds currently carried by this player
	totalRounds = CLuaInterface::GetInstance()->getIntValue("rifleTotalRounds");
	// The max total number of rounds currently carried by this player
	maxTotalRounds = CLuaInterface::GetInstance()->getIntValue("rifleMaxTotalRounds");
	// Clip
	maxClip = 8;
	currentClip = maxClip;

	reloadTime = CLuaInterface::GetInstance()->getFloatValue("rifleReloadTime");

	lua_getglobal(CLuaInterface::GetInstance()->thePlayerLuaState, "rifleTimeBetweenShots");
	// The time between shots
	timeBetweenShots = (float)lua_tonumber(CLuaInterface::GetInstance()->thePlayerLuaState, -1);

	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	bulletMeshName = "sphere";
}
