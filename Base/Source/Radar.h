#ifndef RADAR_H
#define RADAR_H

#include <vector>


using std::vector;



class GameObject;
class Scene;

class Radar {

public:
	Radar(Scene* manager);
	~Radar() {};

	void addUnit(GameObject* unit);
	void removeUnit(GameObject* unit);

	void SetRadarRange(float range);

	void RenderRadar(float x, float y);

private:
	Scene* _scene;

	vector<GameObject*> unitContainer; // To access the position of the units

	float _RadarRange; // Distance before the Radar can detect any enemy, change this to increase Radar's detection range
	float _RadarSize; // Background size of the radar
	float _RadarSizeSquared; // Helper variable: Square length of the Radar
	float _RadarEnemySize; // Enemy Icon Size: Scaling is relative to _RadarSize :: (This * _RadarSize)
	float _RadarPlayerSize; // Player Icon Size: Scaling is relative to _RadarSize :: (This * _RadarSize)


};
#endif