#include "Application.h"
#include "GameObject.h"
#include "Scene.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"


int GameObject::index = 0;

GameObject::GameObject(Scene* scene, unsigned typeValue, float boxWidth, float boxHeight, float boxDepth)
	:
	m_scene(scene),
	type(typeValue),
	scale(1, 1, 1),
	rotation(0, 0, 0),
	collider(this, 0, 0, 0),
	SceneNode(std::to_string(++index), this)
{
	momentOfInertia = mass * scale.x * scale.x;
}

GameObject::~GameObject()
{
}

bool GameObject::Update() {
	return false;
}

void GameObject::UpdatePhysics() {

	// Update Object's velocity based on m_force
	Vector3 a = m_force / mass;
	vel += a * m_scene->_dt;

	if (MAX_VELOCITY != 0 && vel.LengthSquared() > MAX_VELOCITY * MAX_VELOCITY) {
		vel = vel.Normalized() * MAX_VELOCITY;
	}

	// Modify Object position
	pos += vel * m_scene->_dt;

	if (b_EnableGravity)
		vel.y -= 98 * m_scene->_dt;

	// Angular movement
	float alpha = m_torque.z / momentOfInertia;
	angularVelocity += alpha * m_scene->_dt;

	// Only clamp omega if MAX_ANGULAR_VELOCITY is set
	if (MAX_ANGULAR_VELOCITY != 0)
		angularVelocity = Math::Clamp<float>(angularVelocity, -MAX_ANGULAR_VELOCITY, MAX_ANGULAR_VELOCITY);

	float theta = atan2(dir.y, dir.x);
	theta += angularVelocity * m_scene->_dt;
	dir.Set(cos(theta), sin(theta), 0);

	m_force.SetZero();
	m_torque.SetZero();

	// rotation.z = Math::RadianToDegree(atan2(dir.y, dir.x)) - 90;
}

void GameObject::Render()
{
	if (!b_RenderMe)
		return;

	Scene* _scene = static_cast<Scene*>(m_scene);

	m_scene->modelStack->PushMatrix();
	m_scene->modelStack->Translate(pos.x, pos.y, pos.z);
	m_scene->modelStack->Rotate(rotation.y, 0, 1, 0);
	m_scene->modelStack->Rotate(rotation.z, 0, 0, 1);
	m_scene->modelStack->Rotate(rotation.x, 1, 0, 0);
	m_scene->modelStack->Scale(scale.x, scale.y, scale.z);

		if (GetLODStatus() == true || theDetailLevel == NO_DETAILS) {
			if (theDetailLevel != NO_DETAILS)
			{
				if (b_EnableLight)
					RenderHelper::RenderMeshWithLight(GetLODMesh());
				else
					RenderHelper::RenderMesh(GetLODMesh());
			}

		}
		else {
			if (b_EnableLight)
				RenderHelper::RenderMeshWithLight(m_scene->meshList[type]);
			else
				RenderHelper::RenderMesh(m_scene->meshList[type]);
		}
	
	m_scene->modelStack->PopMatrix();
}

void GameObject::OnDestroy()
{
}