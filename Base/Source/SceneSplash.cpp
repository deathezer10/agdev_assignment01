#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "Application.h"
#include "SceneSplash.h"
#include "SceneMainMenu.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "MouseController.h"
#include "KeyboardController.h"
#include "RenderHelper.h"
#include "MeshBuilder.h"
#include "LoadTGA.h"



void SceneSplash::Init()
{
	SceneBase::Init();


	// Create the playerinfo instance, which manages all information about the player
	playerInfo = CPlayerInfo::GetInstance();
	playerInfo->Init();

	// Create and attach the camera to the scene
	camera.Init(playerInfo->GetPos(), playerInfo->GetTarget(), playerInfo->GetUp());
	playerInfo->AttachCamera(&camera);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	meshList[GEO_SPLASHIMAGE] = MeshBuilder::GenerateRadarQuad("GEO_SPLASHIMAGE", Color(1, 1, 1));
	meshList[GEO_SPLASHIMAGE]->textureID = LoadTGA("Image/splashscreen.tga", false);

	m_CurrentSplashTimer = 3;

	glfwSetInputMode(glfwGetCurrentContext(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	MouseController::GetInstance()->SetKeepMouseCentered(false);
}


void SceneSplash::Update(double dt) {

	SceneBase::Update(dt);

	if (m_CurrentSplashTimer > -1) {
		m_CurrentSplashTimer -= (float)dt;

		m_CurrentSplashAlpha = 1 - (m_CurrentSplashTimer / 3);
	}
	else {
		SceneManager::getInstance()->changeScene(new SceneMainMenu());
		return;
	}

	meshList[GEO_SPLASHIMAGE]->alpha = m_CurrentSplashAlpha;

	if (KeyboardController::GetInstance()->IsKeyReleased(GLFW_KEY_SPACE) || MouseController::GetInstance()->IsButtonReleased(0)) {
		SceneManager::getInstance()->changeScene(new SceneMainMenu());
		return;
	}

	GraphicsManager::GetInstance()->UpdateLights(dt);
}

void SceneSplash::Render() {

	SceneBase::Render();

	GraphicsManager::GetInstance()->SetPerspectiveProjection(45, Application::windowWidth() / Application::windowHeight(), 0.1f, 10000.0f);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Setup 2D pipeline then render 2D
	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();


	textManager.dequeueMesh();

	float winHeight = (float)Application::windowHeight() / 10;
	float winWidth = (float)Application::windowWidth() / 10;

	textManager.RenderMeshOnScreen(meshList[GEO_SPLASHIMAGE], winWidth * 0.5f, winHeight * 0.5f, Vector3(90, 0, 0), Vector3(winWidth / 2, 1, winHeight / 2));
	textManager.renderTextOnScreen(UIManager::Text{ std::string("Press <Space> to continue"), Color(0,0,0), UIManager::ANCHOR_BOT_CENTER });

	textManager.dequeueText();
	textManager.RenderTimedMeshOnScreen();
	textManager.reset(); // Must be called at the end of Render()
}

void SceneSplash::Exit()
{
	m_GUI.Exit();

	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();
	playerInfo->DetachCamera();

	playerInfo->DropInstance();

	goManager.Exit();

	GraphicsManager::GetInstance()->RemoveLight("lights[0]");

	SceneBase::Exit();
}
