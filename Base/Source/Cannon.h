#pragma once
#include "WeaponInfo.h"

class CCannon :
	public CWeaponInfo
{
public:
	CCannon();
	virtual ~CCannon();

	// Initialise this instance to default values
	void Init(void);

	virtual void Discharge(Vector3 position, Vector3 target, CPlayerInfo* _source = NULL);
};

