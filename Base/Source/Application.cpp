#include "Application.h"
#include "MouseController.h"
#include "KeyboardController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "LuaInterface.h"

//Include GLEW
#include <GL/glew.h>

//Include GLFW
#include <GLFW/glfw3.h>

//Include the standard C++ headers
#include <stdio.h>
#include <stdlib.h>

#include "SceneSplash.h"
#include "SceneOptions.h"


bool Application::_isPaused = false;
bool Application::_isFullScreen = false;
bool Application::_isValid = true;

int Application::m_window_width = 0;
int Application::m_window_height = 0;

int Application::m_full_window_width = 0;
int Application::m_full_window_height = 0;

GLFWwindow* m_window;

//Define an error callback
static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}

//Define the key input callback
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	auto scene = dynamic_cast<SceneOptions*>(SceneManager::getInstance()->getCurrentScene());

	// Detect key input and set it if its a valid key
	if (scene) {
		scene->SetBindingKey(key);
	}

	if (key == GLFW_KEY_F1 && action == GLFW_RELEASE) {
		Application::ToggleFullscreen();
	}
}

float Application::WindowToFullscreenWidthRatio() {
	return (float)m_full_window_width / m_window_width;
}

float Application::WindowToFullscreenHeightRatio() {
	return (float)m_full_window_height / m_window_height;
}

void Application::ToggleCursor() {
	if (glfwGetInputMode(m_window, GLFW_CURSOR) == GLFW_CURSOR_DISABLED) {
		glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		MouseController::GetInstance()->CenterMousePosition();
		MouseController::GetInstance()->SetKeepMouseCentered(false);
	}
	else {
		glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		MouseController::GetInstance()->SetKeepMouseCentered(true);
	}

}

void Application::ToggleFullscreen() {

	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	if (_isFullScreen == false) { // Switch to Borderless Fullscreen
		glfwSetWindowMonitor(m_window, glfwGetPrimaryMonitor(), 0, 0, mode->width, mode->height, mode->refreshRate);
		m_full_window_width = mode->width;
		m_full_window_height = mode->height;
	}
	else { // Switch to Windowed
		glfwSetWindowMonitor(m_window, NULL, mode->width / 5, mode->height / 5, m_window_width, m_window_height, GL_DONT_CARE);
		m_full_window_width = m_window_width;
		m_full_window_height = m_window_height;
	}

	_isValid = false; // Window was resized

	_isFullScreen = !_isFullScreen;

	HWND windowHandle = GetForegroundWindow();
	long Style = GetWindowLong(windowHandle, GWL_STYLE);
	Style &= ~WS_MAXIMIZEBOX;
	SetWindowLong(windowHandle, GWL_STYLE, Style);
}

void resize_callback(GLFWwindow* window, int w, int h)
{
	glViewport(0, 0, w, h);
}

bool Application::IsKeyPressed(unsigned short key)
{
	return (glfwGetKey(glfwGetCurrentContext(), key) == GLFW_PRESS);
}

Application::Application()
{
}

Application::~Application()
{
}

void Application::Init()
{
	//Set the error callback
	glfwSetErrorCallback(error_callback);

	//Initialize GLFW
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	//Current monitor's video mode
	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	//Set the GLFW window creation hints - these are optional
	glfwWindowHint(GLFW_SAMPLES, 4); //Request 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //Request a specific OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //Request a specific OpenGL version
												   //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 

	//Set the Video mode to same as the current one
	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

	//Initialise the Lua system
	CLuaInterface::GetInstance()->Init();

	//Get the OpenGl resolution
	m_default_window_width = CLuaInterface::GetInstance()->getIntValue("width");
	m_default_window_height = CLuaInterface::GetInstance()->getIntValue("height");

	m_window_width = m_default_window_width;
	m_window_height = m_default_window_height;

	m_full_window_width = m_default_window_width;
	m_full_window_height = m_default_window_height;

	CLuaInterface::GetInstance()->Run();

	//Create a window and create its OpenGL context
	m_window = glfwCreateWindow(m_window_width, m_window_height, "AGDev Assignment 02", NULL, NULL);

	HWND windowHandle = GetForegroundWindow();
	long Style = GetWindowLong(windowHandle, GWL_STYLE);
	Style &= ~WS_MAXIMIZEBOX;
	SetWindowLong(windowHandle, GWL_STYLE, Style);

	//If the window couldn't be created
	if (!m_window)
	{
		fprintf(stderr, "Failed to open GLFW window.\n");
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	// Set windows position
	glfwSetWindowPos(m_window, mode->width / 5, mode->height / 5);

	//This function makes the context of the specified window current on the calling thread. 
	glfwMakeContextCurrent(m_window);

	//Sets the key callback
	glfwSetKeyCallback(m_window, key_callback);
	glfwSetWindowSizeCallback(m_window, resize_callback);

	glewExperimental = true; // Needed for core profile
	//Initialize GLEW
	GLenum err = glewInit();

	//If GLEW hasn't initialized
	if (err != GLEW_OK)
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		//return -1;
	}

	// Hide the cursor
	glfwSetMouseButtonCallback(m_window, &Application::MouseButtonCallbacks);
	glfwSetScrollCallback(m_window, &Application::MouseScrollCallbacks);

}

void Application::Run()
{
	SceneManager* scene_manager = SceneManager::getInstance();

	scene_manager->changeScene(new SceneSplash());
	scene_manager->Update();
	scene_manager->Exit(); // Cleaning up, chances are that the Application is terminating after this	
}

void Application::Exit()
{
	//Drop the Lua system
	CLuaInterface::GetInstance()->Drop();

	//Close OpenGL window and terminate GLFW
	glfwDestroyWindow(m_window);
	//Finalize and clean up GLFW
	glfwTerminate();
}

void Application::UpdateInput()
{
	// Update Mouse Position
	double mouse_currX, mouse_currY;
	glfwGetCursorPos(m_window, &mouse_currX, &mouse_currY);
	MouseController::GetInstance()->UpdateMousePosition(mouse_currX, mouse_currY);

	// Update Keyboard Input
	for (int i = GLFW_KEY_SPACE; i < GLFW_KEY_LAST; ++i)
		KeyboardController::GetInstance()->UpdateKeyboardStatus(i, IsKeyPressed(i));
}

void Application::PostInputUpdate()
{
	// If mouse is centered, need to update the center position for next frame
	if (MouseController::GetInstance()->GetKeepMouseCentered())
	{
		double mouse_currX, mouse_currY;
		mouse_currX = m_window_width >> 1;
		mouse_currY = m_window_height >> 1;
		MouseController::GetInstance()->UpdateMousePosition(mouse_currX, mouse_currY);
		glfwSetCursorPos(m_window, mouse_currX, mouse_currY);
	}

	// Call input systems to update at end of frame
	MouseController::GetInstance()->EndFrameUpdate();
	KeyboardController::GetInstance()->EndFrameUpdate();
}

void Application::MouseButtonCallbacks(GLFWwindow* window, int button, int action, int mods)
{
	// Send the callback to the mouse controller to handle
	if (action == GLFW_PRESS)
		MouseController::GetInstance()->UpdateMouseButtonPressed(button);
	else
		MouseController::GetInstance()->UpdateMouseButtonReleased(button);
}

void Application::MouseScrollCallbacks(GLFWwindow* window, double xoffset, double yoffset)
{
	MouseController::GetInstance()->UpdateMouseScroll(xoffset, yoffset);
}

int Application::GetWindowHeight()
{
	return m_window_height;
}

int Application::GetWindowWidth()
{
	return m_window_width;
}