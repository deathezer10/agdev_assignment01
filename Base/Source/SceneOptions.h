#pragma once 

#include "SceneBase.h"
#include "Mtx44.h"
#include "Mesh.h"
#include "MatrixStack.h"

class SceneOptions : public SceneBase {

public:
	SceneOptions() {};
	~SceneOptions() {};

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

	void SetBindingKey(int key);

private:
	Light* lights[1];
	
	int hotkeySkybox;
	int hotkeyW;
	int hotkeyS;
	int hotkeyA;
	int hotkeyD;
	int hotkey1;
	int hotkey2;
	int hotkeyLShift;

	GUIButton* btnSave;
	GUIButton* btnQuit;

	GUIButton* btnSkybox;
	GUIButton* btnW;
	GUIButton* btnA;
	GUIButton* btnS;
	GUIButton* btnD;
	GUIButton* btn1;
	GUIButton* btn2;
	GUIButton* btnLShift;

	bool isBinding = false;

	GUIButton* currButtonBinding = nullptr;
	int* currHotkey = nullptr;

	const char* GetModKeyName(int key);

};