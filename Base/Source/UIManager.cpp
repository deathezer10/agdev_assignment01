#include "Application.h"
#include <fstream>

#include "GL\glew.h"

#include "Scene.h"
#include "Mesh.h"
#include "UIManager.h"
#include "GraphicsManager.h"
#include "ShaderProgram.h"
#include "RenderHelper.h"

#include <sstream>


using std::ostringstream;



UIManager::UIManager(Scene* scene) {

	_scene = scene;

	textMesh = &(_scene->meshList[Scene::GEO_TEXT]);

	anchor_offset[ANCHOR_BOT_LEFT] = 0;
	anchor_offset[ANCHOR_BOT_RIGHT] = 0;
	anchor_offset[ANCHOR_TOP_LEFT] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_TOP_RIGHT] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_BOT_CENTER] = 0;
	anchor_offset[ANCHOR_TOP_CENTER] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_CENTER_CENTER] = 0;

}

bool UIManager::LoadFontWidth(std::string fontPath) {

	// Clear vector if it's loaded
	if (currentFontWidth.empty())
		currentFontWidth.clear();

	std::ifstream fileStream(fontPath);

	if (!fileStream.is_open()) {
		std::cout << "Impossible to open " << fontPath << ". Are you in the right directory ?\n";
		return false;
	}

	std::string data;
	while (std::getline(fileStream, data, '\n')) // read every line
	{
		currentFontWidth.push_back(std::stoul(data));
	}

	return true;
}

void UIManager::queueRenderText(Text text) {
	currentTextQueue.push(text);
}

void UIManager::dequeueMesh() {

	// Render all queued meshes
	while (!currentMeshQueue.empty()) {

		MeshQueue mesh = currentMeshQueue.front();

		_scene->modelStack->PushMatrix();
		_scene->modelStack->Translate(mesh.position.x, mesh.position.y, mesh.position.z);
		_scene->modelStack->Scale(mesh.scaling.x, mesh.scaling.y, mesh.scaling.z);
		_scene->modelStack->Rotate(mesh.rotation.x, 1, 0, 0);
		_scene->modelStack->Rotate(mesh.rotation.y, 0, 1, 0);
		_scene->modelStack->Rotate(mesh.rotation.z, 0, 0, 1);

		if (mesh.wireframe == true) {
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //default fill mode
			RenderHelper::RenderMesh(mesh.mesh);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //default fill mode
		}
		else {
			RenderHelper::RenderMesh(mesh.mesh);
		}

		_scene->modelStack->PopMatrix();

		currentMeshQueue.pop();
	}
}

void UIManager::dequeueText() {
	// Print all the queued Texts
	while (!currentTextQueue.empty()) {
		renderTextOnScreen(currentTextQueue.front());
		currentTextQueue.pop();
	}
}


void UIManager::renderTextOnScreen(Vector3 pos, Vector3 scale, string text, Color color) {

	if (!textMesh || (*textMesh)->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);

	ShaderProgram* currProg = GraphicsManager::GetInstance()->GetActiveShader();

	// Backup
	Mtx44 originalProjStack = GraphicsManager::GetInstance()->GetProjectionMatrix();
	Mtx44 originalViewStack = GraphicsManager::GetInstance()->GetViewMatrix();

	Mtx44 ortho;
	ortho.SetToOrtho(0, (double)Application::windowWidth() / 10, 0, (double)Application::windowHeight() / 10, -10, 10); //size of screen UI	
	GraphicsManager::GetInstance()->projectionMatrix = ortho; // set to ortho first
	GraphicsManager::GetInstance()->viewMatrix.SetToIdentity();
	GraphicsManager::GetInstance()->modelStack.PushMatrix();
	GraphicsManager::GetInstance()->modelStack.LoadIdentity(); //Reset modelStack

	currProg->UpdateInt("textEnabled", 1);
	currProg->UpdateVector3("textColor", &color.r);
	currProg->UpdateInt("lightEnabled", 0);
	currProg->UpdateInt("colorTextureEnabled", 1);
	GraphicsManager::GetInstance()->UpdateTexture(0, (*textMesh)->textureID);
	currProg->UpdateInt("colorTexture", 0);

	// Width of the entire string
	float totalWidth = 0;

	// Calculate total width of the string, used for offsetting the anchors
	for (unsigned i = 0; i < text.length(); ++i) {
		totalWidth += currentFontWidth[text[i]] / 32.0f;
	}

	float tX, tY;
	tX = ((Application::windowWidth() / 10) * pos.x) - (totalWidth / 2);
	tY = (Application::windowHeight() / 10) * pos.y - 1;

	GraphicsManager::GetInstance()->modelStack.Translate(tX + 1, tY + 1, 0); // minor offset to display properly

	MS textStack;
	unsigned stackCount = 0;

	float prevWidth = 0; // the previous printed character width

	textStack.Scale(scale.x, scale.y, scale.z);

	for (unsigned i = 0; i < text.length(); ++i, ++stackCount) {

		textStack.PushMatrix();
		textStack.Translate(prevWidth, 0, 0); // spacing of each character
		prevWidth = currentFontWidth[text[i]] / 32.0f;
		Mtx44 MVP = GraphicsManager::GetInstance()->GetProjectionMatrix() * GraphicsManager::GetInstance()->GetViewMatrix() * GraphicsManager::GetInstance()->GetModelStack().Top() * textStack.Top();
		currProg->UpdateMatrix44("MVP", &MVP.a[0]);
		(*textMesh)->Render((unsigned)text[i] * 6, 6);
	}

	GraphicsManager::GetInstance()->UnbindTexture(0);
	currProg->UpdateInt("textEnabled", 0);


	for (unsigned i = 0; i < stackCount; ++i) {
		textStack.PopMatrix();
	}

	// Restore backup
	GraphicsManager::GetInstance()->projectionMatrix = originalProjStack;
	GraphicsManager::GetInstance()->viewMatrix = originalViewStack;

	GraphicsManager::GetInstance()->modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);

}

void UIManager::renderTextOnScreen(Text text) {

	if (!textMesh || (*textMesh)->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);

	ShaderProgram* currProg = GraphicsManager::GetInstance()->GetActiveShader();


	// Backup
	Mtx44 originalProjStack = GraphicsManager::GetInstance()->GetProjectionMatrix();
	Mtx44 originalViewStack = GraphicsManager::GetInstance()->GetViewMatrix();

	Mtx44 ortho;
	ortho.SetToOrtho(0, (double)Application::windowWidth() / 10, 0, (double)Application::windowHeight() / 10, -10, 10); //size of screen UI	
	GraphicsManager::GetInstance()->projectionMatrix = ortho; // set to ortho first
	GraphicsManager::GetInstance()->viewMatrix.SetToIdentity();
	GraphicsManager::GetInstance()->modelStack.PushMatrix();
	GraphicsManager::GetInstance()->modelStack.LoadIdentity(); //Reset modelStack

	currProg->UpdateInt("textEnabled", 1);
	currProg->UpdateVector3("textColor", &text.color.r);
	currProg->UpdateInt("lightEnabled", 0);
	currProg->UpdateInt("colorTextureEnabled", 1);
	GraphicsManager::GetInstance()->UpdateTexture(0, (*textMesh)->textureID);
	currProg->UpdateInt("colorTexture", 0);

	// Width of the entire string
	float totalWidth = 0;

	// Calculate total width of the string, used for offsetting the anchors
	for (unsigned i = 0; i < text.value.length(); ++i) {
		totalWidth += currentFontWidth[text.value[i]] / 32.0f;
	}

	float tX = 0, tY = 0;

	// Move the text position according to the given anchor
	switch (text.anchor) {

	case ANCHOR_BOT_LEFT:
		tY = (float)anchor_offset[ANCHOR_BOT_LEFT];
		anchor_offset[ANCHOR_BOT_LEFT] += 2;
		break;

	case ANCHOR_BOT_RIGHT:
		tX = (Application::windowWidth() / 10) - totalWidth;
		tY = (float)anchor_offset[ANCHOR_BOT_RIGHT];
		anchor_offset[ANCHOR_BOT_RIGHT] += 2;
		break;

	case ANCHOR_TOP_LEFT:
		tY = (float)anchor_offset[ANCHOR_TOP_LEFT];
		anchor_offset[ANCHOR_TOP_LEFT] -= 2;
		break;

	case ANCHOR_TOP_RIGHT:
		tX = (Application::windowWidth() / 10) - totalWidth;
		tY = (float)anchor_offset[ANCHOR_TOP_RIGHT];
		anchor_offset[ANCHOR_TOP_RIGHT] -= 2;
		break;

	case ANCHOR_BOT_CENTER:
		tX = (Application::windowWidth() / 20) - (totalWidth / 2);
		tY = (float)anchor_offset[ANCHOR_BOT_CENTER];
		anchor_offset[ANCHOR_BOT_CENTER] += 2;
		break;

	case ANCHOR_TOP_CENTER:
		tX = (Application::windowWidth() / 20) - (totalWidth / 2);
		tY = (float)anchor_offset[ANCHOR_TOP_CENTER];
		anchor_offset[ANCHOR_TOP_CENTER] -= 2;
		break;

	case ANCHOR_CENTER_CENTER:
		tX = (Application::windowWidth() / 20) - (totalWidth / 2);
		tY = (Application::windowHeight() / 20) - (float)anchor_offset[ANCHOR_CENTER_CENTER];
		anchor_offset[ANCHOR_CENTER_CENTER] += 2;
		break;

	default:
		break;

	}

	GraphicsManager::GetInstance()->modelStack.Translate(tX + 1, tY + 1, 0); // minor offset to display properly

	MS textStack;
	unsigned stackCount = 0;

	float prevWidth = 0; // the previous printed character width

	for (unsigned i = 0; i < text.value.length(); ++i, ++stackCount) {

		textStack.PushMatrix();
		textStack.Translate(prevWidth, 0, 0); // spacing of each character

		prevWidth = currentFontWidth[text.value[i]] / 32.0f;
		Mtx44 MVP = GraphicsManager::GetInstance()->GetProjectionMatrix() * GraphicsManager::GetInstance()->GetViewMatrix() * GraphicsManager::GetInstance()->GetModelStack().Top() * textStack.Top();
		currProg->UpdateMatrix44("MVP", &MVP.a[0]);
		(*textMesh)->Render((unsigned)text.value[i] * 6, 6);
	}

	GraphicsManager::GetInstance()->UnbindTexture(0);
	currProg->UpdateInt("textEnabled", 0);


	for (unsigned i = 0; i < stackCount; ++i) {
		textStack.PopMatrix();
	}

	// Restore backup
	GraphicsManager::GetInstance()->projectionMatrix = originalProjStack;
	GraphicsManager::GetInstance()->viewMatrix = originalViewStack;

	GraphicsManager::GetInstance()->modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);

}

void UIManager::addTimedMeshToScreen(MeshQueue mesh, float duration) {
	currentTimeMeshes[mesh] = duration;
}

void UIManager::RenderTimedMeshOnScreen() {


	for (auto &it = currentTimeMeshes.begin(); it != currentTimeMeshes.end();) {

		MeshQueue mq = it->first;

		if (it->second > 0) {
			RenderMeshOnScreen(mq.mesh, mq.position.x, mq.position.y, mq.rotation, mq.scaling);
			it->second -= _scene->_dt;
			++it;
		}
		else {
			it = currentTimeMeshes.erase(it);
		}
	}

}

void UIManager::RenderMeshOnScreen(Mesh* mesh, float x, float y, Vector3 rotate, Vector3 scale) {
	glDisable(GL_DEPTH_TEST);

	// Backup

	Mtx44 ortho;
	ortho.SetToOrtho(0, (double)Application::windowWidth() / 10, 0, (double)Application::windowHeight() / 10, -100, 100); //size of screen UI	
	GraphicsManager::GetInstance()->projectionMatrix = ortho; // set to ortho first
	GraphicsManager::GetInstance()->viewMatrix.SetToIdentity();

	_scene->modelStack->PushMatrix();
	_scene->modelStack->LoadIdentity();
	_scene->modelStack->Translate(x, y, 1);
	_scene->modelStack->Rotate(rotate.y, 0, 1, 0);
	_scene->modelStack->Rotate(rotate.z, 0, 0, 1);
	_scene->modelStack->Rotate(rotate.x, 1, 0, 0);
	_scene->modelStack->Scale(scale.x, scale.y, scale.z);
	RenderHelper::RenderMesh(mesh); //UI should not have light

	// Restore backup


	_scene->modelStack->PopMatrix();
	glEnable(GL_DEPTH_TEST);
}

void UIManager::reset() {

	anchor_offset[ANCHOR_BOT_LEFT] = 0;
	anchor_offset[ANCHOR_BOT_RIGHT] = 0;
	anchor_offset[ANCHOR_TOP_LEFT] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_TOP_RIGHT] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_BOT_CENTER] = 0;
	anchor_offset[ANCHOR_TOP_CENTER] = (Application::windowHeight() / 10) - 2;
	anchor_offset[ANCHOR_CENTER_CENTER] = 0;

}