#include "Pistol.h"
#include "LuaInterface.h"

CPistol::CPistol() : CWeaponInfo("Pistol")
{
}


CPistol::~CPistol()
{
}

// Initialise this instance to default values
void CPistol::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = CLuaInterface::GetInstance()->getIntValue("sniperMagRounds");
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = CLuaInterface::GetInstance()->getIntValue("sniperMaxMagRounds");
	// The current total number of rounds currently carried by this player
	totalRounds = CLuaInterface::GetInstance()->getIntValue("sniperTotalRounds");
	// The max total number of rounds currently carried by this player
	maxTotalRounds = CLuaInterface::GetInstance()->getIntValue("sniperMaxTotalRounds");
	//
	maxClip = 1;
	//
	currentClip = maxClip;

	reloadTime = CLuaInterface::GetInstance()->getFloatValue("sniperReloadTime");

	// The time between shots
	timeBetweenShots = CLuaInterface::GetInstance()->getFloatValue("sniperTimeBetweenShots");
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	bulletMeshName = "cube";
}
