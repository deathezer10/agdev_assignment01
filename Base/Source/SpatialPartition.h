#pragma once

#include "Vector3.h"
#include "Grid.h"

class CSpatialPartition
{
protected:
	static CSpatialPartition *sp_instance;
	// Constructor
	CSpatialPartition(void);

	// Variables
	CGrid* theGrid;
	int xSize;
	int zSize;
	int xGridSize;
	int zGridSize;
	int xNumOfGrid;
	int zNumOfGrid;
	float yOffset;

	// We store the pointer to the Camera so we can get it's position and direction to calculate LOD and visibility
	FPSCamera* theCamera;
	// LOD distances
	float LevelOfDetails_Distances[2];

public:
	static CSpatialPartition *GetInstance()
	{
		if (!sp_instance)
			sp_instance = new CSpatialPartition;
		return sp_instance;
	}
	static bool DropInstance()
	{
		if (sp_instance)
		{
			delete sp_instance;
			sp_instance = NULL;
			return true;
		}
		return false;
	}
	~CSpatialPartition(void);

	// Initialise the spatial partition
	bool Init(	const int xGridSize, const int zGridSize, 
				const int xNumOfGrid, const int zNumOfGrid, 
				const float yOffset = -9.9f);

	// ApplyMesh
	void ApplyMesh(Mesh * mesh);

	// Update the spatial partition
	void Update(void);
	// Render the spatial partition
	void Render(Vector3* theCameraPosition = NULL);

	// Get xSize of the entire spatial partition
	int GetxSize(void) const;
	// Get zSize of the entire spatial partition
	int GetzSize(void) const;
	// Get xSize
	int GetxGridSize(void) const;
	// Get zNumOfGrid
	int GetzGridSize(void) const;
	// Get xNumOfGrid
	int GetxNumOfGrid(void) const;
	// Get zNumOfGrid
	int GetzNumOfGrid(void) const;

	// Get a particular grid
	CGrid GetGrid(const int xIndex, const int zIndex) const;

	// Get vector of objects from this Spatial Partition
	vector<SceneNode*> GetObjects(Vector3 position);

	// Returns all SceneNodes in this and its neighbour's partition
	vector<SceneNode*> GetObjectsWithNeighbour(Vector3 position);

	vector<SceneNode*> GetObjectsWithNeighbour(int _xIndex, int _zIndex);
	
	// Add a new object
	void Add(GameObject* theObject);
	// Remove but not delete object from this grid
	void Remove(GameObject* theObject);

	// Calculate the squared distance from camera to a grid's centrepoint
	float CalculateDistance(Vector3* theCameraPosition, const int xIndex, const int zIndex);

	//PrintSelf
	void PrintSelf() const;

	// The vector of objects due for migration to another grid
	vector<GameObject*> MigrationList;

	// Handling Camera
	void SetCamera(FPSCamera* _cameraPtr);
	void RemoveCamera(void);

	// Set LOD distances
	void SetLevelOfDetails(const float distance_High2Mid, const float distance_Mid2Low);
	// Check if a CGrid is visible to the camera
	bool IsVisible(Vector3 theCameraPosition, Vector3 theCameraDirection, const int xIndex, const int zIndex);

private:
	// Returns true if row/col is inside the spatial partition range
	bool IsInsideGrid(int xIndex, int zIndex);

};
