
enemyCount = 3

enemy0 = {
	
   waypointCount = 4,
   waypoint0 = {x=10.0, y=0 ,z=50},
   waypoint1 = {x=10, y=0 , z=-50},
   waypoint2 = {x=-10, y=0 ,z=-50},
   waypoint3 = {x=-10, y=0 ,z=50},
   enemypos = {x =10, y= 0, z =50},
   currentWayPointIndex = 2,
	   health = 100	,
	   patrolSpeed = 1.0,
	   chaseSpeed = 25.0,
      distanceToSwitchState = 50.0,
}

enemy1 = {
	
   waypointCount = 4,
   waypoint0 = {x=500, y=0 ,z= 200},
   waypoint1 = {x=500, y=0 , z=100},
   waypoint2 = {x= 300, y=0 ,z=100},
   waypoint3 = {x=300, y=0 ,z=200},
   enemypos = {x =200, y= 0, z =50},
   currentWayPointIndex = 0,
      health = 100	,
	   patrolSpeed = 1.0,
	   chaseSpeed = 25.0,
	  distanceToSwitchState = 50.0,
}


enemy2 = {
	
  waypointCount = 4,
   waypoint0 = {x=100, y=0 ,z= 200},
   waypoint1 = {x=100, y=0 , z=100},
   waypoint2 = {x= 200, y=0 ,z= 100},
   waypoint3 = {x=-200, y=0 ,z=200},
   enemypos = {x =100, y= 0, z =100},
   currentWayPointIndex = 0,
      health = 100	,
	   patrolSpeed = 1.0,
	   chaseSpeed = 25.0,
	   distanceToSwitchState = 50.0,
}




