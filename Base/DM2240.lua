function SaveToLuaFile(outputString, overwrite , fileName)
   print("SaveToLuaFile...")
   local f;						-- The file
   if overwrite == 1 then		-- Wipe the contents with new data
      f = assert(io.open(fileName, "w"))
   elseif overwrite == 0 then	-- Append with new data
      f = assert(io.open(fileName, "a"))
   end
   -- Write to the file
   f:write(outputString)
   -- Close the file
   f:close()
   print("OK")
end

title = "DM2240 - Week 14 Scripting"
width = 800
height = 600
numberOfTrees = 10;
groundTerrainScale = {x=1 ,y=1, z=1}

