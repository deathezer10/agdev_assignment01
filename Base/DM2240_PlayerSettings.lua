playerspeed = 40
playerhealth = 100
playerpos = {x=0, y=0 ,z=50}

currentWeapon = 0

sniperMagRounds = 8
sniperMaxMagRounds = 8
sniperTotalRounds = 24
sniperMaxTotalRounds = 24
sniperReloadTime = 2
sniperTimeBetweenShots = 0.3
sniperBulletDamage = 20
sniperBulletSpeed  = 100.0

rifleMagRounds = 30
rifleMaxMagRounds = 30
rifleTotalRounds = 90
rifleMaxTotalRounds = 90
rifleReloadTime = 3
rifleTimeBetweenShots = 0.1
rifleBulletDamage = 10
rifleBulletSpeed = 100.0
